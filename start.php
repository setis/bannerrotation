<?php

$debug = true;
include './bootstrap.php';
/* @var $collection MongoCollection */
$collection = Env::$banner->selectCollection(Env::$config['banner']['db'], Env::$company);
$collection->insert([
    'host' => 'dev2.studio-devel.com:81',
    'host_id' => 1,
    'global' => true,
    'banner' => [
        [
            'img' => 'pic.jpg',
            'width' => 400,
            'heigth' => 200,
            'click' => 'http://ya.ru'
        ]
    ],
    'time_max' => 100,
    'time_uniq' => 600,
    'time_rest'=> 100,
    'allow_country' => ['RU'],
    'allow_browser' => ['chrome']
]);
Token::create_key();