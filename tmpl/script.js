var timeout = 5000;
var timeout2 = 120000;
var id = null;
var s = true;
var url = '__URL__';
var token = '__TOKEN__';
function statistic() {
    var navigator = window.navigator;
    return {
        lang: (navigator.language || navigator.browserLanguage || navigator.systemLanguage || navigator.userLanguage || null),
        screen: window.screen.width + "x" + window.screen.height,
        ads: (
                ('\v' === 'v') &&
                (!(-[1, ]))
                ),
        time: Math.floor(new Date().getTime() / 1000),
        offset: (new Date()).getTimezoneOffset() / 60
    };
}
function init() {
    jsonFix();
    rotation();
    id = setInterval(rotation, timeout);
}
function rotation() {
    banner(url, statistic());
}
function banner(url, data) {
    var xhr = (new window.XMLHttpRequest() || new window.XDomainRequest());
    var link = document.createElement('a');
    link.href = url;
    var f = false;
    for (var i in data) {
        if (f) {
            link.search += '&';
        }
        link.search += i + '=' + data[i];
        f = true;
    }
    link.search += '&token=' + token;
    var load = function () {
        if (xhr.responseText !== '') {
            var data = JSON.parse(xhr.responseText);
            if (data.adsData !== undefined && data.adsData !== '' && data.adsData !== null) {
                (document.body || document.getElementsByTagName('body')[0]).innerHTML = data.adsData;
            }
            if (typeof data.interval === 'number') {
                timeout = data.interval;
                clearInterval(id);
                id = setInterval(rotation, timeout);
            }
            else if (s) {
                s = false;
                clearInterval(id);
                timeout = timeout2;
                id = setInterval(rotation, timeout);
            }
        }
    };
    var error = function () {
        console.error('error');
    };
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            load();
        } else if (xhr.readyState === 4 && xhr.status !== 200) {
            error();
        }
    };
    xhr.open('GET', link.href);
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    xhr.onload = load;
    xhr.onerror = error;
    xhr.send();
}
function jsonFix() {
    JSON = JSON || {};
    JSON.stringify = JSON.stringify || function (obj) {
        var t = typeof (obj);
        if (t != "object" || obj === null) {
            if (t == "string")
                obj = '"' + obj + '"';
            return String(obj);
        }
        else {
            var n, v, json = [], arr = (obj && obj.constructor == Array);
            for (n in obj) {
                v = obj[n];
                t = typeof (v);
                if (t == "string")
                    v = '"' + v + '"';
                else if (t == "object" && v !== null)
                    v = JSON.stringify(v);
                json.push((arr ? "" : '"' + n + '":') + String(v));
            }
            return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
        }
    };
    JSON.parse = JSON.parse || function (str) {
        if (str === "")
            str = '""';
        eval("var p=" + str + ";");
        return p;
    };
}
window.onload = init;
