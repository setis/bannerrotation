<?php

class Traffic {

    public static function check($base){
        $db = Env::$banner->selectDB(Env::$config['banner']['db']);
        if(!in_array($base, $db->getCollectionNames())){
            $db->createCollection($base);
        }
    }
    public static function get($base, array $data) {
        /* @var $collection \MongoCollection */
        $collection = Env::$banner->selectCollection(Env::$config['banner']['db'], $base);
        $result = $collection->findOne($data, ['t','_id']);
        return $result;
    }
    public static function update($base, array $data) {
        /* @var $collection MongoCollection */
        $collection = Env::$banner->selectCollection(Env::$config['banner']['db'], $base);
        $id = ['_id' => $data['_id']];
        unset($data['_id']);
        $collection->update($id, $data);
    }

    public static function set($base, array $data) {
        /* @var $collection MongoCollection */
        $collection = Env::$banner->selectCollection(Env::$config['banner']['db'], $base);;
        $collection->insert($data);
    }

}
