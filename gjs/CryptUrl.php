<?php

namespace gjs;

class CryptUrl {

    /**
     *
     * @var Wrapper;
     */
    public $Wrapper;
    public $link_int, $link_size;
    public $decode;
    public $handler;
    public $self = true;

    public function __construct($wrapper = null) {
        if ($wrapper instanceof Wrapper) {
            $this->Wrapper = &$wrapper;
        } else if (is_array($wrapper)) {
            $this->Wrapper = new Wrapper();
            Base::redis_check();
            $this->Wrapper->data = 'Base::redis_dict';
            $this->Wrapper->restore($wrapper);
        } else {
            $this->Wrapper = new Wrapper();
            Base::redis_check();
            $this->Wrapper->data = 'Base::redis_dict';
        }
    }

    /**
     * 
     * @param string $link
     * @param Wrapper|Array $wrapper
     * @param $this|static|self $self
     * @return string
     */
    public static function run($link, $wrapper = null, &$self = null) {
        $self = new self($wrapper);
        return $self->crypt($link);
    }

    public function crypt($link) {
        list($this->link_int, $this->link_size) = self::encode($link);
        $this->decode = $this->Wrapper->rand(true, false);
        $this->handler = $this->Wrapper->rand(true, false);
        $result[] = $this->decode();
        foreach ($this->callbacks() as $callback) {
            $result[] = $callback;
        }
        $result[] = $this->handler();
        shuffle($result);
        return implode('', $result);
    }

    /**
     * 
     * @return \CryptUrl
     */
    public function clear() {
        $this->link_int = null;
        $this->link_size = null;
        $this->handler = null;
        $this->decode = null;
//        $this->self = false;
        return $this;
    }

    public static function encode($str) {
        $result = false;
        for ($a = 0; $a < strlen($str); $a++) {
            $value = ord(substr($str, $a, 1));
            $result[0][] = $value;
            $result[1][] = strlen($value);
        }
        return $result;
    }

    public function decode() {
        $chroot = $this->Wrapper->chroot();
        $string = $chroot->rand(true, false);
        $start = $chroot->rand(true, false);
        $end = $chroot->rand(true, false);
        return "function {$this->decode}($string, $start, $end) {"
                . "return String.fromCharCode($string.toString().substr($start, $end));"
                . "}";
    }

    public $callbacksUse;
    public $callbacksCell;

    public function callbacks() {
        $this->callbacksCell = $this->callbacksUse = [];
        $size = count($this->link_int);
//        $randCell = mt_rand(5, rand(6, 20));
        $randUse = rand(0, ($size - 1));
        $randCell = 0;
        $listUniq = [];
        while (count($this->callbacksUse) !== $randUse) {
            $len = rand(0, $size - 1);
            if (!in_array($len, $listUniq)) {
                $this->callbacksUse[$this->Wrapper->rand(true, FALSE)] = $len;
                $listUniq[] = $len;
            }
        }
        $this->callbacksUse = array_combine(array_values($this->callbacksUse), array_keys($this->callbacksUse));
        $size2 = $size * rand(1, 5);
        while (count($this->callbacksCell) !== $randCell) {
            $len = rand(0, $size2);
            if (!in_array($len, $listUniq)) {
                $this->callbacksCell[$this->Wrapper->rand(true, FALSE)] = $len;
                $listUniq[] = $len;
            }
        }
        $this->callbacksCell = array_combine(array_values($this->callbacksCell), array_keys($this->callbacksCell));
        $result = [];
        if ($this->self) {
            $print = function($name, $n) {
                return "this.{$name} = function(){"
                        . "return $n;"
                        . "};";
            };
        } else {
            $print = function($name, $n) {
                return "function {$name}(){"
                        . "return $n;"
                        . "}";
            };
        }
        foreach ($this->callbacksUse as $i => $name) {
            if (mt_rand(0, 1)) {
                $array = $this->callGenerate($this->link_int[$i], $s);
//                $array = $this->callGenerateDebug($this->link_int[$i], $s, $js);
                $this->link_size[$i] = strlen($s);
//                echo "console.info('result',{$this->link_int[$i]},$s);";
                $this->link_int[$i] = $s;
                $result[] = $this->callPrint($array, $name);
//                $result[] = $this->callPrintDebug($array, $name);
//                $result[] = $js;
            } else {
                $result[] = call_user_func($print, $name, $this->link_int[$i]);
                $s = rand(0, 255);
                $this->link_size[$i] = strlen($s);
                $this->link_int[$i] = $s;
            }
        }
//        foreach ($this->callbacksCell as $i => $name) {
//            $d = rand(0, 1);
//            if (false) {
//                $array = $this->callGenerate(rand(0, 255), $s);
//                $this->link_int[] = $s;
//                $result[] = $this->callPrint($array, $name);
//            } else {
//                $s = rand(0, 255);
//                $this->link_int[] = $s;
//                $result[] = call_user_func($print, $name, $s);
//            }
//        }
        return $result;
    }

    const division = 0, multiply = 1, subtract = 2, add = 3;

    public static function calculator($method, $n1, $n2) {
        switch ((string) $method) {
            case '/':
            case self::division:
                return $n1 / $n2;
            case '*':
            case self::multiply:
                return $n1 * $n2;
            case '-':
            case self::subtract:
                return $n1 - $n2;
            case '+':
            case self::add:
                return $n1 + $n2;
        }
    }

    public static $rands = [
        self::division => [1, 9],
        self::multiply => [2, 10],
        self::subtract => [0, 255],
        self::add => [0, 255]
    ];
    public static $list = [
        '/', '*', '-', '+'
    ];

    /**
     * 
     * @param int|null $source
     * @param int|null $resource
     * @return array
     */
    public function callGenerate($source = null, &$resource = null) {
        $count = mt_rand(1, 10);
        $result = [];
        $resource = $eq = rand(0, 255);
        while (true) {
            $operation = mt_rand(0, 3);
            list($min, $max) = self::$rands[$operation];
            $n1 = $eq;
            $n2 = rand($min, $max);
            $eq = round(self::calculator($operation, $n1, $n2), 15);
            $result[] = [$operation, $n2];
            if (count($result) === $count) {
                if ($source === null) {
                    $source = rand(0, 255);
                }
                $diff = $eq - $source;
                if ($diff > 0) {
                    $result[] = [self::subtract, abs($diff)];
                } else {
                    $result[] = [self::add, abs($diff)];
                }

                return $result;
            }
        }
    }

    public function callGenerateDebug($source = null, &$resource = null, &$js = null) {
        $count = mt_rand(1, 10);
        $js = $result = [];
        $resource = $eq = rand(0, 255);
        while (true) {
            $operation = mt_rand(0, 3);
            list($min, $max) = self::$rands[$operation];
            $n1 = $eq;
            $n2 = rand($min, $max);
            $eq = round(self::calculator($operation, $n1, $n2), 15);
            echo "console.info('$n1" . self::$list[$operation] . "$n2=$eq');";
            $js[] = [$eq, $n1 . self::$list[$operation] . $n2];
            $result[] = [$operation, $n2];
            if (count($result) === $count) {
                if ($source === null) {
                    $source = rand(0, 255);
                }
                $diff = $eq - $source;
                echo "console.info('";
                echo ($diff > 0) ? "$eq - $diff" : "$eq+$diff";
                echo "');";
                if ($diff > 0) {
                    $result[] = [self::subtract, abs($diff)];
                    $js[] = [$source, $eq . '-' . abs($diff)];
                } else {
                    $result[] = [self::add, abs($diff)];
                    $js[] = [$source, $eq . '+' . abs($diff)];
                }

                $js = "console.info('php'," . json_encode($js) . ");";
                return $result;
            }
        }
    }

    public static function size(array $array) {
        return strlen(current(end($array))[1]);
    }

    public function callPrint($array, $name = null) {
        if ($name === null) {
            $name = $this->Wrapper->rand(true, false);
        }
        $var = $result = $this->Wrapper->rand(false, false);
        foreach ($array as $value) {
            list($operation, $n2) = $value;
            $result = '(' . $result . self::$list[$operation] . $n2 . ')';
        }
        if ($this->self) {
            return "this.$name = function($var){"
                    . "return Math.round($result);"
                    . "};";
        }
        return "function $name($var){"
                . "return Math.round($result);"
                . "}";
    }

    public function callPrintDebug($array, $name = null) {
        if ($name === null) {
            $name = $this->Wrapper->rand(true, false);
        }
        $var = $result = $this->Wrapper->rand(false, false);
        $arr = 'debug';
        foreach ($array as $i => $value) {
            list($operation, $n2) = $value;

            $result = "({$arr}[$i] = " . '(' . $result . self::$list[$operation] . $n2 . ')' . ")";
        }

        return "function $name($var){"
                . "var $arr = [];"
                . "var result = $result;"
                . "console.info('js',{$arr});"
                . "return Math.round(result);"
                . "}";
    }

    public function vars() {
        
    }

    public function handler() {
        $arr = $this->callbacksCell + $this->callbacksUse;
        $callbacksData = json_encode($arr);
        $listData = json_encode($this->link_size);
        $varsData = json_encode([]);
        $cryptData = implode('', $this->link_int);
        $chroot = $this->Wrapper->chroot();
        $chroot->list[] = $this->decode;
        $chroot->lock($this->decode);
        $callbacks = $chroot->rand(true, false);
        $list = $chroot->rand(true, false);
        $vars = $chroot->rand(true, false);
        $crypt = $chroot->rand(true, false);
        $len = $chroot->rand(true, false);
        $return = $chroot->rand(true, false);
        $i = $chroot->rand(true, false);
        $size = $chroot->rand(true, false);
        $value = $chroot->rand(true, false);
        $call = $chroot->rand(true, false);
        $var = $chroot->rand(true, false);
//        foreach (['list', 'vars', 'len', 'crypt', 'call', 'value', 'i', 'size', 'callbacks'] as $v) {
//            ${$v} = $v;
//        }
//        $callbacksData = '[]';
        $result[] = "$list = $listData";
        $result[] = "$vars = $varsData";
        $result[] = "$crypt = \"$cryptData\"";
        $result[] = "$callbacks = $callbacksData";
        $result[] = "$len = 0";
        $result[] = "$return = []";
        shuffle($result);
        $result = implode(',', $result);

        $result2[] = "$size = {$list}[$i]";
        $result2[] = "$value = $crypt.substr($len, $size)";
        $result2[] = "$call = {$callbacks}[$i]";
        $result2[] = "$var = {$vars}[$len]";
        shuffle($result2);
        $result2 = implode(',', $result2);
        return "function $this->handler(){"
                . "var $result;"
//                . "console.info('calling',$callbacks);"
                . "for(var $i in $list){"
                . "var $result2;"
                . "if(String($value).length !== $size){"
//                . "console.info($len,$size,$crypt,$value,'Number(\"$cryptData\".substr('+$len+', '+$size+'))');"
                . "$value = $crypt.substr($len, $size);"
//                . "console.info($len,$size,$crypt,$value,'Number(\"$cryptData\").substr('+$len+', '+$size+'))');"
                . "}"
                . "$value=Number($value);"
//                . "if($call){"
//                . "console.info(typeof $call,$call);"
//                . "console.info(this[$call]);"
//                . "console.info( $call+'('+$value+')',$i);"
//                . "}"
                . "if ($call !== undefined) {"
                . "if($var) {"
                . "this[$var] =  String.fromCharCode((typeof $call === 'function') ? $call($value) : this[$call]($value));"
                . "}else{"
//                . "console.info('error->',{$return},typeof {$return}); "
                . "{$return}.push(String.fromCharCode((typeof $call === 'function') ? $call($value) : this[$call]($value)));"
                . "}}else{"
                . "($var) ? this[$var] = String.fromCharCode($value) : {$return}.push(String.fromCharCode($value));"
                . "}"
//                . "console.info($len,$size,$crypt,$value,String($value).length,'Number(\"$cryptData\".substr('+$len+', '+$size+'))',($call !== undefined),$return);"
                . "$len +=Number($size);"
                . "}"
                . "return $return.join('');"
                . "}";
    }

}
