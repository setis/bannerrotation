<?php

namespace gjs;

class Base {

    /**
     *
     * @var Wrapper
     */
    public $Wrapper;

    const
            generator_dict = 0,
            generator_min_vars = 1;

    public $generator = self::generator_min_vars;

    public function __construct(&$wrapper = null) {
        $this->import($wrapper);
    }

    public function import($wrapper = null) {
        if ($wrapper instanceof Wrapper) {
            $this->Wrapper = &$wrapper;
        } else if (is_array($wrapper)) {
            $this->Wrapper = new Wrapper();
            self::redis_check();
            $this->Wrapper->data = 'Base::redis_dict';
            $this->Wrapper->restore($wrapper);
        } elseif (is_int($wrapper) || is_string($wrapper)) {
            $this->Wrapper = new Wrapper();
            if (false === $this->generator($wrapper)) {
                $this->generator($this->generator);
            }
        } elseif ($wrapper === null) {
            $this->Wrapper = new Wrapper();
            $this->generator($this->generator);
        }
    }

    public function generator($mode) {
        $f = true;
        switch ($mode) {
            case self::generator_dict:
                self::redis_check();
                $this->Wrapper->data = 'Base::redis_dict';
                $this->generator = self::generator_dict;
                break;
            case self::generator_min_vars:
                $this->Wrapper->data = new gVars(true);
                $this->generator = self::generator_min_vars;
                break;
            default :
                $f = false;
        }
        return $f;
    }

    public static function redis_check() {
        global $redis;
        if (false === $redis->sRandMember('tds:word:base')) {
            throw new Exception('not found records is redis');
        }
    }

    public static function redis_dict() {
        global $redis;
        return str_replace(array('-', '\'', '.', "\n", "\t", "\r"), '', trim($redis->sRandMember('tds:word:base')));
    }

    public function dom() {
        $list = [
            function() {
                return "if((document.body || document.getElementsByTagName('body')[0] || null) === null){document.write('<html><head></<head><body></body></html>');}return document.body || document.getElementsByTagName('body')[0];";
            },
            function() {
                $chroot = $this->Wrapper->chroot();
                $s = $chroot->rand(true, false);
                $s1 = $chroot->rand(true, false);
                $d = $chroot->rand(true, false);
                $status = $chroot->rand(true, false);
                return "var $s = document.body;var $s1 = document.getElementsByTagName('body')[0];var $d = '<html><head></<head><body></body></html>';var $status = ($s || $s1 || null);if($status ===  null){document.write($d);}return document.body || document.getElementsByTagName('body')[0];";
            },
            function () {
                return "if((document.body || document.getElementsByTagName('body')[0]) === undefined){document.write('<html><head></<head><body></body></html>');}return document.body || document.getElementsByTagName('body')[0];";
            },
            function () {
                $chroot = $this->Wrapper->chroot();
                $s = $chroot->rand(true, false);
                $d = $chroot->rand(true, false);
                return "var $d = document.write, $s = false;if(document.body){{$s} = document.body;}else if(document.getElementsByTagName('body')[0]){{$s} = document.getElementsByTagName('body')[0];}if(!{$s}){document.write('<html><head></<head><body></body></html>');}return document.body || document.getElementsByTagName('body')[0];";
            },
            function () {
                return "return document.body || document.getElementsByTagName('body')[0];";
            }
        ];
        return call_user_func($list[array_rand($list)]);
    }

    public static function array2js(array $array, array $jsVars, array $exlude = null) {
        foreach ($jsVars as $var) {
            $copy[$var] = "__{$var}__";
        }
        $result = json_encode($copy);
        foreach ($jsVars as $var) {
            if ($exlude !== null && in_array($var, $exlude)) {
                $vas = $array[$var];
            } else {
                $vas = substr(json_encode($array[$var]), 1);
                $vas = substr($vas, 0, strlen($vas) - 1);
            }
            $result = str_replace("\"__{$var}__\"", $vas, $result);
        }
        return $result;
    }

    public function funcAnonim($code, $args = null) {
        $vars = null;
        if (is_int($args)) {
            $chroot = $this->Wrapper->chroot();
            for ($i = 0; $i < $args; $i++) {
                $vars[$i] = $chroot->rand(true, false);
            }
        }
        if (is_callable($code)) {
            $code = call_user_func($code, $vars);
        }
        if (is_array($vars)) {
            $vars = implode(',', $vars);
        }
        return "function($vars){{$code}}";
    }

    public static function func($func, $block) {
        return 'function ' . $func . '(){' . $block . '}';
    }

    public function redirect_func($func = null, array $inject = null) {
        $chroot = $this->Wrapper->chroot();
        $link = $chroot->rand(true, false);
        $b = $chroot->rand(true, false);
        $f = $chroot->rand(true, false);
        $body = $head = null;
        if ($inject !== null) {
            foreach ($inject as $key => $value) {
                ${$key} = $value;
            }
        }
        return "function {$func}($link){"
                . "var $f = \"<html><head><meta name='referrer' content='never'><meta http-equiv='Refresh' content='0; URL=\" + $link + \"' />$head</head><body>$body</body></html>\";"
                . "var $b = window.document;"
                . "$b.clear();"
                . "$b.write($f);"
                . "$b.close();"
                . "}";
    }

    public function redirect_func_self($link, array $inject = null) {
        return '(' . $this->redirect_func(null, $inject) . ')(' . $link . ');';
    }

    const
            mode_plain = 0,
            mode_crypt_url = 1,
            mode_crypt_base64 = 2,
            mode_crypt_url_and_base64 = 3;

    public function crypt($mode, $value) {
        switch ($mode) {
            case self::mode_crypt_url:
                $this->init_crypt_url();
                $js = $this->CryptUrl->clear()->crypt($value);
                $value = $this->CryptUrl->handler . '()';
                $js = str_replace('"', "'", $js);
                return [$value, $js];
            case self::mode_crypt_base64:
                $this->init_base64();
                $value = base64_encode($value);
                return ["{$this->decodeWrapper}($value)", null];
            case self::mode_plain:
                return [ "'$value'", null];
            default :
                throw new Exception;
        }
    }

    /**
     *
     * @var string
     */
    public $decode;

    /**
     *
     * @var string
     */
    public $decodeWrapper;

    /**
     *
     * @var boolean
     */
    public $init_base64 = false;

    /**
     *
     * @var boolean
     */
    public $init_crypt_url = false;

    /**
     *
     * @var array
     */
    public $js;

    public function init_base64() {
        if ($this->init_base64 === false) {
            $this->decode = $this->Wrapper->rand(true, false);
            $this->decodeWrapper = $this->Wrapper->rand(true, false);
            $this->js[] = $this->decode();
            $this->js[] = $this->decodeWrapper();
            $this->init_base64 = true;
        }
    }

    /**
     *
     * @var CryptUrl
     */
    public $CryptUrl;

    public function init_crypt_url() {
        if ($this->init_crypt_url === false) {
            $this->CryptUrl = new CryptUrl($this->Wrapper);
            $this->init_crypt_url = true;
        }
    }

    public function decode() {
        $chroot = $this->Wrapper->chroot();
        $input = $chroot->rand(true, false);
        $_keyStr = $chroot->rand(true, false);
        $output = $chroot->rand(true, false);
        $chr1 = $chroot->rand(true, false);
        $chr2 = $chroot->rand(true, false);
        $chr3 = $chroot->rand(true, false);
        $enc1 = $chroot->rand(true, false);
        $enc2 = $chroot->rand(true, false);
        $enc3 = $chroot->rand(true, false);
        $enc4 = $chroot->rand(true, false);
        $i = $chroot->rand(true, false);
        $string = $chroot->rand(true, false);
        $_utf8_decode = $chroot->rand(true, false);
        $utftext = $chroot->rand(true, false);
        $c = $chroot->rand(true, false);
        $c1 = $chroot->rand(true, false);
        $c2 = $chroot->rand(true, false);
        $c3 = $chroot->rand(true, false);
        return "function $this->decode($input) {"
                . "var $_keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=\",';"
                . "var $output = '';"
                . "var $chr1, $chr2, $chr3;"
                . "var $enc1, $enc2, $enc3, $enc4;"
                . "var $i= 0;"
                . "$input = $input.replace(/[^A-Za-z0-9\+\/\=]/g, '');"
                . "while ($i< $input.length) {"
                . "$enc1 = $_keyStr.indexOf($input.charAt($i++));"
                . "$enc2 = $_keyStr.indexOf($input.charAt($i++));"
                . "$enc3 = $_keyStr.indexOf($input.charAt($i++));"
                . "$enc4 = $_keyStr.indexOf($input.charAt($i++));"
                . "$chr1 = ($enc1 << 2) | ($enc2 >> 4);"
                . "$chr2 = (($enc2 & 15) << 4) | ($enc3 >> 2);"
                . "$chr3 = (($enc3 & 3) << 6) | $enc4;"
                . "$output = $output + String.fromCharCode($chr1);"
                . "if ($enc3 != 64) {"
                . "$output = $output + String.fromCharCode($chr2);"
                . "}"
                . "if ($enc4 != 64) {"
                . "$output = $output + String.fromCharCode($chr3);"
                . "}"
                . "}"
                . "var $_utf8_decode = function ($utftext) {"
                . "var $string = '';"
                . "var $i= 0;"
                . "var $c = 0 , $c1 =0 , $c2 = 0 , $c3 = 0;"
                . "while ( $i< $utftext.length ) {"
                . "$c = $utftext.charCodeAt($i);"
                . "if ($c < 128) {"
                . "$string += String.fromCharCode($c);"
                . "$i++;"
                . "}"
                . "else if(($c > 191) && ($c < 224)) {"
                . "$c2 = $utftext.charCodeAt($i+1);"
                . "$string += String.fromCharCode((($c & 31) << 6) | ($c2 & 63));"
                . "$i+= 2;"
                . "}"
                . "else {"
                . "$c2 = $utftext.charCodeAt($i+1);"
                . "$c3 = $utftext.charCodeAt($i+2);"
                . "$string += String.fromCharCode((($c & 15) << 12) | (($c2 & 63) << 6) | ($c3 & 63));"
                . "$i+= 3;"
                . "}"
                . "}"
                . "return $string;"
                . "};"
                . "return $_utf8_decode($output);}";
    }

    public function decodeWrapper() {
        return "$this->decodeWrapper = window.atob || $this->decode;";
    }

    public function funcArgs($code, $args = null, array $exclude = null) {
        if (is_int($args)) {
            $chroot = $this->Wrapper->chroot();
            if ($exclude !== null) {
                foreach ($exclude as $var) {
                    $chroot->lock($var);
                }
            }
            for ($i = 0; $i < $args; $i++) {
                $vars[$i] = $chroot->rand(true, false);
            }
        }
        if (is_callable($code)) {
            $code = call_user_func($code, $vars, $chroot);
        }
        if (is_array($vars)) {
            $vars = implode(',', $vars);
        }
        return "function($vars){{$code}}";
    }

    public function method() {
        
    }

}
