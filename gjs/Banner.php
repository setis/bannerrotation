<?php

namespace gjs;

class Banner extends Base {

    public $link;
    public $link_click;
    public $link_tracker;
    public $img_click;
    public $img_tracker;
    public $width;
    public $height;
    public $body;
    public $domain;
    public $browser, $lang, $screen, $os, $click, $tracker;
    public $mode_encrypt_click = self::mode_crypt_url;
    public $mode_encrypt_domain = self::mode_crypt_url;
    public $backend = false;
    public $inject;

    public function click() {
        $chroot = $this->Wrapper->chroot();
        $chroot->lock($this->body);
        $aTag = $chroot->rand(true, false);
        $imgTag = $chroot->rand(true, false);
        $data = $chroot->rand(true, false);
        $height = $width = $src = $append = $img = $body = $create = $writeCreate = $write = $href = $images = $link = null;
        $list1 = ['img', 'append', 'body', 'create', 'write', 'writeCreate', 'images', 'link', 'src', 'height', 'width'];
        foreach ($list1 as $var) {
            $val = $chroot->rand(true, false);
            ${$var} = $val;
        }
        $list = [
//            'a' => $this->funcAnonim("return 'a';"),
            'img' => 'img',
            'append' => $this->funcArgs(function($args) {
                        return "return $args[0].appendChild($args[1]);";
                    }, 2),
            'create' => $this->funcArgs(function($args) {
                        return "return document.createElement($args[0]);";
                    }, 1),
            'body' => $this->body,
            'write' => $this->funcArgs(function($args) {
                        return "document.write($args[0].outerHTML);";
                    }, 1),
            'writeCreate' => $this->funcArgs(function($args) {
                        return "document.write('<html><head></head><body>'+$args[0].outerHTML+'</body></html>');";
                    }, 1),
            'src' => $this->funcArgs(function($args) {
                        return "{$args[0]}.src = {$this->img_click};";
                    }, 1),
            'height' => $this->funcArgs(function($args) {
                        return "{$args[0]}.height = {$this->height};";
                    }, 1),
            'width' => $this->funcArgs(function($args) {
                        return "{$args[0]}.width = {$this->width};";
                    }, 1),
            'images' => $this->funcAnonim("var $imgTag = this['$create']('i'+'m'+'g');"
                    . "this['{$height}']({$imgTag});"
                    . "this['{$width}']({$imgTag});"
                    . "this['{$src}']({$imgTag});"
                    . "return $imgTag;"),
            'link' => $this->funcAnonim(function()use($create) {
                        $chroot = $this->Wrapper->chroot();
                        $aTag = $chroot->rand(true, false);
                        $link = "javascript:(function(){window['open']($this->link);void(0);})();";
                        $link = str_replace('\'', "\"", $link);
                        return "var $aTag = this['$create']('a');"
                                . "{$aTag}['href'] = '$link';"
                                . "return $aTag;";
                    })
        ];
        $list2 = [
            'append', 'body', 'create', 'writeCreate', 'write', 'images', 'link', 'src', 'height', 'width'
        ];
        shuffle($list1);
        foreach ($list1 as $var) {
            $resultObj[${$var}] = $list[$var];
            if (in_array($var, $list2)) {
                $list3[] = ${$var};
            }
        }
        $result = self::array2js($resultObj, $list3);
        return "var $data = $result;"
                . "var $aTag = {$data}['$link']();"
                . ($this->backend === true) ? '' : "{$data}['$append']($aTag,{$data}['$images']());"
                . "if({$data}['$body']()){{$data}['$write']($aTag);}"
                . "else{{$data}['$writeCreate']($aTag);}";
    }

    public function backend() {
        $body = $head = null;
        if ($this->inject !== null) {
            foreach ($this->inject as $key => $value) {
                if (is_array($value)) {
                    $value = implode('', $value);
                }
                ${$key} = $value;
            }
        }
        $href = $add = '';
        switch (mt_rand(0, 1)) {
            case 0:
                $href = "javascript:(function(){window['open']($this->link);void(0);})();";
                break;
            case 1:
                $add = "onclick=\"(function(e){window['open']($this->link);void(0);"
//                    . " if(!e){window.event.cancelBubble = true;}else if(e.stopPropagation){e.stopPropagation();}"
                        . "})();return false;\"";
                break;
        }
        $html = false;
        if ($html === true) {
            $h = '<!DOCTYPE html public "-//W3C//DTD HTML 4.0 Transitional//en">';
        } else {
            $h = '<html>';
        }
        return $h . '<head><meta charset="utf-8">' . $head . '</head><body>'
                . '<a href="' . $href . '" ' . $add . '><img src=' . $this->img_click . ' width="' . $this->width . '" heigth="' . $this->height . '"></a>'
                . $body . '</body></html>';
    }

    public function body() {
        return "function {$this->body}(){return document.body||document.getElementsByTagName('body')[0];}";
    }

    public function tracker() {
        $chroot = $this->Wrapper->chroot();
        $chroot->lock($this->body);
        $query = '';
        $exclude = [];
        foreach ([
    'os' => $this->os,
    'browser' => $this->browser,
    'lang' => $this->lang,
    'screen' => $this->screen
        ] as $name => $func) {
            $exclude[] = $func;
            $chroot->lock($func);
            $query.="&$name='+$func()+'";
        }
        $width = $height = $append = $img = $body = $create = $image = null;
        $list1 = [ 'img', 'append', 'create', 'body', 'image'];
        foreach ($list1 as $var) {
            $val = $chroot->rand(true, false);
            ${$var} = $val;
        }
        $list = [
            'img' => $this->funcAnonim("return 'i'+'mg';"),
            'append' => $this->funcArgs(function($args) {
                        return "return $args[0].appendChild($args[1]);";
                    }, 2),
            'create' => $this->funcArgs(function($args) {
                        return "return document['create'+'Element']($args[0]);";
                    }, 1),
            'body' => $this->funcAnonim("return document.body||document.getElementsByTagName('body')[0];"),
            'image' => $this->funcArgs(function($args)use($query) {
                        $link = (($this->backend !== false && $this->mode_encrypt_domain !== self::mode_plain) ? $this->img_tracker . "+'?" . substr($query, 1) . "'" : "'" . $this->img_tracker . '?' . substr($query, 1) . "'");
                        return "$args[0].src = $link;"
                                . "$args[0].height = '1';"
                                . "$args[0].width = '1';"
                                . "return $args[0];";
                    }, 1, $exclude),
        ];
        $list2 = [
            'append', 'body', 'create', 'src', 'image', 'img'
        ];
        shuffle($list1);
        foreach ($list1 as $var) {
            $resultObj[${$var}] = $list[$var];
            if (in_array($var, $list2)) {
                $list3[] = ${$var};
            }
        }
        $data = $chroot->rand(true, false);

        $result = "var $data = " . self::array2js($resultObj, $list3) . ";";
        $result.="{$data}['$append']({$data}['$body'](),{$data}['$image']({$data}['$create']({$data}['$img']())));";
        return $result;
    }

    public function lang() {
        $list = [
            function() {
                return 'return navigator && (navigator.language || navigator.browserLanguage || navigator.systemLanguage || navigator.userLanguage || null);';
            },
            function() {
                return "return (navigator.language || navigator.systemLanguage || navigator.userLanguage || 'ru').substr(0, 2).toLowerCase();";
            },
            function() {
                $chroot = $this->Wrapper->chroot();
                $list = $chroot->rand(true, false);
                $i = $chroot->rand(true, false);
                $value = $chroot->rand(true, false);
                return "var $list = ['language', 'browserLanguage', 'systemLanguage', 'userLanguage'];if(navigator){for(var $i in $list){var $value = {$list}[$i];if(navigator[$value]){return navigator[$value];}}}return '';";
            },
        ];
        return call_user_func($list[array_rand($list)]);
    }

    public function browser() {
        $list = [
            function() {
                $chroot = $this->Wrapper->chroot();
                $N = $chroot->rand(true, false);
                $M = $chroot->rand(true, false);
                $ua = $chroot->rand(true, false);
                $tem = $chroot->rand(true, false);
                return "var $N=navigator.appName, $ua=navigator.userAgent, $tem;var $M=$ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);if($M && ($tem = $ua.match(/version\/([\.\d]+)/i)) != null){{$M}[2] = {$tem}[1];}$M = $M ? [{$M}[1], {$M}[2]] : [$N, navigator.appVersion, '-?'];return {$M}[0]+'|'+{$M}[1];";
            },
            function() {
                $chroot = $this->Wrapper->chroot();
                $BrowserDetect = $chroot->rand(true, false);
                $init = $chroot->rand(true, false);
                $browser = $chroot->rand(true, false);
                $searchString = $chroot->rand(true, false);
                $dataBrowser = $chroot->rand(true, false);
                $version = $chroot->rand(true, false);
                $searchVersion = $chroot->rand(true, false);
                $data = $chroot->rand(true, false);
                $i = $chroot->rand(true, false);
                $dataString = $chroot->rand(true, false);
                $dataProp = $chroot->rand(true, false);
                $versionSearchString = $chroot->rand(true, false);
                $index = $chroot->rand(true, false);
                $string = $chroot->rand(true, false);
                $subString = $chroot->rand(true, false);
                $identity = $chroot->rand(true, false);
                $versionSearch = $chroot->rand(true, false);
                $prop = $chroot->rand(true, false);
                $func = [
                    "$dataBrowser: [{{$string}: navigator.userAgent,$subString: \"Chrome\",$identity: \"Chrome\"},{{$string}: navigator.userAgent,$subString: \"OmniWeb\",$versionSearch: \"OmniWeb/\",$identity: \"OmniWeb\"},{{$string}: navigator.vendor,$subString: \"Apple\",$identity: \"Safari\",$versionSearch: \"Version\"},{{$prop}: window.opera,$identity: \"Opera\"},{{$string}: navigator.vendor,$subString: \"iCab\",$identity: \"iCab\"},{{$string}: navigator.vendor,$subString: \"KDE\",$identity: \"Konqueror\"},{{$string}: navigator.userAgent,$subString: \"Firefox\",$identity: \"Firefox\"},{{$string}: navigator.vendor,$subString: \"Camino\",$identity: \"Camino\"},{{$string}: navigator.userAgent,$subString: \"Netscape\",$identity: \"Netscape\"},{{$string}: navigator.userAgent,$subString: \"MSIE\",$identity: \"Explorer\",$versionSearch: \"MSIE\"},{{$string}: navigator.userAgent,$subString: \"Gecko\",$identity: \"Mozilla\",$versionSearch: \"rv\"},{{$string}: navigator.userAgent,$subString: \"Mozilla\",$identity: \"Netscape\",$versionSearch: \"Mozilla\"}]",
                    "$init: function(){this.$browser = this.$searchString(this.$dataBrowser) || \"An unknown browser\";this.$version = this.$searchVersion(navigator.userAgent)|| this.$searchVersion(navigator.appVersion)|| \"an unknown version\";}",
                    "$searchString: function($data){for(var $i = 0; $i < $data.length; $i++){var $dataString = {$data}[$i].$string;var $dataProp = {$data}[$i].$prop;this.$versionSearchString = {$data}[$i].$versionSearch || {$data}[$i].$identity;if($dataString){if($dataString.indexOf({$data}[$i].$subString) != -1){return {$data}[$i].$identity;}}else if ($dataProp){return {$data}[$i].$identity;}}}",
                    "$searchVersion: function($dataString){var $index=$dataString.indexOf(this.$versionSearchString);if($index == -1){return;}return parseFloat($dataString.substring($index + this.$versionSearchString.length + 1));}"
                ];
                shuffle($func);
                $result = "var $BrowserDetect = {" . implode(',', $func) . '};' . "$BrowserDetect.$init();";
                $result.=(rand(0, 1) === 1) ? "return $BrowserDetect.$browser+'|'+$BrowserDetect.$version;" : "return $BrowserDetect.$version+'|'+$BrowserDetect.$browser;";
                return $result;
            },
                    function() {
                $chroot = $this->Wrapper->chroot();
                $M = $chroot->rand(true, false);
                $ua = $chroot->rand(true, false);
                $tem = $chroot->rand(true, false);
                return "var $ua = navigator.userAgent, $tem,$M = $ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];if(/trident/i.test({$M}[1])) {{$tem} = /\brv[ :]+(\d+)/g.exec($ua) || [];return 'IE ' + ({$tem}[1] || '');}if({$M}[1] === 'Chrome'){{$tem} = $ua.match(/\bOPR\/(\d+)/);if($tem != null){return 'Opera ' + {$tem}[1];}}$M = {$M}[2] ? [{$M}[1], {$M}[2]] : [navigator.appName, navigator.appVersion, '-?'];if(($tem = $ua.match(/version\/(\d+)/i)) != null){{$M}.splice(1, 1, {$tem}[1])};return {$M}[0]+'|'+{$M}[1];";
            },
                    function() {
                $chroot = $this->Wrapper->chroot();
                $ua = $chroot->rand(true, false);
                $check = $chroot->rand(true, false);
                $r = $chroot->rand(true, false);
                $DOC = $chroot->rand(true, false);
                $isStrict = $chroot->rand(true, false);
                $isOpera = $chroot->rand(true, false);
                $isChrome = $chroot->rand(true, false);
                $isWebKit = $chroot->rand(true, false);
                $isSafari = $chroot->rand(true, false);
                $isSafari2 = $chroot->rand(true, false);
                $isSafari3 = $chroot->rand(true, false);
                $isSafari4 = $chroot->rand(true, false);
                $isIE = $chroot->rand(true, false);
                $isIE7 = $chroot->rand(true, false);
                $isIE8 = $chroot->rand(true, false);
                $isIE6 = $chroot->rand(true, false);
                $isGecko = $chroot->rand(true, false);
                $isGecko2 = $chroot->rand(true, false);
                $isGecko3 = $chroot->rand(true, false);
                $isBorderBox = $chroot->rand(true, false);
                $isStrict = $chroot->rand(true, false);
                $isWindows = $chroot->rand(true, false);
                $isMac = $chroot->rand(true, false);
                $isAir = $chroot->rand(true, false);
                $isLinux = $chroot->rand(true, false);
                $isSecure = $chroot->rand(true, false);
                $isIE7InIE8 = $chroot->rand(true, false);
                $isIE7 = $chroot->rand(true, false);
                $DOC = $chroot->rand(true, false);
                $jsType = $chroot->rand(true, false);
                $browserType = $chroot->rand(true, false);
                $browserVersion = $chroot->rand(true, false);
                $osName = $chroot->rand(true, false);
                $isWindows = $chroot->rand(true, false);
                $osName = $chroot->rand(true, false);
                $versionStart = $chroot->rand(true, false);
                $versionEnd = $chroot->rand(true, false);
                $isFF = $chroot->rand(true, false);
                return "var $ua = navigator.userAgent.toLowerCase();var $check = function($r){return $r.test($ua);};var $DOC = document;var $isStrict = $DOC.compatMode == \"CSS1Compat\";var $isOpera = $check(/opera/);var $isChrome = $check(/chrome/);var $isWebKit = $check(/webkit/);var $isSafari = !$isChrome && $check(/safari/);var $isSafari2 = $isSafari && $check(/applewebkit\/4/);var $isSafari3 = $isSafari && $check(/version\/3/);var $isSafari4 = $isSafari && $check(/version\/4/);var $isIE = !$isOpera && $check(/msie/);var $isIE7 = $isIE && $check(/msie 7/);var $isIE8 = $isIE && $check(/msie 8/);var $isIE6 = $isIE && !$isIE7 && !$isIE8;var $isGecko = !$isWebKit && $check(/gecko/);var $isGecko2 = $isGecko && $check(/rv:1\.8/);var $isGecko3 = $isGecko && $check(/rv:1\.9/);var $isBorderBox = $isIE && !$isStrict;var $isWindows = $check(/windows|win32/);var $isMac = $check(/macintosh|mac os x/);var $isAir = $check(/adobeair/);var $isLinux = $check(/linux/);var $isSecure = /^https/i.test(window.location.protocol);var $isIE7InIE8 = $isIE7 && $DOC.documentMode == 7;var $jsType = '', $browserType = '', $browserVersion = '', $osName = '';if ($isIE){{$browserType} = 'IE';$jsType = 'IE';var $versionStart = $ua.indexOf('msie') + 5;var $versionEnd = $ua.indexOf(';', $versionStart);$browserVersion = $ua.substring($versionStart, $versionEnd);$jsType = $isIE6 ? 'IE6' : $isIE7 ? 'IE7' : $isIE8 ? 'IE8' : 'IE';} else if ($isGecko){var $isFF = $check(/firefox/);$browserType = $isFF ? 'Firefox' : 'Others';$jsType = $isGecko2 ? 'Gecko2' : $isGecko3 ? 'Gecko3' : 'Gecko';if ($isFF){var $versionStart = $ua.indexOf('firefox') + 8;var $versionEnd = $ua.indexOf(' ', $versionStart);if($versionEnd == -1){{$versionEnd} = $ua.length;}$browserVersion = $ua.substring($versionStart, $versionEnd);}}else if($isChrome){{$browserType} = 'Chrome';$jsType = $isWebKit ? 'Web Kit' : 'Other';var $versionStart = $ua.indexOf('chrome') + 7;var $versionEnd = $ua.indexOf(' ', $versionStart);$browserVersion = $ua.substring($versionStart, $versionEnd);}else{{$browserType} = $isOpera ? 'Opera' : isSafari ? 'Safari' : '';}return $browserType +'|'+$browserVersion;";
            },
                ];
                return call_user_func($list[array_rand($list)]);
            }

            public function os() {
                $list = [
                    function() {
                        $chroot = $this->Wrapper->chroot();
                        $ua = $chroot->rand(true, false);
                        $r = $chroot->rand(true, false);
                        $check = $chroot->rand(true, false);
                        $osName = $chroot->rand(true, false);
                        $isWindows = $chroot->rand(true, false);
                        $isLinux = $chroot->rand(true, false);
                        $isMac = $chroot->rand(true, false);
                        $osName = $chroot->rand(true, false);
                        $start = $chroot->rand(true, false);
                        $end = $chroot->rand(true, false);
                        return "var $ua = navigator.userAgent.toLowerCase();var $osName;var $check = function($r) {return $r.test($ua);};"
                                . "var $isLinux = $check(/linux/);"
                                . "var $isWindows = $check(/windows|win32/);"
                                . "var $isMac = $check(/macintosh|mac os x/);"
                                . "if ($isWindows){{$osName} = 'Windows';if ($check(/windows nt/)){var $start = $ua.indexOf('windows nt');var $end = $ua.indexOf(';', $start);$osName = $ua.substring($start, $end);}}else{{$osName} = $isMac ? 'Mac' : $isLinux ? 'Linux' : 'Other';}"
                                . "return $osName;";
                    },
                    function() {
                        return "if (navigator.userAgent.indexOf('Windows') != -1)return 'Windows';if (navigator.userAgent.indexOf('Linux') != -1)return 'Linux';if (navigator.userAgent.indexOf('Mac') != -1)return 'Mac';if (navigator.userAgent.indexOf('FreeBSD') != -1)return 'FreeBSD';return 'Other';";
                    },
                    function() {
                        $chroot = $this->Wrapper->chroot();
                        $list = $chroot->rand(true, false);
                        $i = $chroot->rand(true, false);
                        $val = $chroot->rand(true, false);
                        return "var $list = ['Windows', 'Linux', 'Mac', 'FreeBSD'];" .
                                "for (var $i in $list){" .
                                "var $val = {$list}[$i];" .
                                "if(navigator.userAgent.indexOf($val) != -1)" .
                                "return $val;" .
                                "}return 'Other';";
                    },
                    function() {
                        $chroot = $this->Wrapper->chroot();
                        $data = $chroot->rand(true, false);
                        $i = $chroot->rand(true, false);
                        $dataString = $chroot->rand(true, false);
                        $dataProp = $chroot->rand(true, false);
                        $string = $chroot->rand(true, false);
                        $subString = $chroot->rand(true, false);
                        $identity = $chroot->rand(true, false);
                        $prop = $chroot->rand(true, false);
                        return " var $data = [{{$string}: navigator.platform,$subString: \"Win\",$identity: \"Windows\"},{{$string}: navigator.platform,$subString: \"Mac\",$identity: \"Mac\"},{{$string}: navigator.userAgent,$subString: \"iPhone\",$identity: \"iPhone/iPod\"},{{$string}: navigator.platform,$subString: \"Linux\",$identity: \"Linux\"}];for(var $i = 0; $i < $data.length; $i++){var $dataString = {$data}[$i].$string;var $dataProp = {$data}[$i].$prop;if($dataString){if($dataString.indexOf({$data}[$i].$subString) != -1)return {$data}[$i].$identity;}else if ($dataProp)return {$data}[$i].$identity;}";
                    }
                ];
                return call_user_func($list[array_rand($list)]);
            }

            public function screen() {
                $list = [
                    function() {
                        return 'return screen.width+"|"+screen.height;';
                    },
                    function() {
                        $chroot = $this->Wrapper->chroot();
                        $d = $chroot->rand(true, false);
                        $result = $chroot->rand(true, false);
                        $list = $chroot->rand(true, false);
                        $i = $chroot->rand(true, false);
                        return "var $d = window.screen;"
                                . "var $result = '';"
                                . "var $list = ['width','height'];"
                                . "for(var $i in $list){"
                                . "$result+='|'+{$d}[{$list}[$i]];"
                                . "}"
                                . "return $result.substr(1);";
                    }
                ];
                return call_user_func($list[array_rand($list)]);
            }

            public $CryptUrl;

            public function method() {
                $result = [];
                list($func, $js) = $this->crypt($this->mode_encrypt_click, $this->link);
                if ($this->mode_encrypt_click !== self::mode_plain) {
                    $this->link = "(function(){{$js} return $func;})()";
                } else {
                    $this->link = $func;
                }
                if ($this->backend === false && $this->mode_encrypt_domain !== self::mode_plain) {
                    list($func, $js) = $this->crypt($this->mode_encrypt_domain, "http://$this->domain/");
                    $result[] = $js;
                    $this->img_click = "$func+'{$this->link_click}'";
                    $this->img_tracker = "$func+'{$this->link_tracker}'";
                } else {
                    $this->img_click = "'http://$this->domain/$this->link_click'";
                    $this->img_tracker = "'http://$this->domain/$this->link_tracker'";
                }
                $list = ['click', 'lang', 'browser', 'screen', 'os', 'tracker'];
                foreach ($list as $var) {
                    $this->{$var} = $this->Wrapper->rand(true, false);
                }
                $this->body = $this->Wrapper->rand(true, false);
                $result[] = self::func($this->body, $this->dom());
                foreach (['lang', 'browser', 'screen', 'os'] as $func) {
                    $result[] = self::func($this->{$func}, call_user_func([$this, $func]));
                }
                shuffle($result);
                $func = 'tracker';
                $result[] = self::func($this->{$func}, call_user_func([$this, $func]));
                if ($this->backend === false) {
                    $func = 'click';
                    $result[] = self::func($this->{$func}, call_user_func([$this, $func]));
                    $result[] = "{$this->click}();";
                }
                $timeout = mt_rand(250, 450);
                $result[] = "setTimeout({$this->tracker},{$timeout});";
                $js = implode('', $result);
                if ($this->backend === false) {
                    return JSMin::minify('(function(){' . $js . '})();');
                }
                $this->inject['body'][] = '<script type="text/javascript">' . JSMin::minify($js) . '</script>';
                return $this->backend();
            }

            public static function run(array $array, &$self = null) {
                $self = new self();
                foreach ($array as $var => $value) {
                    $self->{$var} = $value;
                }

                return $self->method();
            }

        }
        