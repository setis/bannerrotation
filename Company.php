<?php

class Company {

    public static function getbyHost($host) {
        $collection = Env::$banner->selectCollection(Env::$config['banner']['db'], Env::$company);
        return $collection->findOne(['host'=>$host]);
    }
    public static function getbyHostID($host_id) {
        $collection = Env::$banner->selectCollection(Env::$config['banner']['db'], Env::$company);
        return $collection->findOne(['host_id'=>$host_id]);
    }

}
