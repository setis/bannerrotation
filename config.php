<?php

return [
    'static' => [
        'dsn' => 'mysql:host=127.0.0.1;port=3306;dbname=banner;charset=utf8',
        'login' => 'root',
        'password' => '123123',
        'options' => [
            PDO::ATTR_PERSISTENT => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]
    ],
    'banner' => [
        'server' => 'mongodb://localhost',
        'options' => [],
        'db' => 'banner'
    ],
    'collection' => [
        'company' => 'company',
        'traffic' => 'domain'
    ],
    'redis' => [
        'server' => '127.0.0.1',
        'port' => 6379
    ],
    'mcrypt' => [
        'alg' => MCRYPT_RIJNDAEL_128,
        'mode' => MCRYPT_MODE_ECB,
        'key' => __DIR__ . '/key',
        'secret' => '1231231231231231',
    ],
    'time_rest_uniq' => 36000, //период уникальности для rest в секундах
    'time_ajax_uniq' => 60, //период уникальности для ajax в секундах
    'time_live' => 15, //сколько живет проклада в секундах
    'time_redirect' => 15, //сколько ждать с выдачи кода до перехода в секундах
    'layer_time' => 36000, //период выдачи прокладки в секундах
];
