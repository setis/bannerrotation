<?php

class Browscap {

    /**
     * файл с временим и версий последнего обновления
     * @var string
     */
    public $file;

    /**
     * откуда скачиваем свежую базу
     */
    const url_base = 'http://browscap.org/stream?q=BrowsCapJSON';

    /**
     * откуда узание какая версия установлена
     */
    const url_version = 'http://browscap.org/version-number';

    /**
     * время последнего обновления
     * @var int unixstamp 
     */
    public $time;

    /**
     *
     * @var int
     */
    public $version;

    /**
     *
     * @var array
     */
    public $browcap;

    /**
     *
     * @var int
     */
    public $release;

    /**
     *
     * @var PDO
     */
    public $PDO;

    /**
     *
     * @var настройки к соединение с базой
     */
    public $dsn = 'mysql:host=127.0.0.1;port=3306;dbname=banner;charset=utf8';

    /**
     *
     * @var string
     */
    public $login = 'root';

    /**
     *
     * @var string
     */
    public $password = 't492t29th9rTERY2ht';

    /**
     *
     * @var array
     */
    public $options = [
        PDO::ATTR_PERSISTENT => true,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ];

    public function __construct($file = null) {
        if ($file === null) {
            $this->file = __DIR__ . DIRECTORY_SEPARATOR . 'cfg.json';
        } else {
            $this->file = $file;
        }
    }

    public function cfgLoad() {
        if (!file_exists($this->file)) {
            $this->version = 0;
            return;
        }
        foreach (json_decode(file_get_contents(self::$file), true) as $var => $value) {
            $this->{$var} = $value;
        }
    }

    public function cfgSave() {
        file_put_contents($this->file, json_encode([
            'version' => $this->release,
            'time' => time(),
            'dsn' => $this->dsn,
            'login' => $this->login,
            'password' => $this->password,
            'options' => $this->options
        ]));
    }

    public function cmd_update() {
        $this->release();
        $this->cfgLoad();
        if ($this->release > $this->version) {
            $this->load();
            $this->pdo();
            $this->create();
            $this->records();
        }
    }

    public function release() {
        $this->release = (int) file_get_contents(self::url_version);
    }

    public function load() {
        $this->browscap = json_decode(file_get_contents(self::url_base), true);
        unset($this->browscap['comments']);
        $this->release = $this->browscap['GJK_Browscap_Version']['Version'];
        unset($this->browscap['GJK_Browscap_Version']);
    }

    public function pdo() {
        try {
            $this->PDO = new PDO($this->dsn, $this->login, $this->password, $this->options);
        } catch (PDOException $e) {
            echo "Error!: " . $e->getMessage() . "<br/>";
            exit();
        }
    }

    public function analize() {
        $fields = [];
        foreach ($this->browscap['DefaultProperties'] as $arr) {
            foreach ($arr as $k => $v) {
                $type = gettype($v);
                switch ($type) {
                    case 'boolean':
                        $fields[$k] = $type;
                        break;

                    default:
                        break;
                }
            }
        }
        $fields = array_keys($this->browscap['DefaultProperties']);
        foreach ($this->browscap as $arr) {
            
        }
    }

    public function create() {
        try {
            $db = $this->PDO->exec("CREATE TABLE `browscap` (
  `id` varbinary(767) NOT NULL COMMENT 'md5(\$name)strlen(\$name)',
  `hash` binary(16) NOT NULL,
  `size` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Comment` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Browser` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Parent` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Version` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MajorVer` int(10) unsigned DEFAULT NULL,
  `MinorVer` int(10) unsigned DEFAULT NULL,
  `Platform` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Platform_Version` float(5,4) DEFAULT NULL,
  `Alpha` bit(1) DEFAULT NULL,
  `Beta` bit(1) DEFAULT NULL,
  `Win16` bit(1) DEFAULT NULL,
  `Win32` bit(1) DEFAULT NULL,
  `Win64` bit(1) DEFAULT NULL,
  `Frames` bit(1) DEFAULT NULL,
  `IFrames` bit(1) DEFAULT NULL,
  `Tables` bit(1) DEFAULT NULL,
  `BackgroundSounds` bit(1) DEFAULT NULL,
  `Cookies` bit(1) DEFAULT NULL,
  `ActiveXControls` bit(1) DEFAULT NULL,
  `VBScript` bit(1) DEFAULT NULL,
  `JavaApplets` bit(1) DEFAULT NULL,
  `JavaScript` bit(1) DEFAULT NULL,
  `isMobileDevice` bit(1) DEFAULT NULL,
  `isSyndicationReader` bit(1) DEFAULT NULL,
  `isTablet` bit(1) DEFAULT NULL,
  `AolVersion` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `CssVersion` tinyint(1) DEFAULT NULL,
  `Crawler` bit(1) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
        } catch (PDOException $e) {
            if ($db->errorInfo()[1] === 1050) {
                return;
            }
            echo "Error!: " . $e->getMessage() . "<br/>";
            exit;
        }
    }

    public function records() {

        foreach ($this->browscap as $ua => $arr) {
            $arr = json_decode($arr, true);
            $hash = md5($ua);
            $size = strlen($ua);
            $data[':name'] = $ua;
            $data[':hash'] = $hash;
            $data[':size'] = $size;
            $data[':id'] = $hash . dechex($size);
            $fields = implode(',', array_map(function($value) {
                        return '`' . $value . '`';
                    }, array_keys($arr)));
            $values = implode(',', array_map(function($value) {
                        return '\'' . $value . '\'';
                    }, array_values($arr)));

            $sql = "INSERT INTO `browscap` (`id`, `hash`, `size`, `name`, $fields )VALUES (UNHEX(:id), UNHEX(:hash), :size, :name, $values);";
            $db = $this->PDO->prepare($sql);
            try {
                $db->execute($data);
            } catch (PDOException $e) {
                echo "Error!: " . $e->getMessage() . "<br/>\n";
                echo "Trace!: " . $e->getTraceAsString() . "<br/>\n";
                exit;
            }
        }
    }

}

$browscap = new Browscap();
$browscap->cmd_update();

