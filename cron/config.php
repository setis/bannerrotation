<?php

return [
    'avdetect' => '7h99s43b60720a6ebmv56a0d04a201nb76yv4aa0',
    'db' => [
        'server' => 'mongodb://localhost',
        'options' => [],
        'db' => 'banner',
        'collection' => [
            'ip' => 'ip',
            'flash' => 'flash',
            'domain' => 'domain',
            'company' => 'company'
        ],
    ],
    'static' => [
        'dsn' => 'mysql:host=127.0.0.1;port=3306;dbname=banner;charset=utf8',
        'login' => 'root',
        'password' => '123123',
        'options' => [
            PDO::ATTR_PERSISTENT => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]
    ],
    'prefix' => [
        'traffic' => 't_'
    ],
    'traffic_time_live' => 60//время житья данных о переходах
];
