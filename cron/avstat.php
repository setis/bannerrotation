<?php

final class avstat {

    const
            adult = 1,
            noadult = 0;
    const
            parrent = '/drweb|dr\.web|sophos|mywot|VBA32|googlesb|kis2013|kisksn|nod|trend|avast|avira|bitdef|avg|panda|kaspersky|mcafee|NO_DATA/i';
    const
            method_exploits = '&exploits=true',
            method_files = '&files=true',
            method_domain = '&domain=true',
            method_runtime = '&runtime=true',
            method_queue = '&queue=true';

    /**
     *
     * @var resource Curl
     */
    protected $curl;

    /**
     *
     * @var array
     */
    public $cfg;

    /**
     *
     * @var MongoDB
     */
    public $db;

    /**
     *
     * @var PDO
     */
    public $pdo;

    public function __construct(array $config = null) {
        if ($config !== null) {
            $this->setConfig($config);
        }
        $this->curl = curl_init();
    }

    public function setConfig(array $cfg) {
        $this->cfg = $cfg;
        if (!empty($cfg['advert'])) {
            $this->cfg['key'] = $cfg['advert'];
        } elseif (!empty($cfg['key'])) {
            $this->cfg = $cfg['key'];
        }
        if (!empty($cfg['db']) && is_array($cfg['db'])) {
            $this->cfg['db'] = $cfg['db'];
        }
        if (!empty($cfg['static']) && is_array($cfg['static'])) {
            $this->cfg['static'] = $cfg['static'];
        }
        $this->key = &$this->cfg['key'];
    }

    public function __destruct() {
        curl_close($this->curl);
    }

    protected function query($type, $get) {
        switch ($type) {
            case self::adult:
                $url = $this->cfg['api']['adult'];
                break;
            case self::noadult:
                $url = $this->cfg['api']['noadult'];
                break;

            default:
                throw new Exception('not type api');
        }
        curl_setopt_array($this->curl, [
            CURLOPT_HEADER => 0,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url . $get
        ]);
        $response = curl_exec($this->curl);
        if (($error = curl_errno($this->curl)) > 0) {
            throw new Exception('function: ' . __FUNCTION__ . 'curl error code:' . $error . ' msg:' . curl_error($this->curl), $error);
        }
        return $response;
    }

    public function api($type, $method) {
        try {
            constant(__CLASS__ . '::' . $method);
        } catch (Exception $e) {
            throw new Exception('not method const ' . __CLASS__ . '::' . $method, $e->getCode(), $e);
        }
        $this->query($type, $method);
    }

    /**
     * 
     * @return MongoDb
     */
    public function db() {
        if ($this->db === null) {
            $cfg = $this->cfg['db'];
            $this->db = new MongoClient($cfg['server'], $cfg['options']);
        }
        return $this->db;
    }

    /**
     * 
     * @return PDO
     */
    public function pdo() {
        if ($this->pdo === null) {
            $cfg = $this->cfg['static'];
            $this->pdo = new PDO($cfg['dsn'], $cfg['login'], $cfg['password'], $cfg['options']);
        }
        return $this->pdo;
    }

    /**
     * 
     * @return MongoCollection
     */
    public function collection($collection) {
        return $this->db()->selectCollection($this->cfg['db']['db'], $this->cfg['db']['collection'][$collection]);
    }

    public function log(array $data) {
        list($pdo, $table) = $this->select(self::db_static, 'avstat');
        $db = $pdo->prepare("INSERT INTO $table (date, type, domain, file, exploit, heur, runtime) VALUES (NOW(), '" . $type . "', '" . $domain . "', '" . $file . "', '" . $expl . "', '" . $heur . "', '" . $runtime . "')");
        $db->execute($data);
    }

    public function reason(array $data) {
        if (count($data) === 2) {
            
        } else {
            
        }
    }

    public function run() {
        $reason = false;
        foreach ([self::noadult, self::adult] as $type) {
            foreach (['exploits', 'files', 'domain', 'runtime', 'queue'] as $var) {
                $response = ${$var} = $this->api($type, 'method_' . $var);
                if ($response != '0' && preg_match(self::parrent, $response)) {
                    switch ($var) {
                        case 'exploits':
                            $reason .= "<br>Exploit: $response";
                            break;
                        case 'files':
                            $reason .= "<br>File: $response";
                            break;
                        case 'domain':
                            $reason .= "<br>Domain: $response";
                            break;
                        case 'queue':
                            $reason .= "<br>Heuristic analysis detects: $response";
                            break;
                        case 'runtime':
                            $reason .= "<br>AVScan KIS: $response";
                            break;
                    }
                }
            }
            if ($reason) {
                $collection = $this->collection('domain');
                $arr = $collection->find([
                    'reason' => $reason,
                    'type' => $type
                ]);
                
            }
            $expl_array = json_decode($exploits, true);

            if (is_array($expl_array)) {
                foreach (array_keys($expl_array) as $val) {
                    if (preg_match('/Java/', $val))
                        $this->query($type, '&disablespl=1&spl=Java');
                    if (preg_match('/Flash/', $val))
                        $this->query($type, '&disablespl=1&spl=Flash');
                    if (preg_match('/Silverlight/', $val))
                        $this->query($type, '&disablespl=1&spl=Silverlight');
                }
            }
        }
    }

}
