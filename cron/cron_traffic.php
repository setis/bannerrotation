<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set("display_startup_errors", 1);
ini_set("log_errors", 1);
ini_set('memory_limit', '-1');
$cfg = require __DIR__ . '/config.php';
if (!empty($cfg['mail']['error'])) {
    $mail = $cfg['mail']['error'];
} else {
    $mail = false;
}
try {
    $db = new MongoClient($cfg['db']['server'], $cfg['db']['options']);
    $collectin = $db->selectCollection($cfg['db']['db'], $cfg['db']['collection']['company']);
    $result = $collectin->find();
    $time = time() - $cfg['traffic_time_live'];
    foreach ($result as $arr) {
        $collection1 = $db->selectCollection($cfg['db']['db'], $cfg['prefix']['traffic'] . (string) $arr['host_id']);
        $result1 = $collection1->find([
            '$or' => [
                '$lte' => [
                    'time' => $time
                ],
                'flag' => true
            ]
        ]);
        foreach($result1 as $arr1){
            $collection1->remove(['_id'=>$arr1['_id']]);
        }
    }
} catch (Exception $e) {
    echo "Error!: " . $e->getMessage() . "<br/>";
    if ($mail) {
        @mail($mail['to'], $mail['subject'], $e->getTraceAsString());
    }
}