<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set("display_startup_errors", 1);
ini_set("log_errors", 1);
ini_set('memory_limit', '-1');

final class Avdetect {

    const
            server = 'https://avdetect.com/api/',
            type_domain = 'domain',
            type_file = 'file';

    /**
     *
     * @var MongoDB
     */
    public $db;

    /**
     *
     * @var MongoCollection
     */
    public $collection;

    /**
     *
     * @var string
     */
    protected $key;

    /**
     *
     * @var resource Curl
     */
    protected $curl;

    /**
     *
     * @var array
     */
    public $cfg;

    public function __construct(array $config = null) {
        if ($config !== null) {
            $this->setConfig($config);
        }
        $this->curl = curl_init();
    }

    public function __destruct() {
        curl_close($this->curl);
    }

    public function setConfig(array $cfg) {
        $this->cfg = $cfg;
        if (!empty($cfg['advert'])) {
            $this->cfg['key'] = $cfg['advert'];
        } elseif (!empty($cfg['key'])) {
            $this->cfg = $cfg['key'];
        }
        if (!empty($cfg['db']) && is_array($cfg['db'])) {
            $this->cfg['db'] = $cfg['db'];
        }
        $this->key = &$this->cfg['key'];
    }

    /**
     * 
     * @return MongoDB
     */
    public function db() {
        if ($this->db === null) {
            $this->db = new MongoClient($this->cfg['db']['server'], $this->cfg['db']['options']);
        }
        return $this->db;
    }

    /**
     * 
     * @return MongoCollection
     */
    public function collection($collection) {
        if ($this->collection === null) {
            $this->collection = $this->db()->selectCollection($this->cfg['db']['db'], $this->cfg['db']['collection'][$collection]);
        }
        return $this->collection;
    }

    public function api($data, $type = self::type_domain) {
        curl_setopt_array($this->curl, [
            CURLOPT_POST => 1,
            CURLOPT_VERBOSE => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 15,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_URL => self::server,
            CURLOPT_POSTFIELDS, [
                'api_key' => $this->key,
                'check_type' => $type,
                'data' => ($type === self::type_file) ? base64_encode($data) : $data
            ]
        ]);
        $response = curl_exec($this->curl);
        if (($error = curl_errno($this->curl)) > 0) {
            throw new Exception('function: ' . __FUNCTION__ . 'curl error code:' . $error . ' msg:' . curl_error($this->curl), $error);
        }
        $result = json_decode($response, true);
        if ($result === false) {
            $error = json_last_error();
            throw new Exception('json_decode error code:' . $error . ' msg:' . json_last_error_msg(), $error);
        }
        return $result;
    }

    public function checkDns($host) {
        curl_setopt_array($this->curl, [
            CURLOPT_HEADER => 0,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://' . $host . '/__check.html'
        ]);
        $response = curl_exec($this->curl);
        if (($error = curl_errno($this->curl)) > 0) {
            throw new Exception('function: ' . __FUNCTION__ . 'curl error code:' . $error . ' msg:' . curl_error($this->curl), $error);
        }
        return (trim($response) === 'ok');
    }

    public function domain($domain) {
        $ip = gethostbyname($domain);
        $dns = $this->checkDns($domain);
        $result = $this->api($domain)[0];
        if (count($result['result']) > 20) {
            $link = $result['link'];
            $total = (int) $result['totalavs'];
            $detect = (int) $result['detectavs'];
        } else {
            $link = null;
            $total = null;
            $detect = null;
        }
        $result2 = $this->collection(__FUNCTION__)->findOne(['domain' => $domain]);
        $data = [
            'dns' => $dns,
            'ip' => $ip,
            'time' => time(),
            'link' => $link,
            'total' => $total,
            'detect' => $detect
        ];
        if ($result2 === null) {
            $this->collection->update(['_id' => $result2['_id']], $data);
        }
    }

    public function ip($ip) {
        $dns = $this->checkDns($ip);
        $result = $this->api($ip)[0];
        if (count($result['result']) > 20) {
            $link = $result['link'];
            $total = (int) $result['totalavs'];
            $detect = (int) $result['detectavs'];
        } else {
            $link = null;
            $total = null;
            $detect = null;
        }
        $result2 = $this->collection(__FUNCTION__)->findOne(['ip' => $ip]);
        $data = [
            'dns' => $dns,
            'time' => time(),
            'link' => $link,
            'total' => $total,
            'detect' => $detect
        ];
        if ($result2 === null) {
            $this->collection->update(['_id' => $result2['_id']], $data);
        }
    }

    public function flash($path) {
        $error = 0;
        if (!file_exists($path)) {
            $error = 1;
        }
        if (is_readable($path)) {
            $error = 2;
        }
        if ($error === 0) {
            $result = $this->api(file_get_contents($path), self::type_file)[0];
        }
        if ($error === 0 && count($result['result']) > 20) {
            $link = $result['link'];
            $total = (int) $result['totalavs'];
            $detect = (int) $result['detectavs'];
        } else {
            $link = null;
            $total = null;
            $detect = null;
        }
        $result2 = $this->collection(__FUNCTION__)->findOne(['path' => $path]);
        $data = [
            'time' => time(),
            'link' => $link,
            'total' => $total,
            'detect' => $detect,
            'error' => $error
        ];
        if ($result2 === null) {
            $this->collection->update(['_id' => $result2['_id']], $data);
        }
    }

}

$cfg = require __DIR__ . '/config.php';
if (!empty($cfg['mail']['error'])) {
    $mail = $cfg['mail']['error'];
} else {
    $mail = false;
}
try {
    $avdetect = new Avdetect($cfg);
    call_user_func([&$avdetect, $_SERVER['argv'][1]], $_SERVER['argv'][2]);
} catch (Exception $e) {
    if ($mail) {
        @mail($mail['to'], $mail['subject'], $e->getTraceAsString());
    }
}
