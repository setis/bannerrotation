<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set("display_startup_errors", 1);
ini_set("log_errors", 1);
ini_set('memory_limit', '-1');

$cfg = require __DIR__ . '/config.php';
if (!empty($cfg['mail']['error'])) {
    $mail = $cfg['mail']['error'];
} else {
    $mail = false;
}
$pool = (!empty($cfg['thread'])) ? (int) $cfg['thread'] : 20;
try {
    $db = new MongoClient($cfg['db']['server'], $cfg['db']['options']);
    $collectin = $db->selectCollection($cfg['db']['db'], $cfg['db']['collection'][$_SERVER['argv'][1]]);
    $result = $collectin->find();

    switch ($_SERVER['argv'][1]) {
        case 'domain':
            $field = 'domain';
            break;
        case 'ip':
            $field = 'ip';
            break;
        case 'flash':
            $field = 'path';
            break;
    }
    $signal = function()use(&$result, $field) {
        if (count($result) !== 0) {
            $arr = array_shift($result);
            pcntl_exec(__DIR__ . '/avdetect.php', [$_SERVER['argv'][1], $arr[$field]]);
        }
    };
    pcntl_signal(SIGTERM, $signal);
    pcntl_signal(SIGHUP, $signal);
    pcntl_signal(SIGINT, $signal);
    while ($poll--) {
        $arr = array_shift($result);
        pcntl_exec(__DIR__ . '/avdetect.php', [$_SERVER['argv'][1], $arr[$field]]);
    }
} catch (Exception $e) {
    echo "Error!: " . $e->getMessage() . "<br/>";
    if ($mail) {
        @mail($mail['to'], $mail['subject'], $e->getTraceAsString());
    }
}