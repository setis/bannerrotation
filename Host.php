<?php

class Host {

    public static function add($host) {
        $db = Env::$static->prepare('INSERT INTRO `host` (host) VALUES(:host);');
        try {
            $db->execute([':host' => $host]);
        } catch (PDOException $e) {
            return false;
        }
        return $db->lastInsertId();
    }

}
