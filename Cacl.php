<?php

class Cacl {

    const day = 86400;
    const week = 604800;

    public static function week($time = null) {
        if ($time === null) {
            $time = time();
        }
        list($w, $wn) = explode(':', date('W:w', $time));
        if ($wn === 1) {
            $result[] = date('y_n_j');
        } else {
            while ($wn-- !== 0) {
                $time-=self::day;
                $result[] = date('y_n_j', $time);
            }
        }
        return $result;
    }

    public static function month($time = null) {
        if ($time === null) {
            $time = time();
        }
        list($d, $m, $y) = explode(':', date('t:n:y', $time));
        list($d1, $m1, $y1) = explode(':', date('j:n:y'));
        if ($y === $y1 && $m === $m1 && $d > $d1) {
            $d = $d1;
        }
        while ($d !== 0) {
            $result[] = "{$y}_{$m}_{$d}";
            $d--;
        }
        return $result;
    }

    public static function period($start, $end = null) {
        if ($end === null) {
            $end = time();
        }
        list($data['d'], $data['m'], $data['y']) = explode(':', date('j:n:y', $start));
        list($data['d1'], $data['m1'], $data['y1']) = explode(':', date('j:n:y', $end));
        foreach ($data as $k => $v) {
            ${$k} = (int) $v;
        }
        $f = true;
        $result = [];
        while ($y <= $y1) {
            while ($m <= 12) {
                if ($m1 === $m && $y1 === $y) {
                    $t = $d1;
                    $d = 1;
                } else {
                    if ($f) {
                        $f = false;
                    } else {
                        $d = 1;
                    }
                    $t = date('t', strtotime("$y-$m-1"));
                }
                while ($d <= $t) {
                    $result[] = $y . '_' . $m . '_' . $d;
                    $d++;
                }
                if ($m1 === $m && $y1 === $y) {
                    break;
                }
                $m++;
            }
            $m = 1;
            $y++;
        }
        return $result;
    }

}
