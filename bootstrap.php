<?php

if (isset($debug) && $debug) {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    ini_set("display_startup_errors", 1);
    ini_set("log_errors", 1);
    set_time_limit(10);
    ini_set('memory_limit', '-1');
} else {
    ini_set('memory_limit', '-1');
}

class Env {

    /**
     *
     * @var PDO
     */
    public static $static;

    /**
     *
     * @var MongoClient
     */
    public static $banner;

    /**
     *
     * @var Redis
     */
    public static $redis;

    /**
     *
     * @var array
     */
    public static $config;

    /**
     *
     * @var string dir
     */
    public static $tmpl;

    /**
     *
     * @var string
     */
    public static $company;

    /**
     *
     * @var string
     */
    public static $traffic;

}

Env::$config = require __DIR__ . '/config.php';
Env::$tmpl = __DIR__ . '/tmpl/';
try {
    Env::$static = new PDO(Env::$config['static']['dsn'], Env::$config['static']['login'], Env::$config['static']['password'], Env::$config['static']['options']);
} catch (PDOException $e) {
    echo "Error!: " . $e->getMessage() . "<br/>";
    exit();
}
try {
    Env::$banner = new MongoClient(Env::$config['banner']['server'], Env::$config['banner']['options']);
} catch (MongoConnectionException $e) {
    echo "Error!: " . $e->getMessage() . "<br/>";
    exit();
}
foreach (Env::$config['collection'] as $collection => $value) {
    Env::${$collection} = $value;
}

function redis_connect() {
    global $redis;
    if (Env::$redis !== null) {
        return;
    }
    Env::$redis = new Redis();
    Env::$redis->pconnect(Env::$config['redis']['server'], Env::$config['redis']['port']);
    $redis = &Env::$redis;
    register_shutdown_function(function() {
        Env::$redis->close();
    });
}

spl_autoload_register(function ($class_name) {
    $file = str_replace("\\", "/", $class_name) . '.php';
    if (file_exists($file)) {
        include $file;
    }
}, true);

require __DIR__ . '/gjs/BootStrap.php';
