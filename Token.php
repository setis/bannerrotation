<?php

class Token {

    const plode = '||';

    public static function create($host_id, $host, $ip, $time, $ua) {
        return $time . self::plode . $ip . self::plode . $host . self::plode . $host_id . self::plode . $ua;
    }

    public static function parse($token) {
        $v = explode(self::plode, $token);
        return array_combine(['time', 'ip', 'host', 'host_id', 'ua'], $v);
    }

    public static function is($host_id, $time, $ip) {
        $db = Env::$static->prepare('SELECT count(*) FORM `t_:host` WHERE id = :token LIMIT 1;');
        try {
            $db->execute([':host' => $host_id, ':token' => self::index($time, $ip)]);
        } catch (PDOException $e) {
            return false;
        }
        return ($db->rowCount());
    }

    public static function index($time, $ip) {
        return decbin($time) . inet_pton($ip);
    }

    public static function create_key() {
        $iv_size = mcrypt_get_iv_size(Env::$config['mcrypt']['alg'], Env::$config['mcrypt']['mode']);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        file_put_contents(Env::$config['mcrypt']['key'], $iv);
    }

    public static function load_key() {
        return file_get_contents(Env::$config['mcrypt']['key']);
    }

    public static function decrypt($token) {
        $iv = self::load_key();
        return rtrim(mcrypt_decrypt(Env::$config['mcrypt']['alg'], Env::$config['mcrypt']['secret'], base64_decode($token), Env::$config['mcrypt']['mode'], $iv), "\0");
    }

    public static function crypt($token) {
        $iv = self::load_key();
        return base64_encode(mcrypt_encrypt(Env::$config['mcrypt']['alg'], Env::$config['mcrypt']['secret'], $token, Env::$config['mcrypt']['mode'], $iv));
    }

}
