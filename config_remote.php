<?php

return [
    'static' => [
        'dsn' => 'mysql:host=127.0.0.1;port=3306;dbname=banner;charset=utf8',
        'login' => 'root',
        'password' => 't492t29th9rTERY2ht',
        'options' => [
            PDO::ATTR_PERSISTENT => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]
    ],
    'banner' => [
        'server' => 'mongodb://localhost/banner',
        'options' => [],
        'db' => 'banner'
    ],
    'collection' => [
        'company' => 'company',
        'traffic' => 'traffic'
    ],
    'time_rest_uniq' => 36000,
    'time_ajax_uniq'=> 60,
    'mcrypt' => [
        'alg' => MCRYPT_RIJNDAEL_256,
        'mode' => MCRYPT_MODE_ECB,
        'key' => __DIR__ . '/key',
        'secret' => '1231231231231231',
    ]
];

