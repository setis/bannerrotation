<?php

class Layer {

    const
            not_adalt = 0,
            adalt = 1;

    public static function domain($host_id, $type) {
        $collection = Env::$banner->selectCollection(Env::$config['banner']['db'], Env::$traffic);
        $result = $collection->findOne([
            'host_id' => $host_id,
            'type' => $type,
            'used' => 0,
            'dns' => 1,
            'detect' => 0,
            '$lt' => [
                'total' => 1,
            ]
        ]);
        if ($result === null) {
            return false;
        }
        return $result['domain'];
    }

    public static function getHostId($domain) {
        $collection = Env::$banner->selectCollection(Env::$config['banner']['db'], Env::$traffic);
        $result = $collection->findOne(['domain' => $domain]);
        if ($result === null) {
            return false;
        }
        return (int)$result['host_id'];
    }

}
