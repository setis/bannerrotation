<?php defined('SYSPATH') or die('No direct script access.');

class Request extends Kohana_Request {

    public function redirect($url)
    {
        HTTP::redirect($url);
    }

}

