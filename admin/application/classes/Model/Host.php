<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Host extends ORM {

    protected $_table_name = 'host';
    protected $_db_group = 'static';

    public function get_all() {
        foreach ($this->find_all() as $v) {
            $result[$v->user_id] = $v->as_array();
        }
        return $result;
    }

    public function getUserbyHost($host) {
        foreach ($this->where('host', '=', $host)->find_all() as $v) {
            $result[$v->user_id] = $v->as_array();
        }
        return $result;
    }

    public function addHost($host, $user_id = nul) {
        if ($user_id === null) {
            $user_id = Auth::instance()->get_user()->id;
        }
        $this->host = $host;
        $this->user_id = $user_id;
        try {
            $this->create();
        } catch (Database_Exception $e) {
            if ($e->getCode() === 1062) {
                return false;
            } else {
                throw $e;
            }
        }
        return true;
    }

    public function deleteByHost($host) {
        $this->where('host', '=', $host)->limit(1)->delete();
    }

    public function deleteByHostId($host_id) {
        $this->where('host_id', '=', $host_id)->limit(1)->delete();
    }

    public function getUserById($user_id = null) {
        if ($user_id === null) {
            $user_id = Auth::instance()->get_user()->id;
        }
        foreach ($this->where('user_id', '=', $user_id)->find_all() as $v) {
            $result[] = $v->as_array();
        }
        return $result;
    }

}
