<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Company extends ODM {

    protected $_collection_name = 'company';
    protected $_db_group = 'banner';
    protected $_schema = [
        'host' => 'string',
        'host_id' => 'int',
        'user_id' => 'int',
        'global' => 'bool',
        'banner' => 'array',
        'allow_country' => 'array',
        'allow_browser' => 'array',
        'time_max' => 'int',
        'time_uniq' => 'int',
        'time_rest' => 'int',
    ];

    public function get_all() {
        foreach ($this->find_all() as $v) {
            $result[$v->user_id] = $v;
        }
        return $result;
    }

    public function getByUserId($user_id) {
        return $this->where('user_id', '=', $user_id)->find()->as_array();
    }

    public function get_banner($host_id) {
        return $this->load("{host_id:{$host_id}}");
    }

    public function run($host_id = '', $action_code = '') {
        if ($action_code == 0) {
            $this->run = $host_id;
            $this->status = $action_code;
        } else {
            $this->run = $host_id;
        }
        $this->upsert();
        return DB::query(Database::UPDATE, 'UPDATE ' . $this->campaigns_table_name . '
			SET ' . $sets . '
			WHERE id=:campaign_id')
                        ->param(':action_code', $action_code)
                        ->param(':campaign_id', $campaign_id)
                        ->execute();
    }

}
