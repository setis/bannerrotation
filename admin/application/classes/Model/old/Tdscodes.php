<?php defined('SYSPATH') or die('No direct script access.');

class Model_Tdscodes extends Model_Database
{
	protected $tdscodes_table_name = 'tds_codes';

	protected $stat_db = 'tds';

	public function update_tdscode($data)
	{
		DB::query(Database::UPDATE,
			'UPDATE ' . $this->tdscodes_table_name . '
			SET
				comment=:comment,
				date = now(),
				clean=0
			WHERE id=:id')
			->param(':comment', $data['comment'])
			->param(':id', $data['id'])
			->execute();
	}

	public function mark_tdscode($data)
	{
		DB::query(Database::UPDATE,
			'UPDATE ' . $this->tdscodes_table_name . '
			SET clean=1
			WHERE id=:id')
			->param(':id', $data["id"])
			->execute();
		return true;
	}

	public function get_all_tdscodes()
	{
		return DB::query(Database::SELECT,
			'SELECT `id`,`name`,`date`,`type`,`coder`,`comment`,`clean`
			FROM ' . $this->tdscodes_table_name . '
			ORDER BY id')
			->execute()
			->as_array();
	}

}
