<?php defined('SYSPATH') or die('No direct script access.');

class Model_Banners extends Model_Database
{
	protected $stat_table_name = 'incoming';
	protected $banners_table_name = 'tds_banners';
	protected $domains_table_name = 'domains';
	protected $campaigns_table_name = 'tds_campaigns';
	protected $users_table_name = 'users';

	protected $stat_db = 'tds';

	public function get_all_users()
	{
		return DB::query(Database::SELECT,
			'SELECT id user_id, username
			FROM ' . $this->users_table_name . '
			ORDER BY username')
			->execute()
			->as_array();
	}

	public function get_all_banners($user_id = 0)
	{
		return DB::query(Database::SELECT,
			'SELECT B.id id, B.file_name file_name, B.height height, B.width width, B.type type, IFNULL(C.id, 0) cid, U.username username
			FROM ' . $this->banners_table_name . ' B LEFT JOIN ' . $this->campaigns_table_name . ' C ON B.id = C.banner_id, ' . $this->users_table_name . ' U
			WHERE '.(Auth::instance()->logged_in("admin")?"1 = 1 AND":"B.user_id = :user_id AND").' U.id = B.user_id
			GROUP BY B.id')
			->param(':user_id', $user_id)
			->execute()
			->as_array();
	}

	public function get_all_user_banners($username = '')
	{
		return DB::query(Database::SELECT,
			'SELECT B.id id, B.file_name file_name, B.height height, B.width width, B.type type, IFNULL(C.id, 0) cid, U.username username
			FROM ' . $this->banners_table_name . ' B LEFT JOIN ' . $this->campaigns_table_name . ' C ON B.id = C.banner_id, ' . $this->users_table_name . ' U
			WHERE U.username = :username AND U.id = B.user_id
			GROUP BY B.id')
			->param(':username', $username)
			->execute()
			->as_array();
	}

	public function isset_banner($file_name)
	{
		return DB::query(Database::SELECT,
			'SELECT file_name FROM ' . $this->banners_table_name . '
			WHERE file_name = :file_name')
			->param(':file_name', $file_name)
			->execute()
			->as_array();
	}

	public function save_banner($banner)
	{
		return DB::query(Database::INSERT,
			'INSERT INTO ' . $this->banners_table_name . '
				(file_name, user_id, width, height, type, data)
			VALUES
				(:file_name, :user_id, :width, :height, :type, :data)')
			->param(':file_name', $banner['name'])
			->param(':user_id', $banner['user_id'])
			->param(':width', $banner['width'])
			->param(':height', $banner['height'])
			->param(':type', $banner['type'])
			->param(':data', $banner['data'])
			->execute();
	}

	public function get_banner($banner_id, $user_id)
	{
		return DB::query(Database::SELECT,
			'SELECT type, data
			FROM ' . $this->banners_table_name . '
			WHERE id = :banner_id AND '.(Auth::instance()->logged_in('admin')?'1 = 1':'user_id = :user_id').'
			LIMIT 1')
			->param(':banner_id', $banner_id)
			->param(':user_id', $user_id)
			->execute()
			->as_array();
	}

	public function del_banner($data)
	{
		DB::query(Database::INSERT,
			'DELETE FROM ' . $this->banners_table_name . '
			WHERE id = :id')
			->param(':id', $data['id'])
			->execute();
	}
}
