<?php defined('SYSPATH') or die('No direct script access.');

class Model_Users extends Model_Database
{
	protected $stat_table_name = 'incoming';
	protected $banners_table_name = 'tds_banners';
	protected $domains_table_name = 'domains';
	protected $campaigns_table_name = 'tds_campaigns';
	protected $users_table_name = 'users';
	protected $roles_table_name = 'roles';
	protected $logs_table_name = 'logs';
	protected $roles_users_table_name = 'roles_users';
	protected $avstat_table_name = 'avstat_log';

	protected $stat_db = 'tds';

	public function get_all_domains()
	{
		return DB::query(Database::SELECT,
			'SELECT D.name, D.user_id user_id, D.id d_id, U.id u_id, U.username username
			FROM ' . $this->domains_table_name . ' D, '. $this->users_table_name  .' U
			WHERE U.id = D.user_id AND D.is_delete = 0 
			ORDER BY U.username, D.name')
			->execute()
			->as_array();
	}

	public function get_all_users()
	{
		return DB::query(Database::SELECT,
			"SELECT 
				* 
			FROM
				$this->users_table_name
			WHERE 
				username != 'admin'
			ORDER BY
				username")
			->execute()
			->as_array();
	}

	public function edit_user($data)
	{
		$arr = DB::query(Database::SELECT, 'SELECT id FROM ' . $this->users_table_name . ' WHERE username=:username AND (id <> :id) LIMIT 1')
			->param(':id', $data['id'])
			->param(':username', $data['username'])
			->execute()
			->as_array();

		if (empty($arr))
		{
			if (!empty($data['password'])) $data['password'] = Auth::instance()->hash_password($data['password']);

			DB::query(Database::UPDATE,
				'UPDATE ' . $this->users_table_name . '
				SET username=:username'.(!empty($data['password'])?(",password='".$data['password']."'"):("")).'
				WHERE id=:id')
				->param(':id', $data['id'])
				->param(':username', $data['username'])
				->execute();
		}
	}

	public function add_user($data)
	{
		$arr = DB::query(Database::SELECT,
			'SELECT id
			FROM ' . $this->users_table_name . '
			WHERE username=:username LIMIT 1')
			->param(':username', $data['username'])
			->execute()
			->as_array();

		if (empty($arr))
		{
			$data['password'] = Auth::instance()->hash_password($data['password']);
			list ($new_uid, $res) = DB::query(Database::INSERT,
				'INSERT INTO ' . $this->users_table_name . ' (email,username,password,logins,last_login,av_ignore)
				VALUES (:email,:username,:password,0,0,:av_ignore)')
				->param(':email', $data['username'] . '@local.loc')
				->param(':username', $data['username'])
				->param(':password', $data['password'])
				->param(':av_ignore', '{"domain":[],"file":[],"exploit":[],"heur":[]}')
				->execute();

				DB::query(Database::INSERT,
					'INSERT INTO ' . $this->roles_users_table_name . ' (user_id, role_id)
					VALUES (:uid, 1)')
					->param(":uid", $new_uid)
					->execute();
		}
	}

	public function del_user($data)
	{
		DB::query(Database::DELETE, 'DELETE FROM ' . $this->users_table_name . ' WHERE id=:id')
			->param(':id', $data["id"])
			->execute();
		DB::query(Database::DELETE, 'DELETE FROM ' . $this->campaigns_table_name . ' WHERE user_id=:user_id')
			->param(':user_id', $data["id"])
			->execute();
		DB::query(Database::DELETE, 'DELETE FROM ' . $this->banners_table_name . ' WHERE user_id=:user_id')
			->param(':user_id', $data["id"])
			->execute();
		DB::query(Database::DELETE, 'DELETE FROM ' . $this->avstat_table_name . ' WHERE user_id=:user_id')
			->param(':user_id', $data["id"])
			->execute();
		DB::query(Database::DELETE, 'DELETE FROM ' . $this->logs_table_name . ' WHERE user_id=:user_id')
			->param(':user_id', $data["id"])
			->execute();
		DB::query(Database::DELETE, 'DELETE FROM ' . $this->roles_table_name . ' WHERE user_id=:user_id')
			->param(':user_id', $data["id"])
			->execute();
		DB::query(Database::DELETE, 'DELETE FROM ' . $this->domains_table_name . ' WHERE user_id=:user_id')
			->param(':user_id', $data["id"])
			->execute();
	}

}
