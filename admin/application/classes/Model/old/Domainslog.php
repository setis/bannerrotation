<?php defined('SYSPATH') or die('No direct script access.');

class Model_Domainslog extends Model_Database
{
	protected $ips_table_name = 'ips';
	protected $logs_table_name = 'logs';
	protected $users_table_name = 'users';
	protected $domains_table_name = 'domains';


	public function get_user_id($user)
	{
		return DB::query(Database::SELECT,
			'SELECT id 
			FROM ' . $this->users_table_name  .'
			 WHERE username = :user')
			 ->param(':user', $user)
			 ->execute()
			 ->as_array();
	}
	
	public function get_logs($user_id)
	{
			return DB::query(Database::SELECT,
			'SELECT L.*, D.*, I.*, L.id l_id, I.avd_detect i_avd_detect, I.avd_total i_avd_total, I.avd_link i_avd_link, D.avd_detect d_avd_detect, D.avd_total d_avd_total, D.avd_link d_avd_link, IFNULL(D.ip, I.ip) ip
            FROM ' . $this->logs_table_name . ' L
            LEFT JOIN ('. $this->domains_table_name  .' D
             LEFT JOIN '. $this->ips_table_name  .' I ON D.ip=I.ip) ON L.domain_id=D.id
            WHERE L.user_id = :user_id ORDER BY L.date_del')
			->param(':user_id', $user_id)
			->execute()
			->as_array();
	}

}
