<?php defined('SYSPATH') or die('No direct script access.');

class Model_Flash extends Model_Database
{
	protected $banners_table_name = 'tds_flash';

	public function get_all_banners()
	{
		return DB::query(Database::SELECT,
			'SELECT id, file_name, size, avd_detect, avd_total, last_check, avd_link from ' . $this->banners_table_name)
			->execute()
			->as_array();
	}

	public function isset_banner($file_name)
	{
		return DB::query(Database::SELECT,
			'SELECT file_name FROM ' . $this->banners_table_name . '
			WHERE file_name = :file_name')
			->param(':file_name', $file_name)
			->execute()
			->as_array();
	}

	public function save_banner($banner)
	{
		return DB::query(Database::INSERT,
			'INSERT INTO ' . $this->banners_table_name . '
				(file_name, size, data)
			VALUES
				(:file_name, :size, :data)')
			->param(':file_name', $banner['name'])
			->param(':size', $banner['size'])
			->param(':data', $banner['data'])
			->execute();
	}

	public function get_banner($banner_id, $user_id)
	{
		return DB::query(Database::SELECT,
			'SELECT type, data
			FROM ' . $this->banners_table_name . '
			WHERE id = :banner_id AND '.(Auth::instance()->logged_in('admin')?'1 = 1':'user_id = :user_id').'
			LIMIT 1')
			->param(':banner_id', $banner_id)
			->param(':user_id', $user_id)
			->execute()
			->as_array();
	}

	public function del_banner($data)
	{
		DB::query(Database::INSERT,
			'DELETE FROM ' . $this->banners_table_name . '
			WHERE id = :id')
			->param(':id', $data['id'])
			->execute();
	}
}
