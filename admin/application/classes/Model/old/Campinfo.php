<?php defined('SYSPATH') or die('No direct script access.');

class Model_Campinfo extends Model_Database
{
	protected $stat_table_name = 'incoming';
	protected $banners_table_name = 'tds_banners';
	protected $domains_table_name = 'domains';
	protected $campaigns_table_name = 'tds_campaigns';
	protected $users_table_name = 'users';

	protected $stat_db = 'tds';

	public function get_campaign($name = "")
	{
		return DB::query(Database::SELECT, 'SELECT C.id c_id, C.*, D.id d_id, D.*, C.campaign_domain_id campaign_domain_id, U.username username
			FROM ' . $this->campaigns_table_name . ' C
			LEFT JOIN '. $this->domains_table_name  .' D
			ON C.campaign_domain_id=D.id, '. $this->users_table_name  .' U WHERE U.id=C.user_id AND D.name=:name LIMIT 1')
			->param(':name', $name)
			->execute()
			->as_array();
	}

	public function get_short_stat($site = '')
	{
		$result = array();
		
		$site = DB::query(Database::SELECT, 'SELECT id FROM domains WHERE name=:site LIMIT 1')
			->param(':site', $site)
			->execute()
			->as_array();

		$site = current($site);
		$site = !empty($site["id"]) ? $site["id"] : 0;

		$sql = 'SELECT date a_date, SUM(`in`) snd_in, SUM(`out`) snd_out, SUM(`pre`) snd_pre FROM os WHERE site_id=:site GROUP BY date';

		$redis = new Redis();
		$redis->connect('127.0.0.1', 6379);
		
		$res = DB::query(Database::SELECT, $sql)
			->param(':site', $site)
			->execute($this->stat_db)
			->as_array();
		
		foreach ($res as $val)
		{
		    $val['snd_src'] = $redis->get('tds:source:' . $site . ':' . $val['a_date']);

		    $result[] = $val;
		}
		
		$redis->close();
		
		return $result;
	}

	public function get_detailed_stat($date, $site)
	{
		$a = array();

		$site = DB::query(Database::SELECT, 'SELECT id FROM domains WHERE name=:site LIMIT 1')
			->param(':date', $date)
			->param(':site', $site)
			->execute()
			->as_array();

		$site = current($site);
		$site = !empty($site["id"]) ? $site["id"] : 0;

		$a["br_name"] = DB::query(Database::SELECT, 'SELECT br_name c_name, br_vers, SUM(`in`) snd_in, SUM(`out`) snd_out, SUM(`pre`) snd_pre
						FROM browser
						WHERE site_id=:site AND date=:date GROUP BY br_name,br_vers')
			->param(':date', $date)
			->param(':site', $site)
			->execute($this->stat_db)
			->as_array();

		$a["os"] = DB::query(Database::SELECT, 'SELECT os c_name, `in` snd_in, `out` snd_out, `pre` snd_pre
						FROM os
						WHERE site_id=:site AND date=:date')
			->param(':date', $date)
			->param(':site', $site)
			->execute($this->stat_db)
			->as_array();

		$a["referer"] = DB::query(Database::SELECT, 'SELECT referer c_name, `in` snd_in, `out` snd_out, `pre` snd_pre
						FROM referer
						WHERE site_id=:site AND date=:date')
			->param(':date', $date)
			->param(':site', $site)
			->execute($this->stat_db)
			->as_array();

		$a["geo"] = DB::query(Database::SELECT, 'SELECT geo c_name, `in` snd_in, `out` snd_out, `pre` snd_pre
						FROM geo
						WHERE site_id=:site AND date=:date')
			->param(':date', $date)
			->param(':site', $site)
			->execute($this->stat_db)
			->as_array();

		return $a;
	}

}
