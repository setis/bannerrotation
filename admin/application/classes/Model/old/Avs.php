<?php defined('SYSPATH') or die('No direct script access.');

class Model_Avs extends Model_Database
{
	protected $stat_db = 'tds';

	public function get_all_avs()
	{
		return DB::query(Database::SELECT,
			'SELECT * FROM avs ORDER BY title')
			->execute()
			->as_array();
	}

	public function edit_av($data)
	{
		DB::query(Database::UPDATE,
				'UPDATE avs
				SET
					title=:title,
					acronym=:acronym
				WHERE id=:id')
		->param(':title', $data['title'])
		->param(':acronym', $data['acronym'])
		->param(':id', $data['id'])
		->execute();
	}

	public function add_av($data)
	{
		DB::query(Database::INSERT,
				'INSERT INTO avs
					(title,acronym)
				VALUES
					(:title,:acronym)')
			->param(':title', $data['title'])
			->param(':acronym', $data['acronym'])
		->execute();
	}

	public function del_av($data)
	{
		DB::query(Database::DELETE,
			'DELETE FROM avs
			WHERE id=:id')
			->param(':id', $data["id"])
			->execute();
	}

}
