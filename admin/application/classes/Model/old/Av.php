<?php defined('SYSPATH') or die('No direct script access.');

class Model_Av extends Model_Database
{
    protected $stat_table_name = 'incoming';
    
    protected $stat_db = 'av';

    public function get_stat($user_id = 0)
    {
        $sql = 'SELECT 
    		    tbl.a_date, snd_out, snd_in
    		FROM (
    		    SELECT 
    			DATE(date) AS a_date, COUNT(date) AS snd_out
    		    FROM 
    			' . $this->stat_table_name . ' 
    		    WHERE
    			site = :site AND send=1 
    		    GROUP BY
    			a_date
    		) AS tbl 
    		LEFT JOIN (
    		    SELECT
    			DATE(date) AS a_date, COUNT(date) AS snd_in
    		    FROM
    			' . $this->stat_table_name . '
    		    WHERE
    			site = :site AND send=0 
    		    GROUP BY
    			a_date
    		) AS tbl_2 
    		    ON
    			tbl.a_date = tbl_2.a_date';

	return DB::query(Database::SELECT, $sql)
			->param(':site', $site)
			->execute($this->stat_db)
			->as_array();;
    }
    
}
