<?php defined('SYSPATH') or die('No direct script access.');

class Model_Ips extends Model_Database
{
	protected $stat_table_name = 'incoming';
	protected $banners_table_name = 'tds_banners';
	protected $domains_table_name = 'domains';
	protected $ips_table_name = 'ips';
	protected $campaigns_table_name = 'tds_campaigns';
	protected $users_table_name = 'users';

	protected $stat_db = 'tds';

	public function get_all_ips()
	{
		return DB::query(Database::SELECT,
			'SELECT *
			FROM ' . $this->ips_table_name . ' I
			ORDER BY ip')
			->execute()
			->as_array();
	}

	public function edit_ip($data)
	{
		$arr = DB::query(Database::SELECT,
			'SELECT id
			FROM ' . $this->ips_table_name . '
			WHERE ip=:ip
			LIMIT 1')
			->param(':ip', $data['ip'])
			->execute()
			->as_array();
		if (!empty($arr))
			DB::query(Database::UPDATE,
				'UPDATE ' . $this->ips_table_name . '
				SET
					ip=:ip,
					descr=:descr
				WHERE id=:id')
				->param(':id', $data['id'])
				->param(':ip', $data['ip'])
				->param(':descr', $data['descr'])
				->execute();
	}

	public function add_ip($data)
	{
		preg_match_all("`^\ *(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\ *$`ism", $data["ips_list"], $ips);
		foreach ($ips[1] as $ip) {
			$arr = DB::query(Database::SELECT,
				'SELECT id
				FROM ' . $this->ips_table_name . '
				WHERE ip=:ip
				LIMIT 1')
				->param(':ip', $ip)
				->execute()
				->as_array();
			if (empty($arr))
				DB::query(Database::INSERT,
					'INSERT INTO ' . $this->ips_table_name . '
						(ip,descr)
					VALUES
						(:ip, :descr)')
					->param(':ip', $ip)
					->param(':descr', $data['descr'])
					->execute();
		}
	}

	public function del_ip($data)
	{
		DB::query(Database::DELETE,
			'DELETE FROM ' . $this->ips_table_name . '
			WHERE id=:id')
			->param(':id', $data["id"])
			->execute();
	}

}
