<?php defined('SYSPATH') or die('No direct script access.');

class Model_Avstat extends Model_Database
{
	protected $stat_table_name = 'incoming';
	protected $banners_table_name = 'tds_banners';
	protected $domains_table_name = 'domains';
	protected $campaigns_table_name = 'tds_campaigns';
	protected $users_table_name = 'users';
	protected $avstat_table_name = 'avstat_log';

	protected $stat_db = 'tds';

	public function getdatafromdate($data)
	{
		return DB::query(Database::SELECT,
			'SELECT
					A.date,
					IF(A.domain LIKE "%DOCTYPE%", "-", A.domain) domain,
					IF(A.file LIKE "%DOCTYPE%", "-", A.file) file,
					IF(A.exploit LIKE "%DOCTYPE%", "-", A.exploit) exploit,
					IF(A.heur LIKE "%DOCTYPE%", "-", A.heur) heur,
					IF(A.runtime LIKE "%DOCTYPE%", "-", A.runtime) runtime
			FROM ' . $this->avstat_table_name . ' A
			WHERE A.type=:tp AND A.date LIKE "'.$data['date'].'%" AND
				(A.domain<>"0" OR A.file<>"0" OR A.exploit<>"0" OR A.heur<>"0" OR A.runtime<>"0")
			ORDER BY A.date DESC')
			->param(':tp', $data['type'])
			->execute()
			->as_array();
	}

	public function get_av_ignore_list($user_id = 0)
	{
		$res = DB::query(Database::SELECT,
			'SELECT av_ignore
			FROM '.$this->users_table_name.'
			WHERE id=:user_id')
			->param(':user_id', $user_id)
			->execute()
			->as_array();
		return $res[0]['av_ignore'];
	}

	public function get_type_avstat($type)
	{
		$dt = date("Y-m-d", time());
		return DB::query(Database::SELECT,
			'SELECT
				A.date,
				IF(A.domain LIKE "%DOCTYPE%", "-", A.domain) domain,
				IF(A.file LIKE "%DOCTYPE%", "-", A.file) file,
				IF(A.exploit LIKE "%DOCTYPE%", "-", A.exploit) exploit,
				IF(A.heur LIKE "%DOCTYPE%", "-", A.heur) heur,
				IF(A.runtime LIKE "%DOCTYPE%", "-", A.runtime) runtime
			FROM '.$this->avstat_table_name.' A
			WHERE A.type = :type AND A.date LIKE "'.$dt.'%" AND
				(A.domain<>"0" OR A.file<>"0" OR A.exploit<>"0" OR A.heur<>"0" OR A.runtime<>"0")
			ORDER BY A.date DESC')
			->param(':type', $type)
			->execute()
			->as_array();
	}

	public function edit_ignore_list($user_id, $data)
	{
		$tps = array('domain', 'file', 'exploit', 'heur');
		foreach ($tps as $tp)
			$data[$tp] = explode("|", $data[$tp]);
		foreach ($data as $k => $v) if (!in_array($k, $tps)) unset($data[$k]);
		$data = json_encode($data);
		DB::query(Database::UPDATE,
			'UPDATE '.$this->users_table_name.'
			SET
				av_ignore=:av_ignore
			WHERE id=:user_id')
			->param(':av_ignore', $data)
			->param(':user_id', $user_id)
			->execute();
	}
}
