<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Domain extends ODM {

    protected $_collection_name = 'domain';
    protected $_db_group = 'banner';
    protected $_schema = false;

    public function getByUserId($user_id) {
        foreach ($this->where('user_id', '=', $user_id)->find_all() as $v) {
            $result[] = $v->as_array();
        }
        return $result;
    }

    public function get_all() {
        foreach ($this->find_all() as $v) {
            $result[$v->user_id] = $v->as_array();
        }
        return $result;
    }

}
