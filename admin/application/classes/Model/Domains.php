<?php defined('SYSPATH') or die('No direct script access.');

class Model_Domains extends Model_Database
{
	protected $stat_table_name = 'incoming';
	protected $banners_table_name = 'tds_banners';
	protected $domains_table_name = 'domains';
	protected $logs_table_name = 'logs';
	protected $campaigns_table_name = 'tds_campaigns';
	protected $users_table_name = 'users';
	protected $ips_table_name = 'ips';

	protected $stat_db = 'tds';

	public function get_all_domains()
	{
		return DB::query(Database::SELECT,
			'SELECT D.*, D.id d_id, U.id u_id, U.username username, I.ip, I.avd_detect i_avd_detect, I.avd_total i_avd_total, I.avd_link i_avd_link
			FROM ((domains D LEFT JOIN ips I ON D.ip = I.ip) LEFT JOIN tds_campaigns C ON D.id = C.campaign_domain_id), users U
			WHERE D.is_delete = 0 AND U.id = D.user_id GROUP BY d_id ORDER BY U.username, D.name')
			->execute()
			->as_array();
	}

	public function get_users()
	{
		return DB::query(Database::SELECT,
			'SELECT username
			FROM ' . $this->users_table_name . ' U
			WHERE U.username != "admin"
			GROUP BY username
			ORDER BY username')
			->execute()
			->as_array();
	}

	public function get_all_users()
	{
		return DB::query(Database::SELECT,
			'SELECT U.id id, U.username username
			FROM ' . $this->users_table_name  .' U
			ORDER BY U.username')
			->execute()
			->as_array();
	}

	public function edit_domain($data)
	{
		DB::query(Database::UPDATE,
				'UPDATE ' . $this->domains_table_name . '
				SET
					user_id=:user_id,
					name=:domain_name,
					avd_detect = 0,
					avd_total = 0,
					avd_link = 0
				WHERE id=:id')
				->param(':user_id', $data['user_id'])
				->param(':domain_name', $data['domain_name'])
				->param(':id', $data['id'])
				->execute();
	}

	public function add_domain($data)
	{
		$user_id = $data["user_id"];
		$domains = explode("\n", $data['domain_name_list']);

		foreach ($domains as $domain)
		{
			if ( preg_match('/^[\w\d\-\.]+\.[a-z]{2,6}$/i', trim($domain)) )
			{
		
				$domain = strtolower($domain);

				$arr = DB::query(Database::SELECT,
					'SELECT id
					FROM ' . $this->domains_table_name . '
					WHERE name=:domain_name
					LIMIT 1')
					->param(':domain_name', $domain)
					->execute()
					->as_array();
			
				if (empty($arr))
				{
					$ins = DB::query(Database::INSERT,
							'INSERT INTO ' . $this->domains_table_name . '
								(user_id,name)
							VALUES
								(:user_id,:domain_name)')
					->param(':user_id', $user_id)
					->param(':domain_name', trim($domain))
					->execute();

					$last_id = $ins[0];

					DB::query(Database::INSERT,
							'INSERT INTO ' . $this->logs_table_name . '
								(user_id,domain_id,date_add)
							VALUES
								(:user_id,:domain_id, NOW())')
					->param(':user_id', $user_id)
					->param(':domain_id', $last_id)
					->execute();
					
					
				}
			}
		}
	}

	public function del_domain($data)
	{
		DB::query(Database::DELETE,
			'UPDATE ' . $this->domains_table_name . '
			SET is_delete = 1
			WHERE id=:id')
			->param(':id', $data["id"])
			->execute();

		DB::query(Database::DELETE,
			'UPDATE ' . $this->logs_table_name . '
			SET date_del = NOW()
			WHERE domain_id=:id')
			->param(':id', $data["id"])
			->execute();

	}

}
