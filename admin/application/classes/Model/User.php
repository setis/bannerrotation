<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Model_User extends Model_Auth_User {

    public function user_roles($user_id) {
        $result = array();
        # SELECT -> ROLES_USERS
        $db = DB::select()
                ->from('roles_users')
                ->where('roles_users.user_id', '=', $user_id)

                # SELECT -> ROLES
                ->join('roles')
                ->on('roles.id', '=', 'roles_users.role_id');

        $roles = $db->execute($this->_db_group)->as_array();

        foreach ($roles as $role) {
            $result[(int) $role['role_id']] = $role['name'];
        }
        return $result;
    }

    public function get_all() {
        foreach ($this->find_all()->as_array() as $arr) {
            $result[$arr->id] = $arr->as_array();
        }
        return $result;
    }

}
