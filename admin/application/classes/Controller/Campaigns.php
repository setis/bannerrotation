<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Campaigns extends Controller_Common {

	protected $model;

	public function before()
	{
		parent::before();

		$this->model = Model::factory('Campaigns');
	}


	public function action_user()
	{
		$user_id = $this->request->param("id");
                $company = new Model_Company();
                $companies = $company->getByUserId($user_id);
		$campaigns = $this->model->get_all_user_campaigns($username);
		$banners = $this->model->get_all_user_banners($username);

		$buf = $this->model->get_all_avs();
		$avs = array();
		foreach ($buf as $row) $avs[$row['acronym']] = $row['title'];

		//redirect_domains
		foreach ($campaigns as $k => $camp)
		{
			if ($camp['type'] == 'redirect')
			{
				$res = $this->model->get_current_domain($camp['d_name']);



				if (preg_match('/^[0-9\.\-\_a-z]+$/', $res))
					$campaigns[$k]['curdom'] = $res;
				else
					$campaigns[$k]['curdom'] = '-';
			}
		}

		$users_ = $this->model->get_all_users();
		$users_freedomains = $this->model->get_users_freedomains();
		$banners_ = $this->model->get_all_banners();

		$content = View::factory('/pages/campaigns')
			->bind('username', $username)
			->bind('campaigns', $campaigns)
			->bind('campaign_domains', $campaign_domains)
			->bind('banners', $banners)
			->bind('users_', $users_)
			->bind('users_freedomains', $users_freedomains)
			->bind('banners_', $banners_)
			->bind('avs', $avs);

		$this->template->content = $content;
	}

	public function action_getcampdata()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");

		if ($validation->check())
		{
			$data = $this->model->get_campaign_data($data['id']);
			$res = $data[0];
			exit(json_encode($res));
		}
		exit("err");
	}

}
