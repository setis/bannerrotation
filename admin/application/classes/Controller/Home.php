<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Home extends Controller_Common {

    public $template = 'home';

    public function before() {
        parent::before();

        $site_config = Kohana::$config->load('mcbase');

        View::set_global('title', $site_config->get('title_user'));
        View::set_global('site_name', $site_config->get('site_name'));

        $this->template->scripts = array(
            'libs/jquery-2.0.2.min',
            'libs/jquery-ui-1.10.3.min',
            'bootstrap/bootstrap.min',
            'notification/SmartNotification.min',
            'smartwidgets/jarvis.widget.min',
            'plugin/easy-pie-chart/jquery.easy-pie-chart.min',
            'plugin/sparkline/jquery.sparkline.min',
            'plugin/jquery-validate/jquery.validate',
            'plugin/masked-input/jquery.maskedinput.min',
            'plugin/select2/select2.min',
            'plugin/bootstrap-slider/bootstrap-slider.min',
            'plugin/msie-fix/jquery.mb.browser.min',
            'plugin/smartclick/smartclick',
            'app'
        );

        $this->template->styles = array(
            'bootstrap.min',
            'font-awesome.min',
            'lockscreen',
            'smartadmin-production',
            'smartadmin-skins'
        );

        $this->template->user_id = $this->user->id;
        $this->template->username = $this->user->username;
    }

    public function action_index() {
        $this->template->users = (new Model_User())->get_all();
        $this->template->companies = (new Model_Company())->get_all();
        $this->template->hosts = (new Model_Host())->get_all();
    }

}
