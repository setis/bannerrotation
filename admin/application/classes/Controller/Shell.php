<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Shell extends Controller {

    /**
     * http://tds_admin/shell/index?username=admin&password=123123123123&password_confirm=123123123123&email=setis-0@mail.ru
     */
    public function action_index() {


        try {
            ORM::factory('User')->create_user($_GET, array('username', 'email', 'password'));
        } catch (ORM_Validation_Exception $e) {
            echo Debug::vars($e->errors('validation'));
        }
    }

    public function action_auth() {
        Auth::instance()->force_login($_GET['login'],false);
    }

    public function action_test(){
        $model = new Model_Company();
        $this->response->body(Debug::dump($model->get_all_users()));
    }
}
