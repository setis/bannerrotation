<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Auth extends Controller {

    /**
     *
     * @var Model_User|Model_Auth_User
     */
    public $user;

    /**
     *
     * @var array
     */
    public $roles;

    /**
     *
     * @var array|int 
     */
    public $host_id;

    /**
     *
     * @var Kohana_Auth|Auth|Auth_ODM|Auth_ORM 
     */
    public $Auth;

    public function action_login() {
        $this->Auth = Auth::instance();
        $session = Session::instance();
        if ($this->Auth->logged_in()) {
            $this->request->redirect('/');
        } else {
            if ($_REQUEST) {
                $post = $this->request->post();

                if ($this->Auth->login($post['login'], $post['password'])) {
                    $this->user = $this->Auth->get_user();
                    $model = new Model_Host();
                    $result = $model->getUserById($this->user->id);
                    if (!empty($result['host_id'])) {
                        $this->host_id = (int) $result['host_id'];
                    } else {
                        foreach ($result as $arr) {
                            $this->host_id[] = (int) $arr['host_id'];
                        }
                    }
                    $session->set('host_id', $this->host_id);
                    $this->roles = $this->user->user_roles($this->user->id);
                    $session->set('roles', $this->roles);
                }
                $this->request->redirect('/');
            }
        }

        $this->response->body(View::factory('/pages/login'));
    }

    public function action_logout() {
        Auth::instance()->logout();

        $this->request->redirect('/login');
    }

}
