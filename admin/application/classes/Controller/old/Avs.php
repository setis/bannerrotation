<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Avs extends Controller_Common {

	protected $model;

	public function before()
	{
		parent::before();
		$this->model = Model::factory('Avs');
	}

	public function action_add()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('title', "not_empty");
		$validation->rule('acronym', "not_empty");

		if ($validation->check())
		{
			$this->model->add_av($data);
		}
		else 
		{
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}

		header("Location: /#/avs/show");
	}

	public function action_del()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");

		if ($validation->check())
		{
			$this->model->del_av($data); exit('ok');
		}
		else {
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}
		exit('err');
	}

	public function action_edit()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");
		$validation->rule('title', "not_empty");
		$validation->rule('acronym', "not_empty");

		if ($validation->check())
		{
			$this->model->edit_av($data);
		}
		else {
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}
		header("Location: /#/avs/show");
	}

	public function action_show()
	{
		$avs = $this->model->get_all_avs();

		$content = View::factory('/pages/avs')
			->bind('avs', $avs);

		$this->template->content = $content;
	}
}
