<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Avstat extends Controller_Common {

	protected $model;

	public function before()
	{
		parent::before();
		$this->model = Model::factory('Avstat');
	}

	/*public function action_edit_ignore_list()
	{
		$user_id = $this->user->id;
		$data = $_POST;
		$validation = Validation::factory($data);
		$tps = array('domain', 'file', 'exploit', 'heur');
		foreach ($tps as $tp)
			$validation->rule($tp, 'regex', array(':value', '/([a-z0-9]*\|)*[a-z0-9]* /i'));
		if ($validation->check())
			$this->model->edit_ignore_list($user_id, $data);
		header("Location: /#/avstat/show/");
	}*/

	public function action_type()
	{
		if (!Auth::instance()->logged_in('admin')) header("Location: /");
		$type = $this->request->param("id");
		$avstat_log = $this->model->get_type_avstat($type);

		$content = View::factory('/pages/avstat')
			->bind('type', $type)
			->bind('avstat_log', $avstat_log);

		$this->template->content = $content;
	}

	public function action_getdatafromdate()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('type', "not_empty");
		$validation->rule('type', "regex", array(":value", "`[a-z]+`"));
		$validation->rule('date', "not_empty");
		$validation->rule('date', "regex", array(":value", "`\d{4}\-\d{2}\-\d{2}`"));

		if ($validation->check())
		{
			$res = $this->model->getdatafromdate($data); exit(json_encode($res));
		}
		else {
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}
		exit("false");
	}
}