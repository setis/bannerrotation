<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Banners extends Controller_Common {

	protected $model;

	public function before()
	{
		parent::before();
		$this->model = Model::factory('Banners');
	}

	public function action_upload()
	{
		$file = Validation::factory($_FILES)
			->rules('banner', array(
				array('not_empty'),
				array('Upload::not_empty'),
				array('Upload::valid'),
				//array('Upload::image'),
				array('Upload::size', array(':value', '1M')),
				array('Upload::type', array(':value', array('jpeg', 'png', 'gif', 'jpg', 'swf')))
			));

		if ( $file->check() )
		{
			$banner = array();

			$banner['user_id'] = $_POST['user_id'];
			
			$banner['name'] = $_POST['user_id'] . trim(preg_replace('#[\r|\n|\s|<|>|\"|\/|\\\]+#', '', $_FILES['banner']['name']));
			
			if ( count($this->model->isset_banner($banner['name'])) > 0 )
			{
				$banner['name'] = rand(100,999) . $banner['name'];
			}

			$info = getimagesize($_FILES['banner']['tmp_name']);

			$banner['width'] = $info[0];
			$banner['height'] = $info[1];
			$banner['type'] = $info['mime'];
			$banner['data'] = file_get_contents($_FILES['banner']['tmp_name']);

			if ( count($this->model->save_banner($banner)) > 0 )
			{
				$this->request->redirect('/#/banners/show');
			}
			else
			{
				//error save
			}
		}
		else
		{
			// error upload
		}
		header("Location: /banners/show");
	}

	public function action_show()
	{
		$banners = $this->model->get_all_banners($this->user->id);

		$content = View::factory('/pages/banners')
			->bind('banners', $banners);

		$this->template->content = $content;
	}

	public function action_user()
	{
		$username = $this->request->param("id");
		$banners = $this->model->get_all_user_banners($username);
		$users = $this->model->get_all_users();

		$content = View::factory('/pages/banners')
			->bind('banners', $banners)
			->bind('username', $username)
			->bind('users', $users);

		$this->template->content = $content;
	}

	public function action_banner()
	{
		$id = Validation::factory($this->request->param())
			->rule('id', array('Valid', 'digit'));
		if ( $id->check() )
		{
			$image = $this->model->get_banner($this->request->param('id'),$this->user->id);
			if (count($image[0]) > 0)
			{
				header('Content-Type: ' . $image[0]['type']);
				exit($image[0]['data']);
			}
		}
	}

	public function action_del()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");

		if ($validation->check())
		{
			$this->model->del_banner($data);
			exit('ok');
		}
		exit('err');
	}
}