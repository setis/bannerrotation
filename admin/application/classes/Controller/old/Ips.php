<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ips extends Controller_Common {

	protected $model;

	public function before()
	{
		parent::before();
		$this->model = Model::factory('Ips');
	}

	public function action_add()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");
		$validation->rule('ips_list', "not_empty");
		$validation->rule('ips_list', "regex", array(":value", "`\ *((\d{1,3}\.){3}\d{1,3}\ *(\n|\r|\r\n|\n\r))*\ *(\d{1,3}\.){3}\d{1,3}\ *`"));

		if ($validation->check())
		{
			$this->model->add_ip($data);
		}
		else {
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}
		header("Location: /#/ips/show");
	}

	public function action_del()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");

		if ($validation->check())
		{
			$this->model->del_ip($data); exit('ok');
		}
		else {
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}
		exit('err');
	}

	public function action_edit()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");
		$validation->rule('ip', "regex", array(":value", "/(\d{1,3}\.){3}\d{1,3}/"));

		if ($validation->check())
		{
			$this->model->edit_ip($data);
		}
		else {
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}
		header("Location: /#/ips/show");
	}

	public function action_show()
	{
		$ips = $this->model->get_all_ips();

		$content = View::factory('/pages/ips')
			->bind('ips', $ips);

		$this->template->content = $content;
	}
}