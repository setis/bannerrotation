<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Tdscodes extends Controller_Common {

	protected $model;

	public function before()
	{
		parent::before();
		$this->model = Model::factory('Tdscodes');
	}

	public function action_update()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");
		$validation->rule('comment', "not_empty");

		if ($validation->check())
		{
			$data['comment'] = base64_encode($data['comment']);
			$this->model->update_tdscode($data);
		}
		else {
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}
		header("Location: /#/tdscodes/show");
	}

	public function action_mark()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");

		if ($validation->check())
		{
			if ($this->model->mark_tdscode($data)) exit('ok');
		}
		else {
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}
		exit('err');
	}

	public function action_show()
	{
		$user_id = $this->user->id;
		$tdscodes = $this->model->get_all_tdscodes();

		$content = View::factory('/pages/tdscodes')
			->bind('tdscodes', $tdscodes);

		$this->template->content = $content;
	}
}