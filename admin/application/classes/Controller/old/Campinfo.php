<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Campinfo extends Controller_Common {

	protected $model;

	public function before()
	{
		parent::before();
		$this->model = Model::factory('Campinfo');
	}

	public function action_show()
	{
		$time = microtime(true);

		$id = $this->request->param("id");
		$campaign = $this->model->get_campaign($id);
		$campaign['incoming'] = $this->model->get_short_stat($id);
		$campaign['stat'] = $this->model->get_detailed_stat(date('Y-m-d', time()), $id);


		$content = View::factory('/pages/campinfo')
			->bind('campaign', $campaign)
			->bind('exec_time', $exec_time)
		;

		$exec_time = microtime(true)-$time;

		$this->template->content = $content;
	}

}
