<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Domains extends Controller_Common {

	protected $model;

	public function before()
	{
		parent::before();
		$this->model = Model::factory('Domains');
	}

	public function action_add()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");
		$validation->rule('domain_name_list', "not_empty");
		$validation->rule('domain_name_list', "regex", array(":value", "`\ *([A-Za-z0-9\-\.]+\ *(\n|\r|\r\n|\n\r))*\ *[A-Za-z0-9\-\.]+\ *`"));
		$validation->rule('user_id', "not_empty");
		$validation->rule('user_id', "numeric");

		if ($validation->check())
		{
			$this->model->add_domain($data);
		}
		else 
		{
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}

		header("Location: /#/domains/show");
	}

	public function action_del()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");

		if ($validation->check())
		{
			$this->model->del_domain($data); exit('ok');
		}
		else {
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}
		exit('err');
	}

	public function action_edit()
	{
		$data = $_POST;
		$validation = Validation::factory($data);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");
		$validation->rule('domain_name', "not_empty");
		$validation->rule('domain_name', "regex", array(":value", "`\ *[A-Za-z0-9\-\.]+\s*`"));
		$validation->rule('user_id', "not_empty");
		$validation->rule('user_id', "numeric");

		if ($validation->check())
		{
			$data['domain_name'] = trim($data['domain_name']);
			$this->model->edit_domain($data);
		}
		else {
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}
		header("Location: /#/domains/show");
	}

	public function action_show()
	{
		$user_id = $this->user->id;
		$domains = $this->model->get_all_domains();
		$users = $this->model->get_all_users();

		$buf = array();
		foreach ($domains as $domain)
		{
			if (!isset($buf[$domain['username']])) $buf[$domain['username']] = array();
			$buf[$domain['username']][] = $domain;
		}
		$domains = $buf;
		unset($buf);

		$content = View::factory('/pages/domains')
			->bind('domains', $domains)
			->bind('users', $users);

		$this->template->content = $content;
	}
}
