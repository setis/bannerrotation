<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Domainslog extends Controller_Common {

	protected $model;

	public function before()
	{
		parent::before();
		$this->model = Model::factory('Domainslog');
	}

	public function action_show()
	{
		if ( Auth::instance()->logged_in('admin') )
		{
			$user = $this->request->param('id');
		
			$_id = $this->model->get_user_id($user);
			
			if ( intval($_id[0]['id']) > 0 )
			{
				$user_id = intval($_id[0]['id']);
			}
			else exit('Error: Bad username');
		}
		else
		{
			$user_id = $this->user->id;
		}
		
		$log = $this->model->get_logs($user_id);

		$content = View::factory('/pages/domainslog')
			->bind('log', $log);

		$this->template->content = $content;
	}
}