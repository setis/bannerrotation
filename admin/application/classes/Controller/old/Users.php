<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Users extends Controller_Common {

	protected $model;

	public function before()
	{
		parent::before();
		$this->model = Model::factory('Users');
	}

	public function action_add()
	{
		$user = $_POST;
		$validation = Validation::factory($user);
		$validation->rule('username', "not_empty");
		$validation->rule('username', "regex", array(":value", "/[\w]+/is"));

		if ($validation->check())
		{
			$this->model->add_user($user);
		}
		else {
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}

		header("Location: /#/users/show");
	}

	public function action_edit()
	{
		$user = $_POST;
		$validation = Validation::factory($user);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");
		$validation->rule('username', "not_empty");
		$validation->rule('username', "regex", array(":value", "`[\w]+`"));

		if ($validation->check())
		{
			$this->model->edit_user($user);
		}
		else {
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}
		header("Location: /#/users/show");
	}

	public function action_del()
	{
		$user = $_POST;
		$validation = Validation::factory($user);
		$validation->rule('id', "not_empty");
		$validation->rule('id', "numeric");

		if ($validation->check())
		{
			$this->model->del_user($user); exit("ok");
		}
		else {
			if (!isset($GLOBALS["tds_errors"]))
			{
				$GLOBALS["tds_errors"] = "Entered data is not valid";
			}
		}
		exit("err");
	}

	public function action_show()
	{
		$users = $this->model->get_all_users();


		$content = View::factory('/pages/users')
			->bind('users', $users);

		$this->template->content = $content;
	}
}
