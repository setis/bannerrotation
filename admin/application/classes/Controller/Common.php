<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Controller_Common extends Controller_Template {

    /**
     *
     * @var Model_User|Model_Auth_User
     */
    public $user;

    /**
     *
     * @var array
     */
    public $roles;

    /**
     *
     * @var array|int 
     */
    public $host_id;

    const
            role_id_user = 1,
            role_id_admin = 2;

    /**
     *
     * @var Kohana_Auth|Auth|Auth_ODM|Auth_ORM 
     */
    public $Auth;

    public function before() {
        parent::before();
        $this->Auth = Auth::instance();
        $session = Session::instance();
        if (!$this->Auth->logged_in()) {
            $this->request->redirect('/login');
            return;
        } else {
            $this->host_id = $session->get('host_id', NULL);
            $this->roles = $session->get('roles', null);
        }
        $this->user = $this->Auth->get_user();
        if ($this->roles === null) {
            $this->roles = $this->user->user_roles($this->user->id);
            $session->set('roles', $this->roles);
        }
        if ($this->roles !== null && !isset($this->roles[self::role_id_admin])) {
            $this->request->redirect('/login');
        } else {
            if ($this->host_id === null) {
                $model = new Model_Host();
                $result = $model->getUserById($this->user->id);
                if (!empty($result['host_id'])) {
                    $this->host_id = (int) $result['host_id'];
                } else {
                    foreach ($result as $arr) {
                        $this->host_id[] = (int) $arr['host_id'];
                    }
                }
                $session->set('host_id', $this->host_id);
            }
        }
    }

}
