	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-deletebutton="false">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
					<h2>Total incoming</h2>
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<div id="saleschart" class="chart"></div>
						
					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>
			<!-- end widget -->

		</div>
		<!-- WIDGET END -->

		<!-- NEW WIDGET START -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Date stat</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget content -->
					<div class="widget-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Date</th>
									<th>Source</th>
									<th>Incoming</th>
									<th>Outcoming</th>
									<th>Dropped</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($dates as $date): ?>
								<tr>
									<td><?php echo $date['a_date']; ?></td>
									<td><?php echo $date['snd_src']; ?></td>
									<td><?php echo $date['snd_in']; ?></td>
									<td><?php echo $date['snd_out']; ?></td>
									<td><?php echo ($date['snd_in'] - $date['snd_out']); ?></td>
								</tr>
								<?php endforeach; ?>
		
								
							</tbody>
						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->
		</div>
		<!-- WIDGET END -->

		<!-- NEW WIDGET START -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Browser stat</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget content -->
					<div class="widget-body no-padding">
						<div class="widget-body-toolbar">

						</div>
						
						<table id="dt_basic" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Browser</th>
									<th>Source</th>
									<th>Incoming</th>
									<th>Outcoming</th>
									<th>Dropped</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($brow_stat as $brow):?>
								<tr>
									<td><?=$brow['c_name']?></td>
									<td><?=$brow['snd_src']?></td>
									<td><?=$brow['snd_in']?></td>
									<td><?=$brow['snd_out']?></td>
									<td><?php echo ($brow['snd_in'] - $brow['snd_out']); ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</div>
		<!-- WIDGET END -->

		<!-- NEW WIDGET START -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Country stat</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget content -->
					<div class="widget-body no-padding">
						<div class="widget-body-toolbar">

						</div>
						
						<table id="dt_basic_1" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Country</th>
									<th>Source</th>
									<th>Incoming</th>
									<th>Outcoming</th>
									<th>Dropped</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($country_stat as $country):?>
								<tr>
									<td><?=$country['c_name']?></td>
									<td><?=$country['snd_src']?></td>
									<td><?=$country['snd_in']?></td>
									<td><?=$country['snd_out']?></td>
									<td><?php echo ($country['snd_in'] - $country['snd_out']); ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</div>
		<!-- WIDGET END -->

		<!-- NEW WIDGET START -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-2" data-widget-editbutton="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>OS stat</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget content -->
					<div class="widget-body no-padding">
						<div class="widget-body-toolbar">

						</div>
						
						<table id="dt_basic_2" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Platform</th>
									<th>Source</th>
									<th>Incoming</th>
									<th>Outcoming</th>
									<th>Dropped</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($os_stat as $os):?>
								<tr>
									<td><?=$os['c_name']?></td>
									<td><?=$os['snd_src']?></td>
									<td><?=$os['snd_in']?></td>
									<td><?=$os['snd_out']?></td>
									<td><?php echo ($os['snd_in'] - $os['snd_out']); ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</div>
		<!-- WIDGET END -->

		<!-- NEW WIDGET START -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-3" data-widget-editbutton="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Referer stat</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget content -->
					<div class="widget-body no-padding">
						<div class="widget-body-toolbar">

						</div>
						
						<table id="dt_basic_3" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Referer</th>
									<th>Source</th>
									<th>Incoming</th>
									<th>Outcoming</th>
									<th>Dropped</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($ref_stat as $ref):?>
								<tr>
									<td><?=$ref['c_name']?></td>
									<td><?=$ref['snd_src']?></td>
									<td><?=$ref['snd_in']?></td>
									<td><?=$ref['snd_out']?></td>
									<td><?php echo ($ref['snd_in'] - $ref['snd_out']); ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</div>
		<!-- WIDGET END -->

	</div>
	<!-- end row -->

<script type="text/javascript">
	
	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();
	
	// PAGE RELATED SCRIPTS

	/*
	 * LOAD ALL FLOT DEPENDENCIES
	 */

	loadScript("js/plugin/flot/jquery.flot.cust.js", loadFlotResize);
	
	function loadFlotResize() {
		loadScript("js/plugin/flot/jquery.flot.resize.js", loadFlotFillbetween);
	}
	function loadFlotFillbetween() {
		loadScript("js/plugin/flot/jquery.flot.fillbetween.js", loadFlotOrderBar);
	}
	function loadFlotOrderBar() {
		loadScript("js/plugin/flot/jquery.flot.orderBar.js", loadFlotPie);
	}	
	function loadFlotPie() {
		loadScript("js/plugin/flot/jquery.flot.pie.js", loadFlotToolTip);
	}
	function loadFlotToolTip() {
		loadScript("js/plugin/flot/jquery.flot.tooltip.js", generateAllFlotCharts);
	}

	/* chart colors default */
	var $chrt_border_color = "#efefef";
	var $chrt_grid_color = "#DDD"
	var $chrt_main = "#E24913";			/* red       */
	var $chrt_second = "#6595b4";		/* blue      */
	var $chrt_third = "#FF9F01";		/* orange    */
	var $chrt_fourth = "#7e9d3a";		/* green     */
	var $chrt_fifth = "#BD362F";		/* dark red  */
	var $chrt_mono = "#000";
	
	function generateAllFlotCharts() {
		
		/* stat chart */
		
		if ($("#saleschart").length) {
			var d = [];

			d.push(new Array(<?php echo (strtotime($dates[0]['a_date']) - 86400); ?>000, 0));
			
			<?php foreach($dates as $date): ?>
				d.push(new Array(<?php echo strtotime($date['a_date']); ?>000, <?php echo $date['snd_in']; ?>));
			<?php endforeach; ?>
		
			for (var i = 0; i < d.length; ++i)
				d[i][0] += 60 * 60 * 1000;
		
			function weekendAreas(axes) {
				var markings = [];
				var d = new Date(axes.xaxis.min);
				// go to the first Saturday
				d.setUTCDate(d.getUTCDate() - ((d.getUTCDay() + 1) % 7))
				d.setUTCSeconds(0);
				d.setUTCMinutes(0);
				d.setUTCHours(0);
				var i = d.getTime();
				do {
					// when we don't set yaxis, the rectangle automatically
					// extends to infinity upwards and downwards
					markings.push({
						xaxis : {
							from : i,
							to : i + 2 * 24 * 60 * 60 * 1000
						}
					});
					i += 7 * 24 * 60 * 60 * 1000;
				} while (i < axes.xaxis.max);
		
				return markings;
			}
		
			var options = {
				xaxis : {
					mode : "time",
					tickLength : 5
				},
				series : {
					lines : {
						show : true,
						lineWidth : 1,
						fill : true,
						fillColor : {
							colors : [{
								opacity : 0.1
							}, {
								opacity : 0.15
							}]
						}
					},
                   //points: { show: true },
					shadowSize : 0
				},
				selection : {
					mode : "x"
				},
				grid : {
					hoverable : true,
					clickable : true,
					tickColor : $chrt_border_color,
					borderWidth : 0,
					borderColor : $chrt_border_color,
				},
				tooltip : true,
				tooltipOpts : {
					content : "Incoming for <b>%x</b>: <span>%y</span> unique visitor",
					dateFormat : "%0d-%0m-%y",
					defaultTheme : false
				},
				colors : [$chrt_second],
		
			};
		
			var plot = $.plot($("#saleschart"), [d], options);
		};
		
		/* end sales chart */
		

		/* updating chart */
		
		if ($('#updating-chart').length) {
	
			// For the demo we use generated data, but normally it would be coming from the server
			var data = [], totalPoints = 200;
			function getRandomData() {
				if (data.length > 0)
					data = data.slice(1);
	
				// do a random walk
				while (data.length < totalPoints) {
					var prev = data.length > 0 ? data[data.length - 1] : 50;
					var y = prev + Math.random() * 10 - 5;
					if (y < 0)
						y = 0;
					if (y > 100)
						y = 100;
					data.push(y);
				}
	
				// zip the generated y values with the x values
				var res = [];
				for (var i = 0; i < data.length; ++i)
					res.push([i, data[i]])
				return res;
			}
	
			// setup control widget
			var updateInterval = 1000;
			$("#updating-chart").val(updateInterval).change(function() {
				var v = $(this).val();
				if (v && !isNaN(+v)) {
					updateInterval = +v;
					if (updateInterval < 1)
						updateInterval = 1;
					if (updateInterval > 2000)
						updateInterval = 2000;
					$(this).val("" + updateInterval);
				}
			});
	
			// setup plot
			var options = {
				yaxis : {
					min : 0,
					max : 100
				},
				xaxis : {
					min : 0,
					max : 100
				},
				colors : [$chrt_fourth],
				series : {
					lines : {
						lineWidth : 1,
						fill : true,
						fillColor : {
							colors : [{
								opacity : 0.4
							}, {
								opacity : 0
							}]
						},
						steps : false
	
					}
				}
			};
			var plot = $.plot($("#updating-chart"), [getRandomData()], options);
	
			function update() {
				plot.setData([getRandomData()]);
				// since the axes don't change, we don't need to call plot.setupGrid()
				plot.draw();
	
				setTimeout(update, updateInterval);
			}
	
			update();
	
		}

		/*end updating chart*/
		
	}
	
	/* end flot charts */	

	
	// PAGE RELATED SCRIPTS

	loadDataTableScripts();
	function loadDataTableScripts() {

		loadScript("js/plugin/datatables/jquery.dataTables-cust.min.js", dt_2);

		function dt_2() {
			loadScript("js/plugin/datatables/ColReorder.min.js", dt_3);
		}

		function dt_3() {
			loadScript("js/plugin/datatables/FixedColumns.min.js", dt_4);
		}

		function dt_4() {
			loadScript("js/plugin/datatables/ColVis.min.js", dt_5);
		}

		function dt_5() {
			loadScript("js/plugin/datatables/ZeroClipboard.js", dt_6);
		}

		function dt_6() {
			loadScript("js/plugin/datatables/media/js/TableTools.min.js", dt_7);
		}

		function dt_7() {
			loadScript("js/plugin/datatables/DT_bootstrap.js", runDataTables);
		}

	}

	function runDataTables() {

		/*
		 * BASIC
		 */
		$('#dt_basic').dataTable({
			"sPaginationType" : "bootstrap_full"
		});

		$('#dt_basic_1').dataTable({
			"sPaginationType" : "bootstrap_full"
		});

		$('#dt_basic_2').dataTable({
			"sPaginationType" : "bootstrap_full"
		});

		$('#dt_basic_3').dataTable({
			"sPaginationType" : "bootstrap_full"
		});

		/* END BASIC */

		/* Add the events etc before DataTables hides a column */
		$("#datatable_fixed_column thead input").keyup(function() {
			oTable.fnFilter(this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $("thead input").index(this)));
		});

		$("#datatable_fixed_column thead input").each(function(i) {
			this.initVal = this.value;
		});
		$("#datatable_fixed_column thead input").focus(function() {
			if (this.className == "search_init") {
				this.className = "";
				this.value = "";
			}
		});
		$("#datatable_fixed_column thead input").blur(function(i) {
			if (this.value == "") {
				this.className = "search_init";
				this.value = this.initVal;
			}
		});		
		

		var oTable = $('#datatable_fixed_column').dataTable({
			"sDom" : "<'dt-top-row'><'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
			//"sDom" : "t<'row dt-wrapper'<'col-sm-6'i><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'>>",
			"oLanguage" : {
				"sSearch" : "Search all columns:"
			},
			"bSortCellsTop" : true
		});		
		


		/*
		 * COL ORDER
		 */
		$('#datatable_col_reorder').dataTable({
			"sPaginationType" : "bootstrap",
			"sDom" : "R<'dt-top-row'Clf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
			"fnInitComplete" : function(oSettings, json) {
				$('.ColVis_Button').addClass('btn btn-default btn-sm').html('Columns <i class="icon-arrow-down"></i>');
			}
		});
		
		/* END COL ORDER */

		/* TABLE TOOLS */
		$('#datatable_tabletools').dataTable({
			"sDom" : "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
			"oTableTools" : {
				"aButtons" : ["copy", "print", {
					"sExtends" : "collection",
					"sButtonText" : 'Save <span class="caret" />',
					"aButtons" : ["csv", "xls", "pdf"]
				}],
				"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
			},
			"fnInitComplete" : function(oSettings, json) {
				$(this).closest('#dt_table_tools_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function() {
					$(this).addClass('btn-sm btn-default');
				});
			}
		});
		
		/* END TABLE TOOLS */

	}

</script>
