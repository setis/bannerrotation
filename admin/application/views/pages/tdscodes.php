<?php if (!Auth::instance()->logged_in('admin')) exit();?>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				<h2>Create new PHP code</h2>
			</header>
			<div>
				<div class="jarviswidget-editbox">
				</div>
				<div class="widget-body no-padding">
					<form action="/tdscodes/update/" method="post" id="smart-form" class="smart-form">
						<input type="hidden" value="0" name="id">
						<fieldset>
							<section>
								<label class="textarea"> <i class="icon-append fa fa-info-circle"></i>
									<textarea class="custom-scroll" rows="15" name="comment" placeholder="PHP comment"></textarea>
									<b class="tooltip tooltip-bottom-right">Needed to enter the PHP comment</b> </label>
							</section>
						</fieldset>
						<footer>
							<button type="submit" class="btn btn-primary">
								Update
							</button>
						</footer>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i>
			PHP codes
		</h1>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>PHP codes</h2>
			</header>

			<!-- widget div-->
			<div>

				<!-- widget content -->
				<div class="widget-body">
					<table class="table table-bordered">
						<thead>
						<tr>
							<th style="width:150px">Action</th>
							<th>Name</th>
							<th>Type</th>
							<th>Coder</th>
							<th>Update date</th>
							<th>Comment</th>
						</tr>
						</thead>
						<tbody>

						<?php foreach ($tdscodes as $tdscode): ?>
							<tr id="pc_<?php echo $tdscode['id']; ?>">
								<td style="width:260px">
									<a data-toggle="modal" href="#myModal" class="btn btn-primary" onclick="updateData(<?php echo $tdscode['id']; ?>)">Update</a>
									<?php if (!$tdscode['clean']) : ?>
										<a id="mrk_btn_<?=$tdscode['id']?>" class="btn btn-danger" href="javascript:void(0);" onclick="markAsDetect(<?php echo $tdscode['id']; ?>)">Mark as detect</a>
									<?php endif; ?>
								</td>
								<td>
									<span id="pc_nm_<?php echo $tdscode['id']; ?>" class="badge bg-color-<?=$tdscode['clean']==0?("greenLight"):("red")?>");"><?=$tdscode['name']?></span>
								</td>
								<td>
									<span id="pc_tp_<?php echo $tdscode['id']; ?>"><?=$tdscode['type']?></span>
								</td>
								<td>
									<span id="pc_cdr_<?php echo $tdscode['id']; ?>"><?=$tdscode['coder']?></span>
								</td>
								<td>
									<span id="pc_ud_<?php echo $tdscode['id']; ?>"><?=$tdscode['date']?></span>
								</td>
								<td>
									<span id="pc_cmmnt_<?php echo $tdscode['id']; ?>"><?=base64_decode($tdscode['comment'])?></span>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>

<script type="text/javascript">

	function updateData(id) {
		$("div#wid-id-4.jarviswidget header h2").text("Update PHP code '" + $("#pc_nm_"+id).text() + "'");
		$("#smart-form input[name=id]").val(id);
		$("#smart-form textarea[name=comment]").val($("#pc_cmmnt_"+id).text());
	}


	function markAsDetect(id) {
		$.SmartMessageBox({
				title: "Mark as 'detect' PHP code",
				content: "Are you sure you want to mark as 'detect' the selected PHP code?",
				buttons: "[NO][YES]"
			},
			function (Res) {
				if (Res == "YES") {
					$.ajax({
						url: "/tdscodes/mark",
						method: "post",
						data: {id: id},
						success: function (res) {
							if (res == 'ok')
							{
								$('span#pc_nm_'+id).removeClass('bg-color-greenLight');
								$('span#pc_nm_'+id).addClass('bg-color-red');
								$('a#mrk_btn_' + id).remove();
							}
						}
					});
				}
			});
	}

	pageSetUp();
	loadScript("js/plugin/jquery-form/jquery-form.min.js", runFormValidation);

	function runFormValidation() {
		$("#smart-form").validate({
			ignore : "",
			errorPlacement : function(error, element) {error.insertAfter(element.parent());}
		});
	}
</script>
