
<div class="row">
	<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i>
			SWF banners
	</div>
	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		<!-- Button trigger modal -->
		<a data-toggle="modal" href="#addBanner" class="btn btn-success btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i> Upload new swf banner </a>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="addBanner" tabindex="-1" role="dialog" aria-labelledby="addBannerLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body no-padding">

				<form class="smart-form" id="smart-add-banner-form" enctype="multipart/form-data" accept-charset="utf-8" method="post" action="/flash/upload">
					<fieldset>
						<section>
							<div class="row">
								<label class="label col col-2">Select file</label>
								<div class="col col-10">
									<input type="file" name="banner">
								</div>
							</div>
						</section>
					</fieldset>

					<footer>
						<button type="submit" class="btn btn-primary">
							Upload
						</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">
							Cancel
						</button>

					</footer>
				</form>
			</div>

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			<div>
				<div class="widget-body">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="width:50px">ID</th>
								<th style="width:150px">Name</th>
								<th style="width:50px">Size</th>
								<th style="width:50px">avd</th>
								<th style="width:150px">Last check</th>
								<th style="width:150px">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($banners as $banner): ?>
							<tr id="swf_<?php echo $banner['id']; ?>">
								<td><?=$banner['id']?></td>
								<td><?=$banner['file_name']?></td>
								<td><?php echo round($banner['size']/1024, 2); ?> Kb</td>
								<td><span style="cursor:pointer;" class="badge bg-color-<?=$banner['avd_detect']==0?("greenLight"):("red")?>" onClick="window.open('<?=$banner['avd_link']?>');"><?=$banner['avd_detect']?>/<?=$banner['avd_total']?></span></td>
								<td><?=$banner['last_check']?></td>
								<td><a class="btn btn-danger" href="javascript:void(0);" onclick="delRecord(<?php echo $banner['id']; ?>)">Delete</a></td>
							</tr>
							<? endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">

	var delRecord = function(id) {
		$.SmartMessageBox({
				title: "Delete banner",
				content: "Are you sure you want to delete the selected banner?",
				buttons: "[NO][YES]"
			},
			function (Res) {
				if (Res == "YES") {
					$.ajax({
						url: "/flash/del",
						method: "post",
						data: {id: id},
						success: function (res) {
							//document.location.reload(true);
							if (res == 'ok') $('tr#swf_' + id).remove();
						}
					});
				}
			});
	};

	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

	// PAGE RELATED SCRIPTS

</script>
