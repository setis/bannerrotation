
<div class="row">
	<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i>
			Banners
	</div>
	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
		<!-- Button trigger modal -->
		<a data-toggle="modal" href="#addBanner" onclick="getData('<?=$username?>')" class="btn btn-success btn-lg pull-right header-btn hidden-mobile"><i class="fa fa-circle-arrow-up fa-lg"></i> Upload new banner </a>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="addBanner" tabindex="-1" role="dialog" aria-labelledby="addBannerLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body no-padding">

				<form class="smart-form" id="smart-add-banner-form" enctype="multipart/form-data" accept-charset="utf-8" method="post" action="/banners/upload">
					<fieldset>
						<section>
							<div class="row">
								<label class="label col col-2">Select file</label>
								<div class="col col-10">
									<input type="file" name="banner">
								</div>
							</div>
						</section>

						<section>
							<label class="select">
								<select name="user_id">
									<?php foreach($users as $user): ?>
										<option value="<?=$user['user_id'] ?>" title="<?=$user['username']?>"><?=$user['username']?></option>
									<?php endforeach; ?>
								</select>
							</label>
						</section>

					</fieldset>

					<footer>
						<button type="submit" class="btn btn-primary">
							Upload
						</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">
							Cancel
						</button>

					</footer>
				</form>
			</div>

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- row -->
<div class="row">

	<!-- SuperBox -->
	<div class="superbox col-sm-12">
		<?php
		foreach ($banners as $banner)
		{?>
			<div class="superbox-list">
				<img src="/banners/banner/<?=$banner['id']?>" data-cid="<?=$banner['cid']?>" data-id="<?=$banner['id']?>" data-img="/banners/banner/<?=$banner['id']?>" alt="Size: <?=$banner['width']?> x <?=$banner['height']?>" title="<?=$banner['file_name']?>" class="superbox-img">
			</div>
		<?php
		}?>
		<div class="superbox-float"></div>
	</div>
	<!-- /SuperBox -->

	<div class="superbox-show" style="height:300px; width:300px; display: none"></div>

</div>

<!-- end row -->

<script type="text/javascript">

	function getData(username)
	{
		$('#smart-add-banner-form select').val($('form#smart-add-banner-form select').find('option[title="' + username + '"]').attr('value'));
	}

	var delImg = function(id) {
		$.SmartMessageBox({
				title: "Delete banner",
				content: "Are you sure you want to delete the selected banner?",
				buttons: "[NO][YES]"
			},
			function (Res) {
				if (Res == "YES") {
					$.ajax({
						url: "/banners/del",
						method: "post",
						data: {id: id},
						success: function (res) {
							//document.location.reload(true);
							if (res == 'ok') $('img[data-id="' + id + '"]').parent().remove();
						}
					});
				}
			});
	};

	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

	// PAGE RELATED SCRIPTS

	// load bootstrap-progress bar script
	loadScript("js/plugin/superbox/superbox.js", runSuperBox);


	function runSuperBox() {
		$('.superbox').SuperBox({
			delEvent: delImg
		});

	}

</script>
