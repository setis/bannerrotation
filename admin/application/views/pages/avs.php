<?php if (!Auth::instance()->logged_in('admin')) exit();?>
<!-- Modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">

		<!-- Widget ID (each widget will need unique ID)-->
		<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">

			<header>
				<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				<h2>Add AV</h2>

			</header>

			<!-- widget div-->
			<div>

				<!-- widget edit box -->
				<div class="jarviswidget-editbox">
					<!-- This area used as dropdown edit box -->

				</div>
				<!-- end widget edit box -->

				<!-- widget content -->
				<div class="widget-body no-padding">
					<form action="/avs/add/" method="post" id="smart-form" class="smart-form">
						<input type="hidden" value="0" name="id">
						<fieldset>
							<section>
								<label class="input"> <i class="icon-append fa fa-info-circle"></i>
									<input type="text" name="title" placeholder="AV title">
									<b class="tooltip tooltip-bottom-right">Needed to enter the AV title</b> </label>
							</section>
							<section>
								<label class="input"> <i class="icon-append fa fa-info-circle"></i>
									<input type="text" name="acronym" placeholder="AV acronym">
									<b class="tooltip tooltip-bottom-right">Needed to enter the AV acronym</b> </label>
							</section>
						</fieldset>
						<footer>
							<button type="submit" class="btn btn-primary">
								Add
							</button>
						</footer>
					</form>

				</div>
				<!-- end widget content -->

			</div>
			<!-- end widget div -->

		</div>
		<!-- end widget -->

	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i>
			AV's
		</h1>
	</div>
	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
		<ul id="sparks" class="">
			<li class="sparks-info">
				<a data-toggle="modal" href="#myModal" class="btn btn-success btn-lg pull-right header-btn hidden-mobile" onclick="clearForm();"><i class="fa fa-circle-arrow-up fa-lg"></i> Add AV </a>
			</li>
		</ul>
	</div>
</div>


<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2>AV's</h2>
			</header>

			<div>
				<div class="widget-body">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="width:50px">ID</th>
								<th style="width:150px">Action</th>
								<th>Title</th>
								<th>Acronym</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($avs as $av): ?>
							<tr id="av_<?php echo $av['id']; ?>">
								<td style="width:50px">
									<span><?=$av['id']?></span>
								</td>
								<td style="width:150px">
									<a data-toggle="modal" href="#myModal" class="btn btn-primary" onclick="getData(<?php echo $av['id']; ?>)">Edit</a>
									<a class="btn btn-danger" href="javascript:void(0);" onclick="delRecord(<?php echo $av['id']; ?>)">Delete</a>
								</td>
								<td style="width:50px">
									<span id="av_t_<?php echo $av['id']; ?>"><?=$av['title']?></span>
								</td>
								<td style="width:50px">
									<span id="av_a_<?php echo $av['id']; ?>"><?=$av['acronym']?></span>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- SCRIPTS ON PAGE EVENT -->
<script type="text/javascript">

	function getData(id) {
		$("div#wid-id-4.jarviswidget header h2").text("Edit AV");
		$("#smart-form footer button[type=submit]").text("Save");
		$("form#smart-form").attr("action", "/avs/edit/");
		$("#smart-form input[name=id]").val(id);
		$("#smart-form input[name=title]").val($("#av_t_"+id).text());
		$("#smart-form input[name=acronym]").val($("#av_a_"+id).text());
	}

	function clearForm() {
		$("div#wid-id-4.jarviswidget header h2").text("Add AV");
		$("#smart-form footer button[type=submit]").text("Add");
		$("form#smart-form").attr("action", "/avs/add/");
		$("#smart-form input[name=id]").val("0");
		$("#smart-form input[name=title]").val("");
		$("#smart-form input[name=acronym]").val("");
	}

	function delRecord(id) {
		$.SmartMessageBox({
				title: "Delete AV",
				content: "Are you sure you want to delete the selected AV?",
				buttons: "[NO][YES]"
			},
			function (Res) {
				if (Res == "YES") {
					$.ajax({
						url: "/avs/del",
						method: "post",
						data: {id: id},
						success: function (res) {
							//document.location.reload(true);
							if (res == 'ok') $('tr#av_' + id).remove();
						}
					});
				}
			});
	}

	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

	// PAGE RELATED SCRIPTS

	// Load form valisation dependency
	loadScript("js/plugin/jquery-form/jquery-form.min.js", runFormValidation);


	// Registration validation script
	function runFormValidation() {

		$("#smart-form").validate({
			ignore : "",
			errorPlacement : function(error, element) {error.insertAfter(element.parent());}
		});
	}
</script>
