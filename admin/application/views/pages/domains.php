<?php if (!Auth::instance()->logged_in('admin')) exit();?>
<!-- Modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">

		<!-- Widget ID (each widget will need unique ID)-->
		<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">

			<header>
				<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				<h2>Create new domain</h2>

			</header>

			<!-- widget div-->
			<div>

				<!-- widget edit box -->
				<div class="jarviswidget-editbox">
					<!-- This area used as dropdown edit box -->

				</div>
				<!-- end widget edit box -->

				<!-- widget content -->
				<div class="widget-body no-padding">
					<form action="/domains/add/" method="post" id="smart-form" class="smart-form">
						<input type="hidden" value="0" name="id">
						<fieldset>

							<section>
								<label class="select">
									<select name="user_id">
										<option value=""> - choose user - </option>
										<?php foreach($users as $user): ?>
											<option value="<?php echo $user['id'] ?>"><?php echo $user['username']; ?></option>
										<?php endforeach; ?>
									</select>
									<i></i>
								</label>
							</section>

							<section>
								<label class="textarea"> <i class="icon-append fa fa-info-circle"></i>
									<textarea class="custom-scroll" rows="5" name="domain_name_list" placeholder="Domain name list"></textarea>
									<b class="tooltip tooltip-bottom-right">Needed to enter the domain name list</b> </label>
							</section>

							<section>
								<label class="input"> <i class="icon-append fa fa-info-circle"></i>
									<input type="text" name="domain_name" placeholder="Domain name">
									<b class="tooltip tooltip-bottom-right">Needed to enter the domain name</b> </label>
							</section>
						</fieldset>
						<footer>
							<button type="submit" class="btn btn-primary">
								Create
							</button>
						</footer>
					</form>

				</div>
				<!-- end widget content -->

			</div>
			<!-- end widget div -->

		</div>
		<!-- end widget -->

	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i>
			Domains
		</h1>
	</div>
	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
		<ul id="sparks" class="">
			<li class="sparks-info">
				<a data-toggle="modal" href="#myModal" class="btn btn-success btn-lg pull-right header-btn hidden-mobile" onclick="clearForm();"><i class="fa fa-circle-arrow-up fa-lg"></i> Create new domain </a>
			</li>
		</ul>
	</div>
</div>


<div class="row">
	<?php foreach (array_keys($domains) as $user): ?>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2><?=$user?></h2>
			</header>

			<div>
				<div class="widget-body">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="width:50px">ID</th>
								<th style="width:50px">Used</th>
								<th>Name</th>
								<th>DNS</th>
								<th style="width:50px">avd</th>
								<th style="width:150px">Action</th>
								<th style="width:150px">Last check</th>
								<th style="width:150px">IP</th>
								<th style="width:50px">avd</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($domains[$user] as $domain): ?>
							<tr id="dmn_<?php echo $domain['d_id']; ?>">
								<td style="width:50px">
									<span><?=$domain['d_id']?></span>
								</td>
								<td style="width:50px">
									<span class="badge bg-color-<?=$domain['used']==0?"greenLight":"redLight"?>"><?=$domain['used']==0?"Free":"Now"?></span>
								</td>
								
								<td>
									<span id="dmn_ui_<?php echo $domain['d_id']; ?>" style="display:none"><?php echo $domain['u_id']; ?></span>
									<span id="dmn_un_<?php echo $domain['d_id']; ?>" style="display:none"><?php echo $domain['username']; ?></span>
									<span id="dmn_nm_<?php echo $domain['d_id']; ?>"><?=$domain['name']?></span>
								</td>
								<td style="width:50px">
									<span style="cursor:pointer;" class="badge bg-color-<?=$domain['dns']?'greenLight':'red'?>"><?=$domain['dns']?'up':'down'?></span>
								</td>
								<td style="width:50px">
									<span style="cursor:pointer;" class="badge bg-color-<?=$domain['avd_detect']==0?("greenLight"):("red")?>" onClick="window.open('<?=$domain['avd_link']?>');"><?=$domain['avd_detect']?>/<?=$domain['avd_total']?></span>
								</td>
								<td style="width:150px">
									<a data-toggle="modal" href="#myModal" class="btn btn-primary" onclick="getData(<?php echo $domain['d_id']; ?>)">Edit</a>
									<a class="btn btn-danger" href="javascript:void(0);" onclick="delRecord(<?php echo $domain['d_id']; ?>)">Delete</a>
								</td>
								<td style="width:150px">
									<span><?=$domain['last_check']?></span>
								</td>
								<td style="width:150px">
									<span><?=$domain['ip']?$domain['ip']:"-"?></span>
								</td>
								<td style="width:50px">
									<span style="cursor:pointer;" class="badge bg-color-<?=$domain['i_avd_detect']==0?("greenLight"):("red")?>" onClick="window.open('<?=$domain['i_avd_link']?>');"><?=$domain['i_avd_detect']?>/<?=$domain['i_avd_total']?></span>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php endforeach; ?>
</div>

<!-- SCRIPTS ON PAGE EVENT -->
<script type="text/javascript">

	function getData(id) {
		$("div#wid-id-4.jarviswidget header h2").text("Edit domain");
		$("#smart-form footer button[type=submit]").text("Save");
		$("form#smart-form").attr("action", "/domains/edit/");
		$("#smart-form textarea[name=domain_name_list]").parent().parent().hide();
		$("#smart-form input[name=domain_name]").parent().parent().show();
		$("#smart-form input[name=id]").val(id);
		$("#smart-form select[name=user_id]").val($("#dmn_ui_"+id).text());
		$("#smart-form input[name=domain_name]").val($("#dmn_nm_"+id).text());
		$("#smart-form input[name=domain_name]").rules("add",{
			required: true
		});
		$("#smart-form input[name=domain_name_list]").rules("remove");
	}

	function clearForm() {
		$("div#wid-id-4.jarviswidget header h2").text("Create new domain name list");
		$("#smart-form footer button[type=submit]").text("Create");
		$("form#smart-form").attr("action", "/domains/add/");
		$("#smart-form textarea[name=domain_name_list]").parent().parent().show();
		$("#smart-form input[name=domain_name]").parent().parent().hide();
		$("#smart-form input[name=id]").val("0");
		$("#smart-form select[name=user_id]").val("0");
		$("#smart-form input[name=domain_name]").val("");
		$("#smart-form input[name=domain_name]").rules("remove");
		$("#smart-form input[name=domain_name_list]").rules("add",{
			required: true
		});
	}

	function delRecord(id) {
		$.SmartMessageBox({
				title: "Delete domain",
				content: "Are you sure you want to delete the selected domain?",
				buttons: "[NO][YES]"
			},
			function (Res) {
				if (Res == "YES") {
					$.ajax({
						url: "/domains/del",
						method: "post",
						data: {id: id},
						success: function (res) {
							//document.location.reload(true);
							if (res == 'ok') $('tr#dmn_' + id).remove();
						}
					});
				}
			});
	}

	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

	// PAGE RELATED SCRIPTS

	// Load form valisation dependency
	loadScript("js/plugin/jquery-form/jquery-form.min.js", runFormValidation);


	// Registration validation script
	function runFormValidation() {

		$("#smart-form").validate({
			ignore : "",
			errorPlacement : function(error, element) {error.insertAfter(element.parent());}
		});

		$("#smart-form select[name=user_id]").rules("add",{
			required: true
		});
	}
</script>
