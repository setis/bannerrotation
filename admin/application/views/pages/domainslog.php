
<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i>
			Domain and IP history 
		</h1>
	</div>
</div>


<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			<div>
				<div class="widget-body">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="width:30px">ID</th>
								<th style="width:50px">Domain</th>
								<th style="width:50px">AVD domain</th>
								<th style="width:150px">IP</th>
								<th style="width:50px">AVD IP</th>
								<th style="width:150px">Date add</th>
								<th style="width:150px">Date del</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($log as $_log): ?>
							<tr <?=$_log['date_del']!=''?("style=\"background-color: #F6CECE;\""):("")?>">
								<td><?=$_log['l_id']?></td>
								<td style="width:50px">
									<span><?=$_log['name']?></span>
								</td>
								<td style="width:50px">
									<span style="cursor:pointer;" class="badge bg-color-<?=$_log['d_avd_detect']==0?("greenLight"):("red")?>" onClick="window.open('<?=$_log['d_avd_link']?>');"><?=$_log['d_avd_detect']?>/<?=$_log['d_avd_total']?></span>
								</td>
								<td style="width:50px">
									<span><?=$_log['ip']?></span>
								</td>
								<td style="width:50px">
									<span style="cursor:pointer;" class="badge bg-color-<?=$_log['i_avd_detect']==0?("greenLight"):("red")?>" onClick="window.open('<?=$_log['i_avd_link']?>');"><?=$_log['i_avd_detect']?>/<?=$_log['i_avd_total']?></span>
								</td>
								<td style="width:50px">
									<span><?=$_log['date_add']?></span>
								</td>
								<td style="width:50px">
									<span><?=$_log['date_del']?></span>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- SCRIPTS ON PAGE EVENT -->
<script type="text/javascript">

	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

	// PAGE RELATED SCRIPTS

</script>
