<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				<h2>Create new user</h2>
			</header>
			<div>
				<div class="jarviswidget-editbox"></div>
				<div class="widget-body no-padding">
					<form action="/users/add/" method="post" id="smart-form" class="smart-form">
						<input type="hidden" value="0" name="id">
						<fieldset>
							<section>
								<label class="input"> <i class="icon-append fa fa-info-circle"></i>
									<input type="text" name="username" placeholder="User Name">
									<b class="tooltip tooltip-bottom-right">Needed to enter the User Name</b> </label>
							</section>
							<section>
								<label class="input"> <i class="icon-append fa fa-info-circle"></i>
									<input type="password" name="password" placeholder="Password">
									<b class="tooltip tooltip-bottom-right">Needed to enter the Password</b> </label>
							</section>
						</fieldset>
						<footer>
							<button type="submit" class="btn btn-primary">
								Create
							</button>
						</footer>
					</form>
				</div>
			</div>
		</div>
	</div>
</div><!-- /.modal -->

<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i>
			Users
		</h1>
	</div>
	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
		<ul id="sparks" class="">
			<li class="sparks-info">
				<a data-toggle="modal" href="#myModal" class="btn btn-success btn-lg pull-right header-btn hidden-mobile" onclick="clearForm();"><i class="fa fa-circle-arrow-up fa-lg"></i> Create new user </a>
			</li>
		</ul>
	</div>
</div>

<div class="row">
	<?php foreach ($users as $user): ?>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="usr_<?=$user["id"]?>">
		<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			<header>
				<span class="widget-icon"> <i class="fa fa-table"></i> </span>
				<h2><?=$user["username"]?></h2>
			</header>
			<div>
				<div class="widget-body">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td style="width: 100px">Action:</td>
								<td>
									<a data-toggle="modal" href="#myModal" class="btn btn-primary" onclick="getData(<?php echo $user['id']; ?>)">Edit</a>
									<a class="btn btn-danger" href="javascript:void(0);" onclick="delRecord(<?php echo $user['id']; ?>)">Delete</a>
								</td>
							</tr>
							<tr>
								<td>Username:</td>
								<td colspan="3">
									<span id="u_u_<?php echo $user['id']; ?>" style="display:none"><?php echo $user['username']; ?></span>
									<?=$user['username']?>
								</td>
							</tr>
							<tr>
								<td>Logins:</td>
								<td colspan="3"><?=$user['logins']?></td>
							</tr>
							<tr>
								<td>Last login:</td>
								<td colspan="3">
									<?=date("d-m-Y H:i:s", $user['last_login'])?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php endforeach; ?>
</div>


<!-- SCRIPTS ON PAGE EVENT -->
<script type="text/javascript">

	function getData(id) {
		var lst = null;
		$("div#wid-id-4.jarviswidget header h2").text("Edit user");
		$("#smart-form footer button[type=submit]").text("Save");
		$("form#smart-form").attr("action", "/users/edit/");
		$("#smart-form input[name=id]").val(id);
		$("#smart-form input[name=username]").val($("#u_u_"+id).text());
		$("#smart-form input[name=password]").val("");
	}

	function clearForm() {
		$("div#wid-id-4.jarviswidget header h2").text("Create new user");
		$("#smart-form footer button[type=submit]").text("Create");
		$("form#smart-form").attr("action", "/users/add/");
		$("#smart-form input[name=id]").val("0");
		$("#smart-form input[name=username]").val("");
		$("#smart-form input[name=password]").val("");
	}

	function delRecord(id) {
		$.SmartMessageBox({
			title: "Delete user",
			content: "Are you sure you want to delete the selected user?",
			buttons: "[NO][YES]"
		},
		function (Res) {
			if (Res == "YES")
				$.ajax({
					url: "/users/del",
					method: "post",
					data: {id: id},
					success: function (res) {
						if (res == "ok") $("#usr_" + id).remove();
					}
				});
		});
	}

	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

	// PAGE RELATED SCRIPTS

	// Load form valisation dependency
	loadScript("js/plugin/jquery-form/jquery-form.min.js", runFormValidation);


	// Registration validation script
	function runFormValidation() {

		$("#smart-form").validate({
			ignore : "",
			errorPlacement : function(error, element) {error.insertAfter(element.parent());}
		});


		$("#smart-form input[name=username]").rules("add",{
			required: true
		});
	}
</script>
