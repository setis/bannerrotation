<?php if (!Auth::instance()->logged_in('admin')) exit();?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">

		<!-- Widget ID (each widget will need unique ID)-->
		<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">

			<header>
				<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
				<h2>Create new domain</h2>

			</header>

			<!-- widget div-->
			<div>

				<!-- widget edit box -->
				<div class="jarviswidget-editbox">
					<!-- This area used as dropdown edit box -->

				</div>
				<!-- end widget edit box -->

				<!-- widget content -->
				<div class="widget-body no-padding">
					<form action="/ips/add/" method="post" id="smart-form" class="smart-form">
						<input type="hidden" value="0" name="id">
						<fieldset>

							<section>
								<label class="textarea"> <i class="icon-append fa fa-info-circle"></i>
									<textarea class="custom-scroll" rows="5" name="ips_list" placeholder="IP's list"></textarea>
									<b class="tooltip tooltip-bottom-right">Needed to enter the IP addresses list</b> </label>
							</section>

							<section>
								<label class="input"> <i class="icon-append fa fa-info-circle"></i>
									<input type="text" name="descr" placeholder="Description">
									<b class="tooltip tooltip-bottom-right">Description here</b> </label>
							</section>

							<section>
								<label class="input"> <i class="icon-append fa fa-info-circle"></i>
									<input type="text" name="ip" placeholder="IP">
									<b class="tooltip tooltip-bottom-right">Needed to enter the IP address</b> </label>
							</section>

						</fieldset>
						<footer>
							<button type="submit" class="btn btn-primary">
								Create
							</button>
						</footer>
					</form>

				</div>
				<!-- end widget content -->

			</div>
			<!-- end widget div -->

		</div>
		<!-- end widget -->

	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i>
			IP addresses
		</h1>
	</div>
	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
		<ul id="sparks" class="">
			<li class="sparks-info">
				<a data-toggle="modal" href="#myModal" class="btn btn-success btn-lg pull-right header-btn hidden-mobile" onclick="clearForm();"><i class="fa fa-circle-arrow-up fa-lg"></i> Create new IP addresses </a>
			</li>
		</ul>
	</div>
</div>

	<!-- row -->
	<div class="row">

			<!-- NEW WIDGET START -->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>IP addresses</h2>
					</header>

					<!-- widget div-->
					<div>

						<!-- widget content -->
						<div class="widget-body">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>IP</th>
										<th>Description</th>
										<th style="width:50px">Status</th>
										<th style="width:50px">avd</th>
										<th style="width:150px">Last check</th>
										<th style="width:150px">Action</th>
									</tr>
								</thead>
								<tbody>

								<?php foreach ($ips as $ip): ?>
									<tr id="ip_<?=$ip['id']?>">
										<td>
											<span id="ip_ip_<?php echo $ip['id']; ?>"><?=$ip['ip']?></span>
										</td>
										<td>
											<span id="ip_descr_<?php echo $ip['id']; ?>"><?=$ip['descr']?></span>
										</td>
										<td style="width:50px">
											<span style="cursor:pointer;" class="badge bg-color-<?=$ip['dns']?'greenLight':'red'?>"><?=$ip['dns']?'up':'down'?></span>
										</td>
										<td style="width:50px">
											<span style="cursor:pointer;" class="badge bg-color-<?=$ip['avd_detect']==0?("greenLight"):("red")?>" onClick="window.open('<?=$ip['avd_link']?>');"><?=$ip['avd_detect']?>/<?=$ip['avd_total']?></span>
										</td>
										<td style="width:150px">
											<span><?=$ip['last_check']?></span>
										</td>
										<td style="width:150px">
											<a data-toggle="modal" href="#myModal" class="btn btn-primary" onclick="getData(<?=$ip['id']?>)">Edit</a>
											<a class="btn btn-danger" href="javascript:void(0);" onclick="delRecord(<?=$ip['id']?>)">Delete</a>
										</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
						<!-- end widget content -->

					</div>
					<!-- end widget div -->

				</div>
				<!-- end widget -->

			</div>
			<!-- WIDGET END -->
	</div>

	<!-- end row -->

<!-- SCRIPTS ON PAGE EVENT -->
<script type="text/javascript">

	function getData(id) {
		$("div#wid-id-4.jarviswidget header h2").text("Edit IP address");
		$("#smart-form footer button[type=submit]").text("Save");
		$("form#smart-form").attr("action", "/ips/edit/");
		$("#smart-form textarea[name=ips_list]").parent().parent().hide();
		$("#smart-form input[name=ip]").parent().parent().show();
		$("#smart-form input[name=id]").val(id);
		$("#smart-form input[name=ip]").val($("#ip_ip_"+id).text());
		$("#smart-form input[name=ip]").rules("add",{
			required: true
		});
		$("#smart-form input[name=domain_name_list]").rules("remove");
	}

	function clearForm() {
		$("div#wid-id-4.jarviswidget header h2").text("Create new IP addresses list");
		$("#smart-form footer button[type=submit]").text("Create");
		$("form#smart-form").attr("action", "/ips/add/");
		$("#smart-form textarea[name=ips_list]").parent().parent().show();
		$("#smart-form input[name=ip]").parent().parent().hide();
		$("#smart-form input[name=id]").val("0");
		$("#smart-form input[name=ip]").val("");
		$("#smart-form input[name=ip]").rules("remove");
		$("#smart-form input[name=ips_list]").rules("add",{
			required: true
		});
	}

	function delRecord(id) {
		$.SmartMessageBox({
				title: "Delete IP address",
				content: "Are you sure you want to delete the selected IP address?",
				buttons: "[NO][YES]"
			},
			function (Res) {
				if (Res == "YES") {
					$.ajax({
						url: "/ips/del",
						method: "post",
						data: {id: id},
						success: function (res) {
							//document.location.reload(true);
							if (res == 'ok') $('tr#ip_' + id).remove();
						}
					});
				}
			});
	}

	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

	// PAGE RELATED SCRIPTS

	// Load form valisation dependency
	loadScript("js/plugin/jquery-form/jquery-form.min.js", runFormValidation);


	// Registration validation script
	function runFormValidation() {

		$("#smart-form").validate({
			ignore : "",
			errorPlacement : function(error, element) {error.insertAfter(element.parent());}
		});
	}
</script>
