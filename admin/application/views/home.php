<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title> TDS filter :: </title>
        <?php foreach ($styles as $style): ?>
            <link rel="stylesheet" type="text/css" media="screen" href="<?= URL::base() ?>css/<?= $style ?>.css">
        <?php endforeach; ?>

        <link rel="stylesheet" href="<?= URL::base() ?>css/opensans.css">
        <link rel="shortcut icon" href="<?= URL::base() ?>img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= URL::base() ?>img/favicon/favicon.ico" type="image/x-icon">
    </head>

    <body class="">
        <header id="header"></header>

        <aside id="left-panel">

            <div class="login-info">
                <span>
                    <img src="<?= URL::base() ?>img/avatars/male.png" alt="me" />
                    <a href="#">Welcome, <?= $username ?></a>
                </span>
            </div>

            <nav>
                <ul>
                    <li class="">
                        <a href="dashboard" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-fw fa-info"></i> Campaigns Info</a>
                        <ul>
                            <?php foreach ($users as $user_id => $user): ?>
                                <li>
                                    <a href="#"><i class="fa fa-lg fa-fw fa-user"></i><?= $user['username'] ?></a>
                                    <ul>
                                        <?php if (!empty($companies[$user_id])): ?>
                                            <?php foreach ($companies[$user_id] as $company): ?>
                                                <li>
                                                    <a href="/campinfo/show/<?= $company['host_id'] ?>"><?= $company['name'] ?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-fw fa-crosshairs"></i> Campaigns</a>
                        <ul>

                            <?php foreach ($users as $user_id => $user): ?>
                                <li>
                                    <a href="/campaigns/user/<?= $user_id ?>"><i class="fa fa-lg fa-fw fa-user"></i><?= $user['username'] ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                    <li>
                        <a href="/domains/show/"><i class="fa fa-lg fa-fw fa-globe"></i> Domains</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-fw fa-info"></i> Domain and IP history</a>
                        <ul>
                            <?php foreach ($users as $user_id => $user): ?>
                                <?php if (!empty($hosts[$user_id]['id'])): ?>
                                        <li>
                                            <a href="/domainslog/show/<?= $hosts[$user_id]['id'] ?>"><?= $user['username'] ?></a>
                                        </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                    <li>
                        <a href="/ips/show/"><i class="fa fa-lg fa-fw fa-gear"></i> IP's</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-fw fa-picture-o"></i> Banners</a>
                        <ul>
                            <?php foreach ($users as $user_id => $user): ?>
                                <li>
                                    <a href="/banners/user/<?= $user_id ?>"><i class="fa fa-lg fa-fw fa-user"></i><?= $user['username'] ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                    <li>
                        <a href="/flash/show/"><i class="fa fa-lg fa-fw fa-picture-o"></i>SWF banners</a>
                    </li>
                    <li>
                        <a href="/users/show/"><i class="fa fa-lg fa-fw fa-group"></i> Users</a>
                    </li>
                    <?php /* if (Auth::instance()->logged_in('admin')): ?>
                      <li>
                      <a href="/tdscodes/show/"><i class="fa fa-lg fa-fw fa-code"></i> PHP codes</a>
                      </li>
                      <?php endif; */ ?>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> AV Stat</a>
                        <ul>
                            <li>
                                <a href="/avstat/type/adult"><i class="fa fa-lg fa-fw fa-user"></i>adult</a>
                            </li>
                            <li>
                                <a href="/avstat/type/nonadult"><i class="fa fa-lg fa-fw fa-user"></i>nonadult</a>
                            </li>
                            <li>
                                <a href="/avstat/type/redirect"><i class="fa fa-lg fa-fw fa-user"></i>redirect</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="/avs/show/"><i class="fa fa-lg fa-fw fa-bug"></i> AV list</a>
                    </li>
                </ul>
            </nav>
            <span class="minifyme"> <i class="fa fa-arrow-circle-left hit"></i> </span>
        </aside>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- MAIN CONTENT -->
            <div id="content">

            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->


        <script data-pace-options='{ "restartOnRequestAfter": true }' src="<?= URL::base() ?>js/plugin/pace/pace.min.js"></script>

        <?php foreach ($scripts as $script): ?>
            <script src="<?= URL::base() ?>js/<?= $script ?>.js"></script>
        <?php endforeach; ?>

    </body>
</html>
