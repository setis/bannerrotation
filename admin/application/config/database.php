<?php

defined('SYSPATH') or die('No direct access allowed.');

return [
    'static' => array(
        'type' => 'MySQLi',
        'connection' => array(
            'hostname' => '127.0.0.1',
            'username' => 'root',
            'password' => '123123',
            'database' => 'banner',
            'port' => NULL,
            'socket' => NULL
        ),
        'table_prefix' => '',
        'charset' => 'utf8',
        'caching' => FALSE,
    ),
    'default' => array(
        'type' => 'MySQLi',
        'connection' => array(
            'hostname' => '127.0.0.1',
            'username' => 'root',
            'password' => '123123',
            'database' => 'banner',
            'port' => NULL,
            'socket' => NULL
        ),
        'table_prefix' => '',
        'charset' => 'utf8',
        'caching' => FALSE,
    ),
    'banner' => [
        'type' => 'MongoDB',
        'server' => 'mongodb://localhost',
        'database' => 'banner'
    ]
];

