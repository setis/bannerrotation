<?php defined('SYSPATH') or die('No direct access allowed.');

return array(
    'directory' => '/tmp',
    'remove_spaces' => TRUE
);