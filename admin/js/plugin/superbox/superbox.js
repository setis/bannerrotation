/*
	SuperBox v1.0.0 (modified by bootstraphunter.com)
	by Todd Motto: http://www.toddmotto.com
	Latest version: https://github.com/toddmotto/superbox
	
	Copyright 2013 Todd Motto
	Licensed under the MIT license
	http://www.opensource.org/licenses/mit-license.php

	SuperBox, the lightbox reimagined. Fully responsive HTML5 image galleries.
*/
;(function($) {
		
	$.fn.SuperBox = function(options) {
		
		var superbox      = $('<div class="superbox-show"></div>'),
			superboximg   = $('<img src="" class="superbox-current-img"><div id="imgInfoBox" class="superbox-imageinfo inline-block"> <h1>Image Title</h1><span><p><em>http://imagelink.com/thisimage.jpg</em></p><p class="superbox-img-description">Image description</p><p><a href="javascript:void(0);" class="btn btn-danger btn-sm superbox-delbut">Delete</a></p></span> </div>'),
			superboxclose = $('<div class="superbox-close txt-color-white"><i class="fa fa-times fa-lg"></i></div>');

		superbox.append(superboximg).append(superboxclose);
		
		var imgInfoBox = $('.superbox-imageinfo');
		
		return this.each(function() {
			
			$('.superbox-list').click(function() {
				$this = $(this);

				var currentimg = $this.find('.superbox-img'),
					imgData = currentimg.data('img'),
					imgId = currentimg.data('id'),
					imgCId = currentimg.data('cid'),
					imgDescription = currentimg.attr('alt') || "No description",
					imgLink = imgData,
					imgTitle = currentimg.attr('title') || "No Title";
					
					//console.log(imgData, imgDescription, imgLink, imgTitle)

				superbox.find('#imgInfoBox em').text(imgLink);
				superbox.find('#imgInfoBox >:first-child').text(imgTitle);
				superbox.find('#imgInfoBox .superbox-img-description').text(imgDescription);
					
				superboximg.attr('src', imgData);
				superboximg.find('a.superbox-delbut').off('click');
				if (imgCId != 0) superboximg.find('a.superbox-delbut').hide();
				else superboximg.find('a.superbox-delbut').show();
				if (options['delEvent'] != undefined)
					superboximg.find('a.superbox-delbut').on('click', function() {options['delEvent'](imgId);});
				
				$('.superbox-list').removeClass('active');
				$this.addClass('active');
				
				//console.log("fierd")
				
				if($('.superbox-current-img').css('opacity') == 0) {
					$('.superbox-current-img').animate({opacity: 1});
				}
				
				if ($(this).next().hasClass('superbox-show')) {
					$('.superbox-list').removeClass('active');
					superbox.toggle();
				} else {
					superbox.insertAfter(this).css('display', 'block');
					$this.addClass('active');
				}
				
				$('html, body').animate({
					scrollTop:superbox.position().top - currentimg.width()
				}, 'medium');
			
			});
						
			$('.superbox').on('click', '.superbox-close', function() {
				$('.superbox-list').removeClass('active');
				$('.superbox-current-img').animate({opacity: 0}, 200, function() {
					$('.superbox-show').slideUp();
				});
			});

		});
	};
})(jQuery);