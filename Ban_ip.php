<?php

class Ban_ip {

    public static function is($ip) {
        $db = Env::$static->prepare('SELECT COUNT(*) FROM `ban_ip` WHERE id=:id LIMIT 1;');
        $db->execute([':id' => inet_pton($ip)]);
        return ($db->fetch()[0] == 0) ? false : true;
    }

    public static function ban($ip) {
        $db = Env::$static->prepare('INSERT INTO `ban_ip` (id,ip) VALUES(:id,:ip);');
        try {
            $db->execute([':id' => inet_pton($ip), 'ip' => $ip]);
        } catch (PDOException $e) {
            if ($db->errorInfo()[1] === 1062) {
                return;
            }
            throw $e;
        }
    }

    public static function unban($ip) {
        $db = Env::$static->prepare('DELETE FROM `ban_ip` WHERE id=:id');
        $db->execute([':id' => inet_pton($ip)]);
    }

    public static function create() {
        $db = Env::$static->prepare("CREATE TABLE ban_ip (
  id binary(16) NOT NULL,
  ip varchar(15) NOT NULL,
  UNIQUE INDEX id (id)
)
ENGINE = INNODB
CHARACTER SET latin1
COLLATE latin1_swedish_ci;");
        try {
            $db->execute();
        } catch (PDOException $e) {
            if ($db->errorInfo()[1] !== 1050) {
                echo $e->getMessage();
                exit;
            }
        }
    }

}
