<?php

class Statistics {

    public static function insert($host_id, array $data, $f = false) {
        $prefix = 'q_' . $host_id . '_' . date('y_n_j');
        $db = Env::$static->prepare("INSERT INTO `$prefix` (`ip`, `ua`,`ua_size`, `ua_hash`, `referer`, `ua_uniq`, `referer_uniq`, `referer_host`, `referer_size`,`referer_hash`,`timeoffset`, `request`, `response`, `screen_width`, `screen_heigth`, `ie`,`uniq`,`rest`,`geo_country`,`mode`,`os`,`browser`,`version`)
VALUES (:ip, :ua, :referer, UNHEX(:ua_uniq),:ua_size,UNHEX(:ua_hash), :referer_uniq, :referer_host,:referer_size,:referer_hash, :timeoffset, :request,  now(), :width,:heigth, :ie,:uniq,:rest,:geo_country,:mode,:os,:browser,:version);");
        try {
            $db->execute($data);
        } catch (PDOException $e) {
            //не существует таблицы
            if ($db->errorInfo()[1] === 1146 && $f === false) {
                self::create_table_day($prefix);
                self::create_trigger($host_id, $prefix);
                self::insert($host_id, $data, true);
            } else {
                echo $e->getTraceAsString() . "<br>\n";
                echo $e->getMessage() . "<br>\n";
                var_dump($db->errorInfo());
                var_dump($prefix);
                var_dump($data, array_diff(array_keys($data), [':ip', ':ua', ':ua_uniq', 'ua_size', ':ua_hash', ':referer_uniq', ':referer_host', ':referer_size', ':referer_hash', ':timeoffset', ':request', ':response', ':width', ':heigth', ':ie', ':uniq', ':rest', ':geo_country']));
                echo "<br>\n";
                exit;
            }
        }
    }

    public static function check_table($table) {
        $db = Env::$static->prepare("SELECT count(table_name) FROM information_schema.tables WHERE table_type = '$table';");
        try {
            $db->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }
        return ($db->fetch()[0] == 1);
    }

    public static function create_table_week($host_id) {
        self::create_virtual_table('w_' . date('W_y'), $host_id, Cacl::week());
    }

    public static function create_virtual_table($prefix, $host_id, array $tables) {
        $in = implode(',', array_map(function($v)use($host_id) {
                    return "'q_{$host_id}_{$v}'";
                }, $tables));
        $db = Env::$static->prepare("SELECT table_name FROM information_schema.tables WHERE table_name in ($in);");
        try {
            $db->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }

        $result = [];
        foreach ($db->fetchAll() as $row) {
            $result[] = $row[0];
        }
        if (count($result) === 0) {
            throw new Exception('not tables');
        }
        $table = array_shift($result);
        $sql = "CREATE VIEW `{$host_id}_{$prefix}` AS SELECT `id`,`ip`, `ua`,`ua_size`, `ua_hash`, `referer`, `ua_uniq`, `referer_uniq`, `referer_host`, `referer_size`,`referer_hash`,`timeoffset`, `request`, `response`, `screen_width`, `screen_heigth`, `ie`,`uniq`,`rest`,`geo_country`,`mode` FROM $table";
        foreach ($result as $table2) {
            $sql.=" LEFT JOIN `$table2` ON `$table`.uniq=$table2.uniq";
        }
        $db = Env::$static->prepare($sql);
        try {
            $db->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public static function create_table_day($table) {
        $db = Env::$static->prepare("CREATE TABLE `$table` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip` binary(16) NOT NULL,
  `ua` varchar(255) CHARACTER SET utf8 NOT NULL,
  `referer` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `ua_uniq` varbinary(767) NOT NULL COMMENT 'md5(\$ua).strlen(\$ua)',
  `ua_size` int(5) unsigned DEFAULT NULL,
  `ua_hash` binary(20) DEFAULT NULL,
  `referer_uniq` varbinary(767) DEFAULT NULL COMMENT 'sha1(\$referer).strlen(\$referer)',
  `referer_host` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `referer_size` int(5) unsigned DEFAULT NULL,
  `referer_hash` binary(20) DEFAULT NULL,
  `timeoffset` int(2) DEFAULT NULL COMMENT 'тайм зона',
  `request` float(10,3) DEFAULT NULL COMMENT 'время отправки запрос',
  `response` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'время ответа запроса',
  `screen_width` int(4) DEFAULT NULL COMMENT 'ширина экрана',
  `screen_heigth` int(4) DEFAULT NULL COMMENT 'высота  экрана',
  `os` varchar(255) CHARACTER SET utf8 NOT NULL,
  `browser` varchar(255) CHARACTER SET utf8 NOT NULL,
  `version` varchar(12) CHARACTER SET utf8 NOT NULL,
  `ie` bit(1)  DEFAULT NULL COMMENT 'проверка на ie',
  `geo_country` varchar(255) DEFAULT NULL ,
  `rest` bit(1) NOT NULL DEFAULT b'0' COMMENT '1 - rest, 0 - ajax',
  `uniq` bit(1) NOT NULL DEFAULT b'0' COMMENT '1 - uniq 0 - not uniq',
  `mode` int(2)  unsigned DEFAULT NULL,
  `type` bit(1) NULL COMMENT ' 0 - in 1 - pre 2 - out',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
        try {
            $db->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public static function create() {
        $browser = "CREATE TABLE s_browser (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `browser` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `in` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `out` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `pre` int(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  INDEX IDX_s (host_id, date)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET latin1
COLLATE latin1_swedish_ci;";
        $os = "CREATE TABLE s_os (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `os` varchar(50) NOT NULL,
  `in` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `out` int(6) UNSIGNED NOT NULL DEFAULT 0,
  `pre` int(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  INDEX IDX_s (host_id, date)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET latin1
COLLATE latin1_swedish_ci;";
        $geo = "CREATE TABLE s_geo (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` int(10) NOT NULL DEFAULT 0,
  `date` date NOT NULL,
  `geo` varchar(5) NOT NULL,
  `in` int(6) NOT NULL DEFAULT 0,
  `out` int(6) NOT NULL DEFAULT 0,
  `pre` int(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  INDEX IDX_s (host_id, date,geo)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET latin1
COLLATE latin1_swedish_ci;";
        $referer = "CREATE TABLE s_referer (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_id` int(10) NOT NULL DEFAULT 0,
  `date` date NOT NULL,
  referer varchar(255) NOT NULL,
  `in` int(6) NOT NULL DEFAULT 0,
  `out` int(6) NOT NULL DEFAULT 0,
  `pre` int(6) NOT NULL  DEFAULT 0,
  PRIMARY KEY (id),
  INDEX IDX_s (host_id, date,referer)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET latin1
COLLATE latin1_swedish_ci;";
        $db = Env::$static->prepare($browser);
        try {
            $db->execute();
        } catch (PDOException $e) {
            if ($db->errorInfo()[1] !== 1050) {
                echo $e->getMessage();
                exit;
            }
        }
        $db = Env::$static->prepare($os);
        try {
            $db->execute();
        } catch (PDOException $e) {
            if ($db->errorInfo()[1] !== 1050) {
                echo $e->getMessage();
                exit;
            }
        }
        $db = Env::$static->prepare($geo);
        try {
            $db->execute();
        } catch (PDOException $e) {
            if ($db->errorInfo()[1] !== 1050) {
                echo $e->getMessage();
                exit;
            }
        }
        $db = Env::$static->prepare($referer);
        try {
            $db->execute();
        } catch (PDOException $e) {
            if ($db->errorInfo()[1] !== 1050) {
                echo $e->getMessage();
                exit;
            }
        }
    }

    public static function create_trigger($host_id, $table) {
        $sql = "CREATE 
	DEFINER = 'root'@'localhost'
TRIGGER banner.t_$table
	AFTER INSERT
	ON banner.$table
	FOR EACH ROW
BEGIN
set @host_id := $host_id;
set @date := CURRENT_DATE();
SELECT count(id),s.id,s.in,s.out,s.pre into @check,@id,@in,@out,@pre FROM `s_browser` as s WHERE s.browser = NEW.browser AND s.version = NEW.version AND s.date = @date AND s.host_id = @host_id LIMIT 1;
if @check = 0 then
  if(NEW.type = null) then
    insert into s_browser (`browser`,`version`,`in`,`host_id`,`date`) VALUES(NEW.browser,NEW.version,1,@host_id,@date);
  elseif(NEW.type = 0) then 
    insert into s_browser (`browser`,`version`,`pre`,`host_id`,`date`) VALUES(NEW.browser,NEW.version,1,@host_id,@date);
  elseif(NEW.type = 1) then
    insert into s_browser (`browser`,`version`,`out`,`host_id`,`date`) VALUES(NEW.browser,NEW.version,1,@host_id,@date);
  end if;
else
  if(NEW.type = null) then
    set @in:=@in+1;
    UPDATE s_browser SET s_browser.in = @in  WHERE id =@id; 
  elseif(NEW.type = 0) then 
    set @pre:=@pre+1;
    UPDATE s_browser SET s_browser.pre = @pre  WHERE id =@id; 
  elseif(NEW.type = 1) then
    set @out:=@out+1;
    UPDATE s_browser SET s_browser.out = @out  WHERE id =@id; 
  end if;
end if;
SELECT count(id),s.id,s.in,s.out,s.pre into @check,@id,@in,@out,@pre FROM `s_os` as s WHERE s.os = NEW.os AND s.date = @date AND s.host_id = @host_id LIMIT 1;
if @check = 0 then
  if(NEW.type = null) then
    insert into s_os (`os`,`in`,`host_id`,`date`) VALUES(NEW.os,1,@host_id,@date);
  elseif(NEW.type = 0) then 
    insert into s_os (`os`,`pre`,`host_id`,`date`) VALUES(NEW.os,1,@host_id,@date);
  elseif(NEW.type = 1) then
    insert into s_os (`os`,`out`,`host_id`,`date`) VALUES(NEW.os,1,@host_id,@date);
  end if;
else
  if(NEW.type = null) then
    set @in:=@in+1;
    UPDATE s_os SET s_os.in = @in  WHERE id =@id; 
  elseif(NEW.type = 0) then 
    set @pre:=@pre+1;
    UPDATE s_os SET s_os.pre = @pre  WHERE id =@id; 
  elseif(NEW.type = 1) then
    set @out:=@out+1;
    UPDATE s_os SET s_os.out = @out  WHERE id =@id; 
  end if;
end if;
SELECT count(id),s.id,s.in,s.out,s.pre into @check,@id,@in,@out,@pre FROM `s_geo` as s WHERE s.geo = NEW.geo_country AND s.date = @date AND s.host_id = @host_id LIMIT 1;
if @check = 0 then
  if(NEW.type = null) then
    insert into s_geo (`geo`,`in`,`host_id`,`date`) VALUES(NEW.geo_country,1,@host_id,@date);
  elseif(NEW.type = 0) then 
    insert into s_geo (`geo`,`pre`,`host_id`,`date`) VALUES(NEW.geo_country,1,@host_id,@date);
  elseif(NEW.type = 1) then
    insert into s_geo (`geo`,`out`,`host_id`,`date`) VALUES(NEW.geo_country,1,@host_id,@date);
  end if;
else
  if(NEW.type = null) then
    set @in:=@in+1;
    UPDATE s_geo SET s_geo.in = @in  WHERE id =@id; 
  elseif(NEW.type = 0) then 
    set @pre:=@pre+1;
    UPDATE s_geo SET s_geo.pre = @pre  WHERE id =@id; 
  elseif(NEW.type = 1) then
    set @out:=@out+1;
    UPDATE s_geo SET s_geo.out = @out  WHERE id =@id; 
  end if;
end if;
SELECT count(id),s.id,s.in,s.out,s.pre into @check,@id,@in,@out,@pre FROM `s_referer` as s WHERE s.referer = NEW.referer_host AND s.date = @date AND s.host_id = @host_id LIMIT 1;
if @check = 0 then
  if(NEW.type = null) then
    insert into s_referer (`referer`,`in`,`host_id`,`date`) VALUES(NEW.geo_country,1,@host_id,@date);
  elseif(NEW.type = 0) then 
    insert into s_referer (`referer`,`pre`,`host_id`,`date`) VALUES(NEW.geo_country,1,@host_id,@date);
  elseif(NEW.type = 1) then
    insert into s_referer (`referer`,`out`,`host_id`,`date`) VALUES(NEW.geo_country,1,@host_id,@date);
  end if;
else
  if(NEW.type = null) then
    set @in:=@in+1;
    UPDATE s_referer SET s_referer.in = @in  WHERE id =@id; 
  elseif(NEW.type = 0) then 
    set @pre:=@pre+1;
    UPDATE s_referer SET s_referer.pre = @pre  WHERE id =@id; 
  elseif(NEW.type = 1) then
    set @out:=@out+1;
    UPDATE s_referer SET s_referer.out = @out  WHERE id =@id; 
  end if;
end if;
END";
        $db = Env::$static->prepare($sql);
        try {
            $db->execute();
        } catch (PDOException $e) {
            if ($db->errorInfo()[1] !== 1050) {
                echo $e->getMessage();
                exit;
            }
        }
    }

    public static function collection(array $data = []) {
        $ua_hash = sha1($_SERVER['HTTP_USER_AGENT']);
        $ua_size = strlen($_SERVER['HTTP_USER_AGENT']);
        $data[':ip'] = inet_pton($_SERVER['REMOTE_ADDR']);
        $data[':ua'] = $_SERVER['HTTP_USER_AGENT'];
        $data[':ua_hash'] = $ua_hash;
        $data[':ua_size'] = $ua_size;
        $data[':ua_uniq'] = $ua_hash . dechex($ua_size);
        if (!empty($_SERVER['HTTP_REFERER'])) {
            $referer_hash = sha1($_SERVER['HTTP_REFERER'], true);
            $referer_size = strlen($_SERVER['HTTP_REFERER']);
            $referer_host = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
            $data[':referer'] = $_SERVER['HTTP_REFERER'];
            $data[':referer_hash'] = $referer_hash;
            $data[':referer_size'] = $referer_size;
            $data[':referer_uniq'] = $referer_hash . decbin($referer_size);
            $data[':referer_host'] = $referer_host;
        } else {
            $data[':referer_host'] = $data[':referer_uniq'] = $data[':referer_size'] = $data[':referer_hash'] = $data[':referer'] = null;
        }
        $data[':geo_country'] = (!empty($_SERVER['GEOIP_COUNTRY_CODE'])) ? $_SERVER['GEOIP_COUNTRY_CODE'] : null;
        $data[':request'] = $_SERVER['REQUEST_TIME_FLOAT'];
        $data[':ie'] = (empty($_GET['asd'])) ? null : $_GET['asd'];
        $data[':timeoffset'] = (empty($_GET['offset'])) ? null : $_GET['offset'];
        foreach ([':width', ':heigth', ':mode'] as $v) {
            $data[$v] = (!empty($data[$v])) ? $data[$v] : null;
        }
        return $data;
    }

}
